<?php
session_start();
require_once('codigos/funcoesPainel.php');
$dadosPainel  = dadosConfiguracoes();

// se j&aacute; est&aacute; logado
if (isset($_SESSION[SESSAO_CHAVE])) {
	header("Location: home.php");
	exit;
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<?php require_once('includes/head.php'); ?>
<link type="text/css" href="estilos-login.css" rel="stylesheet" />
<script type="text/javascript">
	$(document).ready (function(){
		if (!$.support.placeholder) {
			$("#usuario, #senha").focus (function(){ if ($(this).val() == $(this).attr("placeholder")) { $(this).val(""); } });
			$("#usuario, #senha").blur  (function(){ if ($(this).val() == "") { $(this).val($(this).attr("placeholder")); } });
			$("#usuario, #senha").blur();
		}
		
		$("#usuario").focus();
	});
</script>
</head>

<body class="login gradiente">
	<div class="loginWrapper">
    	<div class="loginLogo"><img src="img/logo-empresa.png" /></div>
        <div class="loginTexto">Painel de Administra&ccedil;&atilde;o do site <?=$dadosPainel['nome_entidade'];?></div>
        <div class="loginConteudo">
        	<form action="login.php" method="post">
        		<div class="loginCampo" id="login">
            		<div class="loginCampoLabel">Login</div>
            	    <input type="text" name="usuario" id="usuario" value='' placeholder="Login" class="loginCampoInput" />
            	</div>
        		<div class="loginCampo">
            		<div class="loginCampoLabel">Senha</div>
            	    <input type="password" name="senha" id="senha" value='' placeholder="Senha" class="loginCampoInput" />
                    <input type="submit" name="entrar" value='Entrar' class="loginCampoButton" />
            	</div>
            </form>
        </div>
    </div>
</body>
</html>