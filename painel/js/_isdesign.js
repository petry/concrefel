/*
Copyright [2014] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS [www.isdesign.com.br]
*/

// a fun&ccedil;&atilde;o
ativarOuExcluir = function(opt) {
	
	/* defauts do plugin */
	var defaults = {
		tabela: ""
	};
	
	var settings = $.extend({}, defaults, opt );
	// a fun&ccedil;&atilde;o $.extend que ir&aacute; buscar os valores passados pela vari&aacute;vel opt e mesclar com os valores definidos na vari&aacute;vel defaults, armazenando em outra vari&aacute;vel chamada settings.
	
	
	/* delegate */
	$("body").delegate("a[data-campo='ativo'], a[data-campo='capa'], a[data-campo='lancamento']","click", ativarDesativar);
	//$("body").delegate("input.botaoInputExcluir","click", excluir);
	
	/* fun&ccedil;&atilde;o para ativar ou desativar */
	function ativarDesativar() {
		
		// variaveis
		var ajaxContent = $(this).parent(), 
			id = $(this).attr("data-id"),
			campo = $(this).attr("data-campo"),
			status = $(this).attr("data-status");
		
		// Mensagem de carregando
		ajaxContent.html("<img src='img/loading.gif' height='15'>");
		
		// Faz requisi&ccedil;&atilde;o ajax
		$.post("codigos/ajax.php", {
			id:id, campo:campo, status:status, table:settings.tabela, acao:"ativar"
		}, function(resposta) {
			// Coloca as enquetes na DIV
			ajaxContent.html(resposta);
		});
	};
	/**/
	
	// excluir foto do texto
	$(".botaoInputExcluir").click(function(){ 
		$("#alert-excluir").show(200);
	});
	
	// cancelar exclus&atilde;o
	$("#alert-excluir #cancelarExclusao").click(function(){
		$("#alert-excluir").hide(200);
	});
	
	
	// confirmar exclus&atilde;o
	$("body").delegate("#alert-excluir #excluirSim","click",function(){ 
		$("#alert-excluir").hide(200);
		
		var excluir = [];
		// percorrer todos
		for (i=0; i< $('input.check').length; i++) {
			var quem = $("input.check:eq("+i+")");
			if (quem.is(':checked')) {
				excluir.push(quem.attr("data-id"));
			}
		};
		if(excluir.length > 0) {
			$(".ajaxStatusDel").html('<img src="img/loading.gif" height="20">');
			$(".ajaxStatusDel").slideDown("fast", function() {
				// Faz requisi&ccedil;&atilde;o ajax
				$.post("codigos/ajax.php", {	
				excluir:excluir, table:settings.tabela, acao:"excluir" 
				}, function(resposta) {
					
					//console.log(resposta);
					
					if(resposta.replace(/ /g, "").replace(/1/g, "") == "excluido") { // .replace(/ /g, "") &eacute; a express&atilde;o regular para remover espa&ccedil;os
						$(".ajaxStatusDel").html("Excluido com sucesso.");
						// sumir com as tr's
						for (j=0; j<excluir.length; j++) {
							$("input[data-id="+excluir[j]+"]").parent().parent().stop().slideUp(300, function(){
								$("input[data-id="+excluir[j]+"]").removeAttr("checked");
							});
						}
					} else {
						$(".ajaxStatusDel").html("Nao foi possivel excluir os registros selecionados.");
					}
				});
				
			});
		}
		else {
			$(".ajaxStatusDel").html("Nenhum registro selecionado.");
		}
	});
	
	/* selecionar todos */
	$("#selecionarTodos").click(function() {
		$('input.check').prop("checked",$("#selecionarTodos").prop("checked"));
	});
};