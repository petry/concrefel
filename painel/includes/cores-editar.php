<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');
require_once('codigos/moduloProdutos.php'); //Fun&ccedil;őes e m&eacute;todos do m&oacute;dulo

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

// salvar
if(isset($_POST['id'])){ //Quer dizer que &eacute; um cadastro
	salvarCor();
	exit;
}

//Caso deseje listar determinado registro
if($_GET['id'] && is_numeric($_GET['id'])){
	$edi 		= 1;
	$id			= (int)$_GET['id'];
	$consulta	= consultar('*','cor', 'id='.$id);
	$dados = mysqli_fetch_assoc($consulta);
}
else {
	$id = 's';

}
?>
<h1>Cores</h1>
<form action="" method="post" enctype="multipart/form-data">
<input name="id" type="hidden" id="id" value="<?=$id;?>" />

<div class="bloco marginTop20">
	<div class="padding20 metade">
		<h2>Nome da Cor:</h2>
		<input type="text" id="titulo" name="titulo" value='<?=$dados["titulo"];?>' placeholder="Cor" maxlength="50" class="form-input" />
	</div>

	<div class="padding20 metade">
    	<h2>Cor:</h2>
		
    	<input name="cor" id="cor" class="form-input" type="color" value="<?=$dados['cor'];?>">
	    
	</div>
	<div class="clearBoth"></div>
</div>
<?php /*
<div class="bloco marginTop20">
	<div class="padding20">
		<h2>T&iacute;tulo do Texto:</h2>
		<input type="text" id="titulo1" name="titulo1" value='<?=$dados["titulo_1"];?>' placeholder="T&iacute;tulo do texto" maxlength="120" class="form-input" />
	</div>
</div>
*/ ?>

<input name="button" type="submit" class="botaoInput width150 marginTop10 float" id="button" value="Salvar" />
</form>

<script type="text/javascript" src="http:js/core/ajaxfileupload.js"></script>
<script type="text/javascript" language="javascript">
</script>