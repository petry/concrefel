<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 10/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);
?>

<div class="titulo">
	<h1>Editor de Textos</h1>
</div>
<div class="spacer25"></div>

<div class="legenda">Selecione a p&aacute;gina que voc&ecirc; deseja editar:</div>
<table>
	<tr>
      <td class="listaBarraTexto">&nbsp; P&aacute;gina</td>
    </tr>
<?php
	$consulta = consultar("id, titulo, subtitulo", "texto", "", "id ASC");
	////
    while($texto = mysqli_fetch_assoc($consulta)){
		?>
		<tr>
		  <td class="listaItemTexto listaEditorTexto"><a href="home.php?pagina=textos-editar&id=<?=$texto['id'];?>" title="Editar p&aacute;gina"><span><?= $texto['titulo'].' &raquo; '.$texto['subtitulo']; ?> &nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a></td>
		</tr>
<?php } ?>
</table>
