<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

//Total de registros a serem exibitos por p&aacute;ginas
$registrosPorPagina = 75;

//Listagem
if ((isset($_POST["pesquisa"])  && $_POST["pesquisa"]!="Pesquisar") || isset($_GET["pesquisa"])) {
    $pesquisa = (isset($_POST["pesquisa"])) ? $_POST["pesquisa"] : $_GET["pesquisa"];
	$pesquisa1 = str_replace(" ", "%", antiInjection($pesquisa));
    $where = "titulo LIKE '%$pesquisa1%' OR subtitulo LIKE '%$pesquisa1%' OR texto LIKE '%$pesquisa1%'";
}else{
    $where = "";
}
// pagina&ccedil;&atilde;o
if (isset($_GET["pag"]) && is_numeric($_GET["pag"])) {
    $comecar = (($_GET["pag"] - 1) * $registrosPorPagina);
}else{
	$comecar = 0;
}?>
<div class="titulo">
	<h1>Not&iacute;cias</h1>
    <div class="tituloBotoes">
    	<form action="" method="post">
        	<div class="pesquisarFundo"><input type="submit" name="enviar" value='' class="pesquisarBotao" /></div>
        	<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquisar" class="pesquisarCampo" value="<?=$pesquisa;?>" />
        </form>
    	<a href="home.php?pagina=noticias-editar" title="Adicionar nova not&iacute;cia" class="botao botaoAdicionar">Adicionar not&iacute;cia</a>
    </div>
</div>
<div class="spacer50"></div>

<div class="legenda">
	<p>Exibir na capa</p>
    <img src="img/legenda-home.png" width="18" height="18" />
	<p>Exibir no site</p>
    <img src="img/legenda-visivel.png" width="18" height="18" />
</div>

<table>
	<tr>
	  <td class="listaBarraCheck"><input type="checkbox" id="selecionarTodos" title="Selecionar todos os produtos" /></td>
	  <td class="listaBarraTexto" colspan="1">Not&iacute;cia:</td>
	  <td class="listaBarraTexto">Tipo:</td>
	  <td class="listaBarraTexto listaBarraInfo">Data:</td>
	  <td class="listaBarraIcones"><img src="img/legenda-visivel.png" width="18" height="18" /></td>
	  <td class="listaBarraIcones"><img src="img/legenda-home.png" width="18" height="18" /></td>

	</tr>
    <?php
	$consulta = consultar("id,titulo,subtitulo,exibir,data,capa,ativo", "noticia", $where, "data DESC, id DESC", $comecar, $registrosPorPagina); 
	
	////
	while ($noticia	= mysqli_fetch_assoc($consulta)) {
		?>
  <tr>
	<td class="listaItemCheck"><input type="checkbox" class="check" data-id="<?=$noticia['id'];?>" title="Selecionar esse produto" /></td>
	<td class="listaItemTexto">
		<a href="home.php?pagina=noticias-editar&id=<?=$noticia['id'];?>" title="Editar not&iacute;cia"><span><?=$noticia['titulo']?>
		<?php 
		if ($noticia['subtitulo']){
			echo ' &raquo; '.$noticia['subtitulo'];
		} ?>
		&nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a>
	</td>
	<td class="listaItemTexto"><?php 
	if ($noticia['exibir']==1){ echo "Not&iacute;cias"; } 
	else if ($noticia['exibir']==2){ echo "Fotos de Obras"; } 
	else if ($noticia['exibir']==3){ echo "Not&iacute;cias + Fotos de Obras"; } 
	?></td>
	<td class="listaItemTexto listaItemInfo"><?=dataPadraoBrasileiro($noticia['data']);?></td>
	<td class="listaItemIcones ajaxContent">
	  <a href="javascript:;" data-campo="ativo" data-status="<?=$noticia['ativo'];?>" data-id="<?=$noticia['id'];?>"><img src="img/form-checkbox<?php if($noticia['ativo']) { echo "-checked"; } ?>.jpg" width="13" height="13" /></a>
	</td>
	<td class="listaItemIcones ajaxContent">
      	<a href="javascript:;" data-campo="capa" data-status="<?=$noticia['capa'];?>" data-id="<?=$noticia['id'];?>"><img src="img/form-checkbox<?php if($noticia['capa']) { echo "-checked"; } ?>.jpg" width="13" height="13" /></a>
      </td>
	
  </tr>
    <? } ?>
</table>

<div class="divExcluir">
    <input type="button" value="Excluir" class="botaoInput botaoInputExcluir" />
    <span class="ajaxStatusDel"></span>
</div>

<div class="listaPaginacao marginTop10 text12">
<?php 
//Pagina&ccedil;&atilde;o de Resultados
paginar('noticia','noticias', $where, $pesquisa, $registrosPorPagina);
?>
</div>

<script type="text/javascript" src="js/_isdesign.js?2"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	ativarOuExcluir({tabela: "noticia"}); // fun&ccedil;&atilde;o da isDesign que permite ativar e excluir por ajax
});
</script>