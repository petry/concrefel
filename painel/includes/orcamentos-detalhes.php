<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);
if(isset($_GET['id']) && is_numeric($_GET['id'])){
	$id = (int)$_GET['id'];
}
else{
	header("Location: home.php?pagina=orcamentos-lista");
	exit;
}
// consulta
$consulta = consultar("*", "orcamentos", "id=$id"); 
$orcamento = mysqli_fetch_assoc($consulta);
$status = $orcamento['status'];

?>
<div class="titulo">
	<h1>Detalhes do Or&ccedil;amento</h1>
</div>
<div class="spacer25"></div>

<table>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Nº Or&ccedil;amento:</td>
	  <td class="listaItemTexto"><?=$orcamento['id'];?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Nome:</td>
	  <td class="listaItemTexto"><?=$orcamento['nome'];?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Email:</td>
	  <td class="listaItemTexto"><?=$orcamento['email'];?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Telefone:</td>
	  <td class="listaItemTexto"><?=$orcamento['telefone'];?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Endere&ccedil;o:</td>
	  <td class="listaItemTexto"><?=$orcamento['endereco'];?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">CPF ou CNPJ:</td>
	  <td class="listaItemTexto"><?=$orcamento['cpf'];?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Data:</td>
	  <td class="listaItemTexto"><?=dataEhora($orcamento['data']);?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Observa&ccedil;&otilde;es:</td>
	  <td class="listaItemTexto"><?=$orcamento['observacoes'];?></td>
	</tr>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaItemTexto">Status:</td>
	  <td class="listaItemTexto"><select name="status" id="status" class="form-input">
		<option value="1" <?php if($status == 1){ echo 'selected';} ?>>Aberto</option>
		<option value="2" <?php if($status == 2){ echo 'selected';} ?>>Em produ&ccedil;&atilde;o</option>
		<option value="3" <?php if($status == 3){ echo 'selected';} ?>>Entregue</option>
		<option value="4" <?php if($status == 4){ echo 'selected';} ?>>Faturado</option>
	</select></td>
	</tr>
</table>
	
<?php
$consultarItens = consultar("p.titulo as titulo, p.subtitulo as subtitulo, c.nome as categoria, pi.quantidade, pi.cor AS cor", "orcamentos_itens pi, produto p, categoria c", "pi.idOrcamento=$id AND pi.produto=p.id AND p.categoria=c.id ", "pi.id ASC");
if(mysqli_num_rows($consultarItens)>0){
?>

<div class="spacer25"></div>
<h2>Itens do Or&ccedil;amento: </h2>

<table>
	<tr>
	  <td class="listaBarraCheck"></td>
	  <td class="listaBarraTexto">Produto:</td>
	  <td class="listaBarraTexto">Quantidade:</td>
	  <td class="listaBarraTexto">Cor:</td>
	</tr>
	<?php
	while($itens = mysqli_fetch_assoc($consultarItens)){
		?>
		<tr>
		  <td class="listaItemTexto"></td>
		  <td class="listaItemTexto">
		  <?=$itens['categoria'];?> >> <?=$itens['titulo'];?>
		  <?php 
			if ($produto['subtitulo']){
				echo ' &raquo; '.$produto['subtitulo'];
			} ?>
		  </td>
		  <td class="listaItemTexto"><?=$itens['quantidade'];?></td>
		  <td class="listaItemTexto"><?=$itens['cor'];?></td>
		</tr>
	<?php 
	} // while
	?>
</table>
<?php	
} // if itens
?>  

<script type="text/javascript" language="javascript">
$(document).ready(function(){

	$("#status").change(function(){
		var acao		= "trocarStatus",
			table	= "orcamentos",
			campo	= "status",
			id		= <?=$id;?>,
			status	= $("#status").val();
		
		$.post("codigos/ajax.php", {
		id:id, status:status, campo:campo, table:table, acao:acao
		}, function(respostaI) {
			//
		});
	});
});
</script>