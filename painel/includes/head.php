<?php
require_once('codigos/funcoesPainel.php');
$dadosPainel  = dadosConfiguracoes();
?>
<title>Painel de Administra&ccedil;&atilde;o | <?=$dadosPainel['nome_entidade'];?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Robots" content="ALL" />
<meta name="Language" content="pt-br" />
<meta name="Webmaster" content="isDesign" />
<meta name="Author" content="http://www.isdesign.com.br/" />
<meta HTTP-EQUIV="Reply-to" content="contato@isdesign.com.br" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

<link type="text/css" href="estilos.css" rel="stylesheet" />
<script type="text/javascript" src="js/core/jquery-1.8.3.js"></script>
<link type="text/css" href="js/core/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" />
<script type="text/javascript" src="js/core/jquery-ui-1.9.2.custom.min.js"></script>