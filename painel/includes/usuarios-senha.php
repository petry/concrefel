<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 13/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');
require_once('codigos/moduloUsuario.php'); //Fun&ccedil;őes padrőes do sistema

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso();

if($_POST['senhaAtual'] && $_POST['senhaNova'] && $_POST['senhaConfirma']){
	$alterar = alterarSenha();
}
if($_POST['senhaAtual'] && $alterar=="0"){ ?>

	<strong class="color-vermelha">Erro! N&atilde;o foi poss&iacute;vel alterar sua senha. A senha atual est&aacute; incorreta.</strong>

<?php }else if($_POST['senhaAtual'] && $alterar=="2"){ ?>

	<strong class="color-vermelha">Erro! N&atilde;o foi poss&iacute;vel alterar sua senha. A nova senha e a confirma&ccedil;&atilde;o de senha est&atilde;o diferentes.</strong>

<?php }else if($alterar=="1"){ ?>

	<strong class="color-azul">Ok! Senha alterada corretamente.</strong>

<?php } ?>

<div class="separa"></div>

<h1>Usu&aacute;rios - Alterar senha</h1>

<form action="" method="post" name="formulario" id="formulario">
<div class="bloco marginTop20 padding20">
	<h2>Trocar a senha de <?php echo $dadosUsuario['nome']; ?>:</h2>   
     
    <div class="separa"></div>
    
    <label for="senhaAtual" class="label width125 float">Senha atual:</label> 
    <input type="password" name="senhaAtual" id="senhaAtual" value='' class="input float" required />
    
    <div class="separa"></div>
    
    <label for="senhaNova" class="label width125 float">Nova senha:</label> 
    <input type="password" name="senhaNova" id="senhaNova" value='' class="input float" required />
    
    <div class="separa"></div>
    
    <label for="senhaConfirma" class="label width125 float">Confirmar senha:</label> 
    <input type="password" name="senhaConfirma" id="senhaConfirma" value='' class="input float" required />
    
    <div class="clearBoth"></div>
    
</div>
<input type="submit" name="enviar" id="enviar" value="Alterar senha" class="botaoInput width125 marginTop10 float" />
</form>
	