<?php
session_start();
require_once('codigos/funcoesPainel.php'); //Fun&ccedil;őes padrőes do sistema
?>
<div class="homeOla">
    <div class="homeOlaLogo"><img src="img/logo-empresa.png" /></div>
    <div class="homeOlaTexto">
        <h1>Ol&aacute;, <?=$dadosUsuario['nome'];?>.</h1>
        <p>&nbsp;</p>
        <p>Voc&ecirc; est&aacute; no Painel de Administra&ccedil;&atilde;o do site.</p>
        <p>Aqui voc&ecirc; pode gerenciar o conte&uacute;do do seu site.</p>
    </div>
</div>
<div class="spacer80"></div>
<div class="home">
    <div class="homeTitle">
        <h1>Voc&ecirc; no controle</h1>
        <p>&nbsp;</p>
        <p>Para ajudarmos voc&ecirc;, criamos, abaixo, uma breve explica&ccedil;&atilde;o de como este painel funciona:</p>
    </div>
    <ul>
        <li>
            <div class="homeExplicacao" id="homeExplicacao1">
                <h2>Menu</h2>
                <p>&nbsp;</p>
                <p>Na esquerda do painel, voc&ecirc; encontra uma lista com todas as p&aacute;ginas de deu site.</p>
                <img src="img/setas-lado.png" width="150" height="40" class="setasLado" />
            </div>
        </li>
        <li>
            <div class="homeExplicacao" id="homeExplicacao3">
                <h2>Barra do Usu&aacute;rio</h2>
                <p>&nbsp;</p>
                <p>Acima, voc&ecirc; v&ecirc; as principais informa&ccedil;&otilde;es do usu&aacute;rio, pode voltar &agrave; home e/ou sair.</p>
                <img src="img/setas-cima.png" width="80" height="230" class="setasCima" />
            </div>
        </li>
        <div class="clearBoth"></div>
    </ul>
</div>

<div class="spacer80"></div>

<script type="text/javascript">
$(document).ready (function(){
	resize = (function(){
		var explicacaoFinal = 0;
		var atualizacaoFinal = 0;
		
		$("#homeExplicacao1, #homeExplicacao2, #homeExplicacao3").each (function() {
			var explicacaoVar = $(this).attr("id");
		    if ($("#"+explicacaoVar).height() > explicacaoFinal) { explicacaoFinal = $("#"+explicacaoVar).height();}
		});
		$("#homeExplicacao1, #homeExplicacao2, #homeExplicacao3").height(explicacaoFinal);
		
		$("#homeAtualizacao1,#homeAtualizacao2,#homeAtualizacao3,#homeAtualizacao4,#homeAtualizacao5,#homeAtualizacao6,#homeAtualizacao7,#homeAtualizacao8,#homeAtualizacao9").each (function() {
			var atualizacaoVar = $(this).attr("id");
		    if ($("#"+atualizacaoVar).height() > atualizacaoFinal) { atualizacaoFinal = $("#"+atualizacaoVar).height();}
		});
		$("#homeAtualizacao1, #homeAtualizacao2, #homeAtualizacao3, #homeAtualizacao4, #homeAtualizacao5, #homeAtualizacao6, #homeAtualizacao7, #homeAtualizacao8, #homeAtualizacao9").height(atualizacaoFinal);
	});
	
	$(window).resize(function(){
		$("#homeExplicacao1, #homeExplicacao2, #homeExplicacao3").height("auto");
		$("#homeAtualizacao1, #homeAtualizacao2, #homeAtualizacao3, #homeAtualizacao4, #homeAtualizacao5, #homeAtualizacao6, #homeAtualizacao7, #homeAtualizacao8, #homeAtualizacao9").height("auto");
		resize();
	});
	resize();
});
</script>