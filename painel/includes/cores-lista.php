<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

//Total de registros a serem exibitos por p&aacute;ginas
$registrosPorPagina = 75;

//Listagem
if ((isset($_POST["pesquisa"])  && $_POST["pesquisa"]!="Pesquisar") || isset($_GET["pesquisa"])) {
    $pesquisa = (isset($_POST["pesquisa"])) ? $_POST["pesquisa"] : $_GET["pesquisa"];
	$pesquisa1 = str_replace(" ", "%", antiInjection($pesquisa));
    $where = "titulo LIKE '%$pesquisa1%'";
}else{
    $where = "";
}
// pagina&ccedil;&atilde;o
if (isset($_GET["pag"]) && is_numeric($_GET["pag"])) {
    $comecar = (($_GET["pag"] - 1) * $registrosPorPagina);
}else{
	$comecar = 0;
}?>
<div class="titulo">
	<h1>Cores</h1>
    <div class="tituloBotoes">
    	<form action="" method="post">
        	<div class="pesquisarFundo"><input type="submit" name="enviar" value='' class="pesquisarBotao" /></div>
        	<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquisar" class="pesquisarCampo" value="<?=$pesquisa;?>" />
        </form>
    	<a href="home.php?pagina=cores-editar" title="Adicionar novo produto" class="botao botaoAdicionar">Adicionar Cor</a>
    </div>
</div>
<div class="spacer50"></div>


<table>
	<tr>
	  <td class="listaBarraCheck"><input type="checkbox" id="selecionarTodos" title="Selecionar todos os cores" /></td>
	  <td class="listaBarraTexto" colspan="1">Nome:</td>
	  <td class="listaBarraTexto">Cor:</td>
	

	</tr>
    <?php
	$consulta = consultar("id,cor,titulo", "cor", $where, "titulo ASC", $comecar, $registrosPorPagina); 
	
	////
	while ($cor	= mysqli_fetch_assoc($consulta)) {
		
	?>
  <tr>
	<td class="listaItemCheck"><input type="checkbox" class="check" data-id="<?=$cor['id'];?>" title="Selecionar essa cor" /></td>
	<td class="listaItemTexto">
		<a href="home.php?pagina=cores-editar&id=<?=$cor['id'];?>" title="Editar cor"><span><?=$cor['titulo'];?>
		
		&nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a>
	</td>
	<td class="listaItemTexto"><div class="produtos-cores-cor" style="background-color:<?=$cor['cor'];?>"></div></td>
	
  </tr>
    <? } ?>
</table>

<div class="divExcluir">
    <input type="button" value="Excluir" class="botaoInput botaoInputExcluir" />
    <span class="ajaxStatusDel"></span>
</div>

<div class="listaPaginacao marginTop10 text12">
<?php 
//Pagina&ccedil;&atilde;o de Resultados
paginar('produto','produtos', $where, $pesquisa, $registrosPorPagina);
?>
</div>

<script type="text/javascript" src="js/_isdesign.js?2"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	ativarOuExcluir({tabela: "cor"}); // fun&ccedil;&atilde;o da isDesign que permite ativar e excluir por ajax
});
</script>