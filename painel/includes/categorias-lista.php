<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 06/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

//Total de registros a serem exibitos por p&aacute;ginas
$registrosPorPagina = 50;
//Listagem 
if ((isset($_POST["pesquisa"])  && $_POST["pesquisa"]!="Pesquisar") || isset($_GET["pesquisa"])) {
	$pesquisa = (isset($_POST["pesquisa"])) ? $_POST["pesquisa"] : $_GET["pesquisa"];
	$pesquisa1 = str_replace(" ", "%", antiInjection($pesquisa));
	$where = "nome LIKE '%$pesquisa1%'";
}else{
	$where = '';
}
// pagina&ccedil;&atilde;o
if (isset($_GET["pag"]) && is_numeric($_GET["pag"])) {
	$comecar = (($_GET["pag"] - 1) * $registrosPorPagina);
}else{
	$comecar = 0;
}
?>
<div class="titulo">
	<h1>Categorias de Produtos</h1>
	 <div class="tituloBotoes">
    	<form action="?pagina=categorias-lista" name="frm_filtros" method="post">
        	<div class="pesquisarFundo"><input type="submit" name="enviar" value='' class="pesquisarBotao" /></div>
        	<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquisar" class="pesquisarCampo" value="<?=$pesquisa;?>" />
        </form>
    	<a href="home.php?pagina=categorias-editar" title="Adicionar nova categoria" class="botao botaoAdicionar">Adicionar categoria</a>
    </div>
 </div>
<div class="spacer50"></div>

<div class="legenda">
    <p>Exibir no site</p>
    <img src="img/legenda-visivel.png" width="18" height="18" />
</div>

<table>
    <tr>
 	  <td class="listaBarraCheck"><input type="checkbox" id="selecionarTodos" title="Selecionar todos as categorias" /></td>
      <td class="listaBarraTexto listaEditorTexto">Categorias</td>
      <td class="listaBarraIcones"><img src="img/legenda-visivel.png" width="18" height="18" /></td>
    </tr>
    <?php
	$consulta = consultar('*','categoria', $where, 'nome ASC', $comecar, $registrosPorPagina);
	while($dados = mysqli_fetch_assoc($consulta)){
		?>
  <tr>
	<td class="listaItemCheck"><input type="checkbox" class="check" data-id="<?=$dados['id'];?>" title="Selecionar essa categoria" /></td>
    <td class="listaItemTexto listaEditorTexto" ><a href="home.php?pagina=categorias-editar&id=<?=$dados['id'];?>" title="Editar categoria"><span><?=$dados['nome'];?> &nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a></td>
	<td class="listaItemIcones ajaxContent">
	  <a href="javascript:;" data-campo="ativo" data-status="<?=$dados['ativo'];?>" data-id="<?=$dados['id'];?>"><img src="img/form-checkbox<?php if($dados['ativo']) { echo "-checked"; } ?>.jpg" width="13" height="13" /></a>
	</td>
  </tr>
    <? } ?>
</table>
<div class="divExcluir">
    <input type="button" value="Excluir" class="botaoInput botaoInputExcluir" />
    <span class="ajaxStatusDel"></span>
</div>

<div class="listaPaginacao marginTop10 text12">
<?php 
//Pagina&ccedil;&atilde;o de Resultados
paginar('categoria','categorias', $where, $pesquisa, $registrosPorPagina);
?>
</div>

<script type="text/javascript" src="js/_isdesign.js?2"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	ativarOuExcluir({tabela: "categoria"}); // fun&ccedil;&atilde;o da isDesign que permite ativar e excluir por ajax
});
</script>