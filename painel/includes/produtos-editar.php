<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');
require_once('codigos/moduloProdutos.php'); //Fun&ccedil;őes e m&eacute;todos do m&oacute;dulo

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

// salvar
if(isset($_POST['id'])){ //Quer dizer que &eacute; um cadastro
	salvarProduto();
	exit;
}

//Caso deseje listar determinado registro
if($_GET['id'] && is_numeric($_GET['id'])){
	$edi 		= 1;
	$id			= (int)$_GET['id'];
	$consulta	= consultar('*','produto', 'id='.$id);
	$dados = mysqli_fetch_assoc($consulta);
}
else {
	$id = 's';
	$dados['ativo'] = 1;
	$dados['capa'] = 1;
}
?>
<h1>Produtos</h1>
<form action="" method="post" enctype="multipart/form-data">
<input name="id" type="hidden" id="id" value="<?=$id;?>" />
<input name="novas-fotos" type="hidden" id="novas-fotos"/>

<div class="bloco marginTop20">
	<div class="padding20">
		<h2>T&iacute;tulo do Produto:</h2>
		<input type="text" id="titulo" name="titulo" value='<?=$dados["titulo"];?>' placeholder="Titulo do produto" maxlength="120" class="form-input" />
	</div>
</div>
<div class="bloco marginTop20">
	<div class="padding20 metade">
		<h2>Subt&iacute;tulo do Produto:</h2>
		<input type="text" id="subtitulo" name="subtitulo" value='<?=$dados["subtitulo"];?>' placeholder="Subtitulo do produto" maxlength="150" class="form-input" />
	</div>

	<div class="padding20 metade">
    	<h2>Categoria:</h2>
		<div class="form-input">
    	<select name="categoria" id="categoria" class="form-select">
	    	<option value="" selected="selected"> - - selecione - - </option>
    		<?php
			$consultaCategorias = consultar('id,nome','categoria', '', 'nome ASC');
			while($categoria = mysqli_fetch_assoc($consultaCategorias)){
				?>
				<option value="<?=$categoria['id'];?>" <?php if($categoria['id'] == $dados['categoria']) { echo 'selected="selected"'; } ?>><?=$categoria['nome'];?></option>
        	<?php } ?>
        </select>
		</div>
	</div>
	<div class="clearBoth"></div>
</div>
<?php /*
<div class="bloco marginTop20">
	<div class="padding20">
		<h2>T&iacute;tulo do Texto:</h2>
		<input type="text" id="titulo1" name="titulo1" value='<?=$dados["titulo_1"];?>' placeholder="Titulo do texto" maxlength="120" class="form-input" />
	</div>
</div>
*/ ?>
<div class="bloco marginTop20 padding20">
	<h2>Texto:</h2>
	<textarea name="texto" cols="100" rows="20" id="texto1" ><?=$dados['texto'];?></textarea>
</div>

<!--Imagem do topo-->
<div class="bloco marginTop20">
	<div class="editarNome"><h2>Imagem no topo do produto <span class="ajaxStatusTopo text12 text666"></span></h2></div>
	<div id="conteudo">
		<div class="editarConteudo" id="imagemPrincipalTopo" style="display:<?php if($dados['imagem_topo']) { echo "block"; } else { echo "none"; } ?>;">
			<div class="editarConteudoItem">
				<div id="imagemPrincipalSimTopo" style="display:<?php if($dados['imagem_topo']) { echo "block"; } else { echo "none"; } ?>;">
					<?php if($dados['imagem_topo']) { ?>
						<a href="../imgs/<?=$dados['imagem_topo'];?>" data-fancybox="gallery" title="<?=$dados['legenda_topo'];?>"><img src="../includes/thumb.php?file=../imgs/<?=$dados['imagem_topo'];?>&w=110&h=100" alt="<?=$dados['legenda_topo'];?>" /></a>
					<?php } ?>
				</div>
				<div id="imagemPrincipalNaoTopo" style="display:<?php if($dados['imagem_topo']) { echo "none"; } else { echo "block"; } ?>;">
					<p>Upload da imagem: (.jpg .gif ou .png)</p>
					<input type="file" name="imagem_topo" id="imagem_topo" value="<?=$dados['imagem_topo'];?>" />
				</div>
			</div>
			<div class="editarConteudoItem">
				<p>Legenda da imagem</p>
				<input type="text" name="legendaTopo" id="legendaTopo" class="input" value='<?=$dados['legenda_topo'];?>' maxlength="100" />
			</div>
			<div class="clearBoth"></div>
			<a href="javascript:;" id="botaoRemoverImagemTopo" title="Remover esta imagem" class="botao botaoCinza botaoRemover">Remover</a>
		</div>
	</div>
	<div class="editarRodape">
		<a href="javascript:;" id="botaoAdicionarImagemTopo" title="Adicionar uma imagem" class="botao botaoCinza width200 float" style="display:<?php if($dados['imagem_topo']) { echo "none"; } else { echo "block"; } ?>;">Adicionar uma imagem</a>
	</div>
</div>

<div class="clearBoth"></div>

<!--Imagens ao lado do texto-->
<div class="bloco marginTop20">
	<div class="editarNome"><h2>Imagem ao lado do texto <span class="ajaxStatusLado text12 text666"></span></h2></div>
	<div id="conteudo">
		<div class="editarConteudo" id="imagemPrincipalLado" style="display:<?php if($dados['imagem_lado']) { echo "block"; } else { echo "none"; } ?>;">
			<div class="editarConteudoItem">
				<div id="imagemPrincipalSimLado" style="display:<?php if($dados['imagem_lado']) { echo "block"; } else { echo "none"; } ?>;">
					<?php if($dados['imagem_lado']) { ?>
						<a href="../imgs/<?=$dados['imagem_lado'];?>" data-fancybox="gallery" title="<?=$dados['legenda_lado'];?>"><img src="../includes/thumb.php?file=../imgs/<?=$dados['imagem_lado'];?>&w=110&h=100" alt="<?=$dados['legenda_lado'];?>" /></a>
					<?php } ?>
				</div>
				<div id="imagemPrincipalNaoLado" style="display:<?php if($dados['imagem_lado']) { echo "none"; } else { echo "block"; } ?>;">
					<p>Upload da imagem: (.jpg .gif ou .png)</p>
					<input type="file" name="imagem_lado" id="imagem_lado" value="<?=$dados['imagem_lado'];?>" />
				</div>
			</div>
			<div class="editarConteudoItem">
				<p>Legenda da imagem</p>
				<input type="text" name="legendaLado" id="legendaLado" class="input" value='<?=$dados['legenda_lado'];?>' maxlength="100" />
			</div>
			<div class="clearBoth"></div>
			<a href="javascript:;" id="botaoRemoverImagemLado" title="Remover esta imagem" class="botao botaoCinza botaoRemover">Remover</a>
		</div>
	</div>
	<div class="editarRodape">
		<a href="javascript:;" id="botaoAdicionarImagemLado" title="Adicionar uma imagem" class="botao botaoCinza width200 float" style="display:<?php if($dados['imagem_lado']) { echo "none"; } else { echo "block"; } ?>;">Adicionar uma imagem</a>
	</div>
</div>
	

<div class="bloco marginTop20 padding20">
	<h2>Imagens do Produto: <span class="text12 text666 textNormal">Arraste as fotos para organiz&aacute;-las.</span></h2>
	
	<div class='cadastro-album'>
		<span id='morefilesFotos'>
		<?php 
		$consultaFotos = consultar("id,file,legenda", "imagem", "pai='produto' AND idPai=$id AND idPai != 0", "pos ASC, id ASC",0,0,0);
		while ($foto = @mysqli_fetch_assoc($consultaFotos)) {
			?>
			<div class='cadastro-album-li'>
				<div class='cadastro-album-imagem'>
					<img src='../includes/thumb.php?file=../imgs/<?=$foto['file'];?>&w=160&h=500' />
				</div>
				<div class='foto-album-legenda'>
					<a href="javascript:;" title="Excluir foto" class="cadastro-album-excluir" data-id="<?=$foto['id'];?>">x</a>
					<input type='text' data-id='<?=$foto['id'];?>' value='<?=$foto['legenda'];?>' maxlength='100' class='cadastro-album-legenda' placeholder='Legenda'/>
				</div>
			</div>
			<?php 
		} // for fotos ?>
		</span>
		
		<a href='javascript:;' title='Adicionar mais fotos' class='cadastro-album-novo'>
			<div class='cadastro-novo-botao'>Adicionar</div>
		</a>
		<input type='file' multiple name='fotosToUpload[]' id='fotosToUpload' class='cadastro-album-escondido' />
		<div class="clearBoth"></div>
		<div class="fotos-first">Foto principal</div>
	</div>
</div>

<div class="bloco marginTop20">
	<div class="editarNome"><h2>V&iacute;deos:</h2></div>
    <div id="morefilesVideo">
	<?php
		$consultaVideos = consultar("url,titulo,descricao", "video", "pai='produto' AND idPai=$id AND idPai !=0", "titulo ASC");
		$contV = 0;
		while($video = @mysqli_fetch_assoc($consultaVideos)){
			?>
            <div id="video<?=$contV;?>" class='editarConteudo' data-id="<?=$contV;?>">
                <div class='editarConteudoItem'>
                    <p>Insira o link do seu v&iacute;deo no YouTube:</p>
					<input type='text' name='urlVideo[]' class='form-input' maxlength="150" value='<?=$video['url'];?>' />
                </div>
                <div class='editarConteudoItem'>
                    <p>T&iacute;tulo do v&iacute;deo:</p>
                    <input type='text' name='tituloVideo[]' class='form-input' maxlength='100' value='<?=$video['titulo'];?>' />
                </div>
                <div class='editarConteudoItem'>
                    <p>Descri&ccedil;&atilde;o do v&iacute;deo:</p>
					<input type='text' name='descricaoVideo[]' class='form-input' maxlength="200" value='<?=$video['descricao'];?>' />
                </div>
                <div class='clearBoth'></div>
                <a href="javascript:rmvMoreFilesVideo('video<?=$contV;?>')" title='Remover este v&iacute;deo' class='botao botaoCinza botaoRemover'>Remover</a>
            </div>
        <?php $contV++;
        }
    ?>
	</div>
	<div class="editarRodape"><a href="javascript:dispMoreFilesVideo();" title="Adicionar v&iacute;deo" class="botao botaoCinza width150 float">Adicionar v&iacute;deo</a></div>
</div>

<div class="bloco marginTop20">
	<div class="editarNome"><h2>Arquivos Anexos:</h2></div>
    <div id="morefilesArq">
    <?php
		$consultaArq = consultar("id,arquivo,titulo", "arquivo", "pai='produto' AND idPai=$id", "titulo ASC");
		$contAr = 0;
		while($arquivo = @mysqli_fetch_assoc($consultaArq)){
			$extArq	= iconeArquivo($arquivo['arquivo']);
			?>
			<div id="arq<?=$contAr;?>" class='editarConteudo' data-id="<?=$contAr;?>">
				<div class='editarConteudoItem'>
					<a href="../imgs/<?=$arquivo['arquivo'];?>" title='<?=$arquivo['titulo'];?>' target="_blank">
						<div class="arquivosIcone" align="center"><img src="img/icone-arquivo-<?=$extArq;?>.jpg" alt='<?=$arquivo['titulo'];?>' /></div>
					</a>
                </div>    
                <div class='editarConteudoItem'>
					<p>Legenda: </p>
					<input type='hidden' name='idArquivoOld[]' value='<?=$arquivo['id'];?>' />
					<input type='hidden' name='legendaArquivoOld[]' value='<?=$arquivo['titulo'];?>' />
                    <input type='hidden' name='nomeArquivoOld[]' value='<?=$arquivo['arquivo'];?>' />
                    <!-- -->
                    <input type='text' name='legendaArquivoNew[]' class='form-input' value='<?=$arquivo['titulo'];?>' required maxlength='150' />
                </div>
                <div class='clearBoth'></div>
                <span class="ajaxStatusArq">
                	<a href="javascript:;" title="Remover este arquivo" class="botao botaoCinza botaoRemover removerArquivo" data-idArquivo="<?=$arquivo['id'];?>" data-fileArquivo="arq<?=$contAr;?>" >Remover</a>
                </span>
            </div>
        <?php $contAr++;
        } 
    
    ?>
    </div>
    <div class="editarRodape"><a href="javascript:dispMoreFilesArq();" title="Adicionar arquivos" class="botao botaoCinza width150 float">Adicionar arquivo</a></div>
</div>
<div class="bloco marginTop20">
	<div class="editarNome"><h2>Cores:</h2></div>

<?php 
	$cCores = consultar("*","cor");
	$cont = 0;
	while($cor = mysqli_fetch_assoc($cCores)){
		$checked = '';
		$cor_produto = consultar("idCor","cor_produto","idProduto = '$id' AND idCor = ".$cor['id']."");
		if(mysqli_num_rows($cor_produto) > 0){
			$checked = 'checked';
		}
		?>
		<span style="height:50px; float:left; margin:20px;">
		 <label><div class="produtos-cores-cor" style="background-color:<?=$cor['cor'];?>"><input type="checkbox" name="cores[]" value="<?=$cor['id'];?>" style="margin-top:6px; margin-left:7px;" <?=$checked;?> ></div> <span style="margin-top:10px;"><?=$cor['titulo'];?></span></label>
		</span>
	<?php } ?>
	<div class="clearBoth"></div>
</div>
<div class="bloco marginTop20">
	<div class="padding20">
		<h2>Visibilidade:</h2>
	    <p>&nbsp;</p>
	    <div class="visibilidade visibilidadeBig"><input type="checkbox" name="ativo" id="visibilidadeSite" value="1" <?php if($dados['ativo']){ ?>checked="checked"<?php } ?> />&nbsp; <label for="visibilidadeSite"><img src="img/legenda-visivel.png" width="18" height="18" />&nbsp; Exibir nos produtos</label></div>
		<div class="visibilidadeSpacer"></div>
		<div class="visibilidade visibilidadeH"><input type="checkbox" name="capa" id="visibilidadeHome" value="1"  <?php if($dados['capa']){ ?>checked="checked"<?php } ?> />&nbsp; <label for="visibilidadeHome"><img src="img/legenda-home.png" width="18" height="18" />&nbsp; na home</label></div>
		<div class="clearBoth"></div>
	</div>
</div>

<input name="button" type="submit" class="botaoInput width150 marginTop10 float" id="button" value="Salvar" />
</form>

<script type="text/javascript" src="http:js/core/ajaxfileupload.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	
	/*****
	** Imagem Topo
	****/
	// add
	$("#botaoAdicionarImagemTopo").click (function(){
		$("#imagemPrincipalTopo").slideDown(200);
		$("#botaoAdicionarImagemTopo").hide();
	});
	// excluir
	$("body").delegate("#botaoRemoverImagemTopo", "click", function(){ 
		$("#alert-excluir").show(200);
		$("#alert-excluir #excluirSim").attr("data-oque","topo");
		$("#alert-excluir #excluirSim").attr("data-id","<?=$id;?>");
	});
	
	/**** 
	** Imagem Lado
	****/
	// add
	$("#botaoAdicionarImagemLado").click (function(){
		$("#imagemPrincipalLado").slideDown(200);
		$("#botaoAdicionarImagemLado").hide();
	});
	// excluir
	$("body").delegate("#botaoRemoverImagemLado", "click", function(){ 
		$("#alert-excluir").show(200);
		$("#alert-excluir #excluirSim").attr("data-oque","lado");
		$("#alert-excluir #excluirSim").attr("data-id","<?=$id;?>");
	});
	
	/*****
	** remove Imagens
	*****/
	removerImagem = (function(campo){
		
		// variaveis
		var table = "produto", 
			acao = "excluirImagem", 
			id = <?=$id;?>,
			ajaxStatus = $(".ajaxStatus");
		
		//console.log(campo);
		if(campo=="lado") {
			var ajaxStatus = $(".ajaxStatusLado");
		} else {
			var ajaxStatus = $(".ajaxStatusTopo");
		}
		// post
		ajaxStatus.html('<img src="img/loading.gif" height="20">');
		ajaxStatus.slideDown("fast", function() {
			// Faz requisi&ccedil;&atilde;o ajax
			$.post("codigos/ajax.php", {
			id:id, campo:campo, table:table, acao:acao
			}, function(respostaI) {
				console.log(respostaI);
				ajaxStatus.html(respostaI);
				ajaxStatus.delay(2500).slideUp(800);
			});
		});
	});
	
	// cancelar exclus&atilde;o
	$("#alert-excluir #cancelarExclusao").click(function(){
		$("#alert-excluir").hide(200);
	});
	// confirmar exclus&atilde;o
	$("body").delegate("#alert-excluir #excluirSim","click",function(){ 
		$("#alert-excluir").hide(200);
		
		// tirar da tela
		tirarDaTela = function() {
			if(oque == "imagemAlbum") {
				$(".cadastro-album-excluir[data-id="+dataId+"]").parent().parent().stop().slideUp(300,function(){
					$(".cadastro-album-excluir[data-id="+dataId+"]").parent().parent().remove();
				});
			}
		};
		
		// vars
		var 	dataId		= $("#alert-excluir #excluirSim").attr("data-id"),
			oque		= $("#alert-excluir #excluirSim").attr("data-oque"),
			elemento	= $(".cadastro-album-excluir[data-id="+dataId+"]").parent().parent(),
			acao		= "";
		
		// se tiver ID
		if(dataId>0){
			// excluir do BD
			if(oque == "imagemAlbum") {
				acao="eliminarImagem";
				//
				elemento.slideUp(200,function(){
					elemento.html('<img src="img/loading.gif" height="20"> removendo...');
					elemento.slideDown(200);
				});
				//
				$.post("codigos/moduloImagem.php", {
					acao:acao, dataId:dataId, tabela:"imagem"
				}, function(resposta) { 
					console.log(resposta);
					if(resposta ==1) { // excluiu
						tirarDaTela();
					}
					else { // erro
						window.alert('Erro ao excluir a imagem do produto!');
					}
				});
			}
			else if(oque == "topo" || oque == "lado") {
				removerImagem(oque);
			}
		}
		else {
			if(oque == "imagemAlbum") {
				tirarDaTela();
			}
		}
		//====================
		if(oque == "topo") {
			// limpar forms
			$("#imagem_topo, #legendaTopo").val("");
			// trocar botoes
			$("#botaoAdicionarImagemTopo").show();
			// trocar divs
			$("#imagemPrincipalTopo").slideUp(200);
			$("#imagemPrincipalSimTopo").hide();
			$("#imagemPrincipalNaoTopo").show();
		}
		else if(oque == "lado") {
			// limpar forms
			$("#imagem_lado, #legendaLado").val("");
			// trocar botoes
			$("#botaoAdicionarImagemLado").show();
			// trocar divs
			$("#imagemPrincipalLado").slideUp(200);
			$("#imagemPrincipalSimLado").hide();
			$("#imagemPrincipalNaoLado").show();
		}
	});
	
	/* 
	** Carregar Fotos 
	*/
	// vars
	var numFotos			= 0,
		posicoes		= [], 
		
		// cada uma das fotos do &aacute;lbum
		modelo_fotos_li = "<div class='cadastro-album-li'><div class='cadastro-album-imagem'><img id='foto-li-###' /></div><div class='foto-album-legenda' id='nova-foto-###'><div class='legenda-transparente'><img src='img/loading.gif' height='15' align='absmiddle'> salvando...</div></div></div>";
	
	// clicou no adicionar fotos.
	$("body").delegate(".cadastro-album-novo","click", function(){ 
		$("#fotosToUpload").click();
	});
	
	// selecionou fotos
	$("body").delegate(".cadastro-album-escondido","change", function(){ 
	
		posicoes = []; // limpar as posi&ccedil;őes
		var 	elemento = document.getElementById('fotosToUpload');
		
		for(var i=0; i<elemento.files.length && i<=5; i++){
			numFotos++;
			
			if(i==5){
				window.alert("Selecione 5 fotos de cada vez.");
				ajaxUploadFotos(posicoes);
			}
			else {
				// colocar a div na tela
				fotos_li = modelo_fotos_li.replace(/###/g, numFotos);
				$("#morefilesFotos").append(fotos_li);
				
				// ler a imagem
				readFileDataURL(elemento.files.item(i), 'foto-li-'+numFotos, "fotos");
				
				posicoes[i] = numFotos;
				// chamar o ajax
				if(i==(elemento.files.length-1)){
					ajaxUploadFotos(posicoes);
				}
			}
		}
	});
	
	// pr&eacute;via da imagem
	function readFileDataURL(quem, aonde, tipo){
		var reader	= new FileReader();
		reader.readAsDataURL(quem);
		reader.onload = function(event){
			document.getElementById(aonde).src=event.target.result;
		};
	};
	
	// upload da imagem
	function ajaxUploadFotos(posicoes){
		
		var acao = "incluirFoto",
			idPai = '<?=$id;?>',
			pai   = 'produto',
			prefixo = 'produto';
			
		$.ajaxFileUpload({
			url: 'codigos/moduloImagem.php',
			secureuri: false,
			fileElementId: "fotosToUpload",
			dataType: 'json',
			data: {
				acao:acao, idPai:idPai, pai:pai, prefixo:prefixo
			},
			success: function(data,status) {
				if((typeof(data.error) != 'undefined') && (data.error != '')){
					window.alert("Erro ao salvar a foto " + posicao);
					//console.log(data.error);
				}
				else{
					var quebraDados=data.msg.split("|");
					//console.log(data.msg);
					for(i=0; i<=posicoes.length; i++){
						var idFoto	= quebraDados[i], 
							posicao	= posicoes[i];
							
						$("#nova-foto-"+posicao).html("<a href='javascript:;' title='Excluir foto' class='cadastro-album-excluir' data-id='"+idFoto+"'>x</a><input type='text' data-id='"+idFoto+"' value='' maxlength='100' class='cadastro-album-legenda' placeholder='Legenda'/>");
						if(idFoto>0){
							$("#novas-fotos").val( $("#novas-fotos").val()+idFoto+"," );
						}
					};
					gravaPosicaoFotos();
					reloadLegenda();
				}
			},
			error:function(data,status,e){
				if((typeof(data.error) != 'undefined') && (data.error != '')){
					window.alert("Erro ao salvar a foto " + posicao);
					//console.log(data.error);
				}
			}
		});
		return false;
	};
	
	// mudar posicionamento
	function gravaPosicaoFotos(){
		var ArrId	= [],
			ArrPos	= [];
		//
		for(i=0; i<$(".cadastro-album-excluir").length; i++){
			ArrId[i]=$("a[class=cadastro-album-excluir]:eq("+i+")").attr("data-id");
			ArrPos[i]=i;
		};
		
		$.post("codigos/moduloImagem.php",{
			acao:"mudaPosicao", ArrId:ArrId, ArrPos:ArrPos, tabela:"imagem"
		}, function(resposta){
			//console.log(resposta);
		});
	};
	
	// alterar posicionamento
	$("#morefilesFotos").sortable({ 
		update:function(event,ui){ 
			gravaPosicaoFotos();
		}
	});
	
	// legenda
	var nomeOld		= "", 
		nomeNew		= "", 
		id			= "", 
		podeMudar	= false;
	
	reloadLegenda=(function(){
		$("input[class=cadastro-album-legenda]").focus(function(){
			nomeOld	  = $(this).val();
			podeMudar = true;
		});
		$("input[class=cadastro-album-legenda]").blur(function(){
			nomeNew	= $(this).val();
			id	= $(this).attr("data-id");
			
			if(nomeOld != nomeNew && podeMudar){
				$.post("codigos/moduloImagem.php",{
					acao:"trocaLegenda", id:id, nome:escape(nomeNew), tabela:"imagem", prefixo:"produto"
				}, function(resposta){
					console.log(resposta);
				});
			}
		});
	});
	reloadLegenda();
	
	/* excluir */
	// excluir foto do &aacute;lbum
	$("body").delegate(".cadastro-album-excluir", "click", function(){ 
		$("#alert-excluir").show(200);
		//
		var dataId = $(this).attr("data-id");
		$("#alert-excluir #excluirSim").attr("data-id",dataId);
		$("#alert-excluir #excluirSim").attr("data-oque","imagemAlbum");
	});
	
	/*
	** Remover arquivo
	*/
	$(".removerArquivo").click (function(){
		var acao = "excluirArquivo", 
			idArq = $(this).attr("data-idArquivo"),
			fileArq = $(this).attr("data-fileArquivo"),
			ajaxStatusArq = $(this).parent().parent();
			
		// post
		ajaxStatusArq.slideUp(200, function(){
			ajaxStatusArq.html('<img src="img/loading.gif" height="20">');
			ajaxStatusArq.slideDown(200, function() {
				// Faz requisi&ccedil;&atilde;o ajax
				$.post("codigos/ajax.php", { idArq:idArq, acao:acao }, function(respostaI) {
					ajaxStatusArq.html(respostaI);
					rmvMoreFilesArq(fileArq);
				});
			});
		});
	});
});
</script>