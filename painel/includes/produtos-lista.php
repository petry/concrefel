<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

//Total de registros a serem exibitos por p&aacute;ginas
$registrosPorPagina = 75;

//Listagem
if ((isset($_POST["pesquisa"])  && $_POST["pesquisa"]!="Pesquisar") || isset($_GET["pesquisa"])) {
    $pesquisa = (isset($_POST["pesquisa"])) ? $_POST["pesquisa"] : $_GET["pesquisa"];
	$pesquisa1 = str_replace(" ", "%", antiInjection($pesquisa));
    $where = "titulo LIKE '%$pesquisa1%' OR subtitulo LIKE '%$pesquisa1%'";
}else{
    $where = "";
}
// pagina&ccedil;&atilde;o
if (isset($_GET["pag"]) && is_numeric($_GET["pag"])) {
    $comecar = (($_GET["pag"] - 1) * $registrosPorPagina);
}else{
	$comecar = 0;
}?>
<div class="titulo">
	<h1>Produtos</h1>
    <div class="tituloBotoes">
    	<form action="?pagina=produtos-lista" method="post">
        	<div class="pesquisarFundo"><input type="submit" name="enviar" value='' class="pesquisarBotao" /></div>
        	<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquisar" class="pesquisarCampo" value="<?=$pesquisa;?>" />
        </form>
    	<a href="home.php?pagina=produtos-editar" title="Adicionar novo produto" class="botao botaoAdicionar">Adicionar produto</a>
    </div>
</div>
<div class="spacer50"></div>

<div class="legenda">
	<p>Exibir no site</p>
    <img src="img/legenda-visivel.png" width="18" height="18" />
	<p>Exibir na capa</p>
    <img src="img/legenda-home.png" width="18" height="18" />
</div>

<table>
	<tr>
	  <td class="listaBarraCheck"><input type="checkbox" id="selecionarTodos" title="Selecionar todos os produtos" /></td>
	  <td class="listaBarraTexto" colspan="1">Produto:</td>
	  <td class="listaBarraTexto">Categoria:</td>
	  <td class="listaBarraIcones"><img src="img/legenda-visivel.png" width="18" height="18" /></td>
	    <td class="listaBarraIcones"><img src="img/legenda-home.png" width="18" height="18" /></td>

	</tr>
    <?php
	$consulta = consultar("id,titulo,subtitulo,ativo,capa,categoria", "produto", $where, "categoria ASC, titulo ASC", $comecar, $registrosPorPagina); 
	
	////
	while ($produto	= mysqli_fetch_assoc($consulta)) {
		$catConsulta = consultar("id,nome", "categoria", "id=".$produto['categoria']);
		$cat = mysqli_fetch_assoc($catConsulta);
	?>
  <tr>
	<td class="listaItemCheck"><input type="checkbox" class="check" data-id="<?=$produto['id'];?>" title="Selecionar esse produto" /></td>
	<td class="listaItemTexto">
		<a href="home.php?pagina=produtos-editar&id=<?=$produto['id'];?>" title="Editar produto"><span><?=$produto['titulo']?>
		<?php 
		if ($produto['subtitulo']){
			echo ' &raquo; '.$produto['subtitulo'];
		} ?>
		&nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a>
	</td>
	<td class="listaItemTexto"><?php if ($cat['id'] == 0){ echo "<b>N&atilde;o definida</b>"; } else{ echo $cat['nome']; } ?></td>
	<td class="listaItemIcones ajaxContent">
	  <a href="javascript:;" data-campo="ativo" data-status="<?=$produto['ativo'];?>" data-id="<?=$produto['id'];?>"><img src="img/form-checkbox<?php if($produto['ativo']) { echo "-checked"; } ?>.jpg" width="13" height="13" /></a>
	</td>
	<td class="listaItemIcones ajaxContent">
      	<a href="javascript:;" data-campo="capa" data-status="<?=$produto['capa'];?>" data-id="<?=$produto['id'];?>"><img src="img/form-checkbox<?php if($produto['capa']) { echo "-checked"; } ?>.jpg" width="13" height="13" /></a>
      </td>
	
  </tr>
    <? } ?>
</table>

<div class="divExcluir">
    <input type="button" value="Excluir" class="botaoInput botaoInputExcluir" />
    <span class="ajaxStatusDel"></span>
</div>

<div class="listaPaginacao marginTop10 text12">
<?php 
//Pagina&ccedil;&atilde;o de Resultados
paginar('produto','produtos', $where, $pesquisa, $registrosPorPagina);
?>
</div>

<script type="text/javascript" src="js/_isdesign.js?2"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	ativarOuExcluir({tabela: "produto"}); // fun&ccedil;&atilde;o da isDesign que permite ativar e excluir por ajax
});
</script>