<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

//Total de registros a serem exibitos por p&aacute;ginas
$registrosPorPagina = 75;

$where = "1=1 ";
//Listagem
if ((isset($_POST["pesquisa"])  && $_POST["pesquisa"]!="Pesquisar") || isset($_GET["pesquisa"])) {
	$pesquisa = (isset($_POST["pesquisa"])) ? $_POST["pesquisa"] : $_GET["pesquisa"];
	$pesquisa1 = str_replace(" ", "%", antiInjection($pesquisa));
	$where .= "AND nome LIKE '%$pesquisa1%'";
}
if ((isset($_POST["status"])  && $_POST["status"]!="") || isset($_GET["status"])) {
	$stat = (isset($_POST["status"])) ? $_POST["status"] : $_GET["status"];
	$where .= "AND status = $stat";
}
// pagina&ccedil;&atilde;o
if (isset($_GET["pag"]) && is_numeric($_GET["pag"])) {
    $comecar = (($_GET["pag"] - 1) * $registrosPorPagina);
}else{
	$comecar = 0;
}?>
<div class="titulo">
	<h1>Or&ccedil;amentos</h1>
</div>
<div class="spacer50"></div>
<form action="" method="post">
	<div class="filtro">
		<label for="status" class="filtro-label">Status do or&ccedil;amento:</label>
		<div class="filtro-input">
			<select name="status" id="status">
				<option value="" selected>Todos os or&ccedil;amentos</option>
				<option value="1" <?php if($stat == 1){ echo 'selected';} ?>>Aberto</option>
				<option value="2" <?php if($stat == 2){ echo 'selected';} ?>>Em produ&ccedil;&atilde;o</option>
				<option value="3" <?php if($stat == 3){ echo 'selected';} ?>>Entregue</option>
				<option value="4" <?php if($stat == 4){ echo 'selected';} ?>>Faturado</option>
			</select>
		</div>
	</div>
	<div class="filtro">
		<label for="pesquisa" class="filtro-label">Busca personalizada</label>
		<div class="filtro-input">
			<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquisar" value="<?=$pesquisa;?>" />
		</div>
	</div>
	
	<div class="filtro">
		<input type="submit" name="buscar" id="buscar" value="Aplicar filtros" class="filtro-submit" />
	</div>

</form>
<div class="spacer50"></div>

<table>
	<tr>
	  <td class="listaBarraCheck"><input type="checkbox" id="selecionarTodos" title="Selecionar todos os or&ccedil;amentos" /></td>
	  <td class="listaBarraTexto">Nº Or&ccedil;amento:</td>
	  <td class="listaBarraTexto">Nome:</td>
	  <td class="listaBarraTexto">E-mail:</td>
	  <td class="listaBarraTexto">Telefone:</td>
	  <td class="listaBarraTexto">Data:</td>
	  <td class="listaBarraTexto">Status:</td>
	</tr>
    <?php
	$consulta = consultar("id,nome,email,telefone,observacoes,data,status", "orcamentos", $where, "id DESC", $comecar, $registrosPorPagina); 
	
	////
	while ($orcamento	= mysqli_fetch_assoc($consulta)) {
		
		if		($orcamento['status'] == 1){ 	$status = "Aberto";			}
		else if	($orcamento['status'] == 2){	$status = "Em Produ&ccedil;&atilde;o";		}
		else if	($orcamento['status'] == 3){	$status = "Entregue";		}
		else if	($orcamento['status'] == 4){	$status = "Faturado";		}
	?>
  <tr>
	<td class="listaItemCheck"><input type="checkbox" class="check" data-id="<?=$orcamento['id'];?>" title="Selecionar esse or&ccedil;amento" /></td>
	<td style="cursor:pointer;" data-id="<?=$orcamento['id'];?>" class="listaItemTexto detalhes-orcamentos"><?=$orcamento['id'];?></td>
	<td style="cursor:pointer;" data-id="<?=$orcamento['id'];?>" class="listaItemTexto detalhes-orcamentos"><?=$orcamento['nome'];?></td>
	<td style="cursor:pointer;" data-id="<?=$orcamento['id'];?>" class="listaItemTexto detalhes-orcamentos"><?=$orcamento['email'];?></td>
	<td style="cursor:pointer;" data-id="<?=$orcamento['id'];?>" class="listaItemTexto detalhes-orcamentos"><?=$orcamento['telefone'];?></td>
	<td style="cursor:pointer;" data-id="<?=$orcamento['id'];?>" class="listaItemTexto detalhes-orcamentos"><?=dataEhora($orcamento['data']);?></td>
	<td style="cursor:pointer;" data-id="<?=$orcamento['id'];?>" class="listaItemTexto detalhes-orcamentos"><?=$status;?></td>	
	
  </tr>
    <? } ?>
</table>

<div class="divExcluir">
    <input type="button" value="Excluir" class="botaoInput botaoInputExcluir" />
    <span class="ajaxStatusDel"></span>
</div>

<div class="listaPaginacao marginTop10 text12">
<?php 
//Pagina&ccedil;&atilde;o de Resultados
paginar('orcamentos','orcamentos', $where, $pesquisa, $registrosPorPagina);
?>
</div>

<script type="text/javascript" src="js/_isdesign.js?2"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	ativarOuExcluir({tabela: "orcamentos"}); // fun&ccedil;&atilde;o da isDesign que permite ativar e excluir por ajax
	//
	$(".detalhes-orcamentos").click(function(){
		id = $(this).attr("data-id");
		window.location.href = "?pagina=orcamentos-detalhes&id="+id;
	});
});
</script>