<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 13/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

//Total de registros a serem exibitos por p&aacute;ginas
$registrosPorPagina = 75;
//Listagem
if ((isset($_POST["pesquisa"]) && $_POST["pesquisa"]!="Pesquisar") || isset($_GET["pesquisa"])) {
	$pesquisa = (isset($_POST["pesquisa"])) ? $_POST["pesquisa"] : $_GET["pesquisa"];
	$pesquisa1 = str_replace(" ", "%", antiInjection($pesquisa));
	$where = "nome LIKE '%$pesquisa1%' OR email LIKE '%$pesquisa1%' AND id<>1";
}else{
	$where = "id<>1";
}
// pagina&ccedil;&atilde;o
if (isset($_GET["pag"]) && is_numeric($_GET["pag"])) {
	$comecar = (($_GET["pag"] - 1) * $registrosPorPagina);
}else{
	$comecar = 0;
}
?>
<div class="titulo">
	<h1>Usu&aacute;rios</h1>
	<div class="tituloBotoes">
		<form action="?pagina=usuarios-lista" method="post">
			<div class="pesquisarFundo"><input type="submit" name="enviar" value='' class="pesquisarBotao" /></div>
			<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquisar" class="pesquisarCampo" value="<?=$pesquisa;?>" />
		</form>
		<a href="home.php?pagina=usuarios-editar" title="Adicionar novo usu&aacute;rio" class="botao botaoAdicionar">Adicionar usu&aacute;rio</a>
    </div>
</div>
<div class="spacer50"></div>

<div class="legenda">
	<p>Ativo</p>
	<img src="img/legenda-visivel.png" width="18" height="18" />
</div>

<table>
	<tr>
	  <td class="listaBarraCheck"><input type="checkbox" id="selecionarTodos" title="Selecionar todos os usu&aacute;rios" /></td>
	  <td class="listaBarraTexto">Nome do usu&aacute;rio</td>
	  <td class="listaBarraTexto">Email</td>
	  <td class="listaBarraTexto listaItemIcones"><img src="img/legenda-visivel.png" width="18" height="18" /></td>
	</tr>
    <?php
	$consulta = consultar("id,nome,email,ativo", "login", $where, "nome ASC", $comecar, $registrosPorPagina);
	////
    while($login = mysqli_fetch_assoc($consulta)){
		?>
	<tr>
	  <td class="listaItemCheck"><input type="checkbox" class="check" data-id="<?=$login['id'];?>" title="Selecionar esse usu&aacute;rio" /></td>
	  <td class="listaItemTexto"><a href="home.php?pagina=usuarios-editar&id=<?=$login['id'];?>" title="Editar usu&aacute;rio"><span><?=$login['nome'];?> &nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a></td>
	  <td class="listaItemTexto"><?=$login['email'];?></td>
	  <td class="listaItemIcones ajaxContent">
	  	<a href="javascript:;" data-campo="ativo" data-status="<?=$login['ativo'];?>" data-id="<?=$login['id'];?>"><img src="img/form-checkbox<?php if($login['ativo']) { echo "-checked"; } ?>.jpg" width="13" height="13" /></a>
	  </td>
	</tr>
	<?php } ?>
</table>

<div class="divExcluir">
	<input type="button" value="Excluir" class="botaoInput botaoInputExcluir" />
	<span class="ajaxStatusDel"></span>
</div>

<div class="listaPaginacao marginTop10 text12">
<?php 
//Pagina&ccedil;&atilde;o de Resultados
paginar('login','usuarios', $where, $pesquisa, $registrosPorPagina);
?>
</div>

<script type="text/javascript" src="js/_isdesign.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	ativarOuExcluir({tabela: "login"}); // fun&ccedil;&atilde;o da isDesign que permite ativar e excluir por ajax
});
</script>