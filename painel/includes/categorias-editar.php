<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 07/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');
require_once('codigos/moduloProdutos.php'); //Fun&ccedil;őes e m&eacute;todos do m&oacute;dulo

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

// salvar
if($_POST['id']){
	salvarCategoria();
	exit;
}

//Caso deseje listar determinado registro
if($_GET['id'] && is_numeric($_GET['id'])){
	$edi 		= 1;
	$id			= $_GET['id'];
	$consulta	= consultar("*", "categoria", "id=".$id);
	$dados = mysqli_fetch_assoc($consulta);
} else {
	$id = 's';
	$dados["ativo"] = 1;
}
?>
<h1>Categorias de Produtos</h1>
<form action="" method="post">
  <input name="id" type="hidden" id="id" value="<?=$id;?>" />
  
<div class="bloco marginTop20">
	<div class="padding20">
		<h2>Nome da Categoria:</h2>
		<input type="text" id="nome" name="nome" value='<?=$dados["nome"];?>' placeholder="Nome da Categoria" maxlength="100" class="form-input" />
	</div>
</div>
<div class="bloco marginTop20">
	<div class="padding20">
		<h2>Visibilidade:</h2>
	    <p>&nbsp;</p>
	    <div class="visibilidade visibilidadeBig"><input type="checkbox" name="ativo" id="visibilidadeSite" value="1" <?php if($dados['ativo']){ ?>checked="checked"<?php } ?> />&nbsp; <label for="visibilidadeSite"><img src="img/legenda-visivel.png" width="18" height="18" />&nbsp; Exibir Categoria</label></div>
	    <div class="clearBoth"></div>
	</div>
</div>

<input name="button" type="submit" class="botaoInput width150 marginTop10 float" id="button" value="Salvar" />
</form>