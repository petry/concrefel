<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 13/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');
require_once('codigos/moduloUsuario.php'); //Fun&ccedil;őes e m&eacute;todos do m&oacute;dulo

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

// cadastrar ou editar
if($_POST['id']=="s"){ //Quer dizer que &eacute; um cadastro
	cadastrarUsuario();
	exit;
}else if($_POST['id']>0){
	editarUsuario();
	exit;
}
//Caso deseje listar determinado registro
if($_GET['id'] && is_numeric($_GET['id'])){
	$edi 		= 1;
	$id			= $_GET['id'];
	$consulta	= consultar('*','login', 'id='.$id, '', 0, 0);
	$login = mysqli_fetch_assoc($consulta);
} else {
	$id = 's';
	$login['ativo'] = 1;
}
?>
<h1>Usu&aacute;rios</h1>
<form action="" method="post" id="usuario">
    <input name="id" type="hidden" id="id" value="<?=$id;?>" />
    
    <div class="bloco marginTop20">
        <div class="padding20">
        	<label for="nome" class="label float">Nome:</label> <input type="text" name="nome" id="nome" value='<?=$login['nome'];?>' maxlength="150" class="input float" required />
            <div class="separa"></div>
            
            <label for="email" class="label float">E-mail:</label> <input type="text" name="email" id="email" value='<?=$login['email'];?>' maxlength="150" class="input float" />
            <div class="separa"></div>
            
            <label for="senha" class="label float">Senha:</label> <input type="password" name="senha" id="senha" value='' class="input float" <?php if(!$edi){ ?>required<?php } ?> />
            <div class="separa"></div>
            
            <label for="ativo" class="label float">Ativo: </label><input name="ativo" class="label float" type="checkbox" value="1" <?php if($login['ativo']){ ?>checked="checked"<?php } ?> /> <img src="img/legenda-visivel.png" width="18" height="18" class="float" />
		</div>
		<div class="clearBoth"></div>
	</div>
	
	<div class="bloco marginTop20">
		<div class="padding20">
			<h2>Permiss&otilde;es</h2>
			<p>&nbsp;</p>
			<div class="colunas5">
			<?php
			$consultaM = consultar("*", "menu", "pagina_inicial=1", "nome ASC");
			////
			while ($menu = mysqli_fetch_assoc($consultaM)) {
				?>
				<label><input type="checkbox" name="permissao[]" class="permissao" value="<?=$menu['id'];?>" <?php if(permissaoUsuario($id, $menu['id'])&& $edi){ ?>checked="checked"<?php } ?> /> <?=utf8_encode($menu['nome']); ?></label><br><br>
			<?php } ?>
			</div>
		</div>
	</div>
    <input type="submit" value="Salvar" class="botaoInput width150 marginTop10 float" />
</form>