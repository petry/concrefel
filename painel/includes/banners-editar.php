<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 13/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');
require_once('codigos/moduloBanners.php'); // Fun&ccedil;őes e m&eacute;todos do m&oacute;dulo
$dadosPainel  = dadosConfiguracoes();

//Verifica se o usu&aacute;rio pode acessar este pagina
restritoAcesso(1);

// salvar
if($_POST['id']){ //Quer dizer que &eacute; um cadastro
	salvarBanner();
	exit;
}

//Caso deseje listar determinado registro
if($_GET['id'] && is_numeric($_GET['id'])){
	$edi 		= 1;
	$id			= (int)$_GET['id'];
	$consulta	= consultar("*", "banners", "id=$id");
	if(mysqli_num_rows($consulta)>0){
		$dados = mysqli_fetch_assoc($consulta);
		//
		$dados['dataAgenda']		= dataPadraoBrasileiro($dados['exibirAPartir']);
		$dados['horaAgenda']		= horaPadraoBrasileiro($dados['exibirAPartir']);
		$dados['minutoAgenda']	= minutoPadraoBrasileiro($dados['exibirAPartir']);
	} else {
		echo 'Banner n&atilde;o encontrado.
		<META HTTP-EQUIV="Refresh" CONTENT="5; URL=home.php?pagina=banners-lista">';
		exit;
	}
}
else {
	$id = "s";
	$dados["ativo"] = 1;
	//
	$dados['dataAgenda']		= date("d/m/Y");
	$dados['horaAgenda']		= date("H");
	$dados['minutoAgenda']	= date("i");
}
?>
	
<h1>Banners</h1>
<form action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" id="id" value='<?=$id;?>' />

<div class="bloco marginTop20 padding20">
	<h2>T&iacute;tulo do banner:</h2>
	<input type="text" id="titulo" name="titulo" value='<?=$dados["titulo"];?>' placeholder="Titulo do banner" maxlength="200" class="form-input marginTop5" autocomplete="off"/>
	<h2>Subt&iacute;tulo:</h2>
	<input type="text" id="subtitulo" name="subtitulo" value='<?=$dados["subtitulo"];?>' placeholder="Subtitulo do banner" maxlength="200" class="form-input marginTop5" autocomplete="off"/>
</div>

<div class="bloco marginTop20">
	<div class="padding20">
		<h2>Banner:</h2>
		<p>(720 x 540 pixels)</p>
		<div class='previa-img previa-img-banner'>
			<a href='javascript:;' title='Excluir foto' class='cadastro-foto-excluir cadastro-foto-excluir-2' <?php if($dados['file']!=""){ echo 'style="display:block;"'; } ?>>x</a>
			<img id='previa-img'<?php 
			if($dados['file']!=""){ 
				?> src='../includes/thumb.php?file=../imgs/<?=$dados['file'];?>&w=396&h=500' <?php 
			}
			?>/>
		</div>
		<label for='file' class='form-input marginTop5'>
			<input type='file' id='file' name='file' class='imagem-texto' />
		</label>
	</div>
	<div class="blocoLinha marginTop15"></div>
	
	<div class="padding20 marginTop15">
		<h2>Link:</h2>
		<label for='link' class='form-label'>(p&aacute;gina que abrir&aacute; ao clicarmos no banner / inclua o "<em>http://</em>")</label>
		<input type='text' id="link" name="link" class='form-input form-js marginTop5' value='<?=$dados['link'];?>' placeholder="http://www.exemplo.com.br/"/>
	</div>
</div>

<div class="bloco marginTop20">
	<div class="padding20">
		<h2>Visibilidade:</h2>
		<div class="cadastro-texto">
			<label for="ativo-sim" class="display-block float-left">
				<input type="radio" name="ativo" id="ativo-sim" value='1' <?php if($dados["ativo"]){echo 'checked="checked"';} ?> />&nbsp; Vis&iacute;vel
			</label>
			<label for="ativo-nao" class="marginLeft20 display-block float-left">
				<input type="radio" name="ativo" id="ativo-nao" value='0' <?php if(!$dados["ativo"]){echo 'checked="checked"';} ?>/>&nbsp; Rascunho
			</label>
		</div>
		<div class="clear-both"></div>
	</div>
	
	<div class="blocoLinha marginTop10"></div>
	
	<div class="padding20 marginTop10">
		<h2>Publicar quando:</h2>
		<div class="visibilidade width175">
        	<label for="publicarAgora"><input type="radio" name="agendar" id="publicarAgora" value="0" <?php if(!$edi || ($edi && !$dados['dataAgenda'])){ ?>checked="checked"<?php } ?> class="agendar" /> publicar imediatamente</label>
        </div>
		<div class="visibilidadeSpacer"></div>
		<div class="visibilidade width100">
        	<label for="agendar"><input type="radio" name="agendar" id="agendar" value="1" <?php if($edi && $dados['dataAgenda']){ ?>checked="checked"<?php } ?> class="agendar" /> <img src="img/legenda-relogio.png" width="18" height="18" align="absmiddle" /> agendar</label>
        </div>
		<div class="visibilidadeSpacer"></div>
		<div id="agendador" class="visibilidade width350 <?php if(!$edi || ($edi && !$dados['dataAgenda'])){ ?>displayNone<?php } ?>" >
            Publicar em: 
            <input type="text" name="dataAgenda" id="dataAgenda" class="input width85" value="<?=$dados['dataAgenda'];?>" maxlength="10" /> &nbsp; / &nbsp; 
            <select name="horaAgenda" id="horaAgenda">
                <?php for ($h=0; $h<=23; $h++) { ?>
                    <option value="<?=$h;?>" <?php if($h == $dados['horaAgenda']){ echo 'selected="selected"'; } ?> ><?=$h;?></option>
                <?php } ?>
            </select> h
            <select name="minutoAgenda" id="minutoAgenda">
                <?php for ($m=0; $m<=55; $m=$m+5) { ?>
                    <option value="<?=$m;?>" <?php if($m == $dados['minutoAgenda']){ echo 'selected="selected"'; } ?> ><?=$m;?></option>
                <?php } ?>
            </select> m
        </div>
	    <div class="clearBoth"></div>
	</div>
</div>
<input type="submit" value="Salvar" class="botaoInput width150 marginTop10 float" />
</form>

<script type="text/javascript" language="javascript">
$(document).ready(function(){
	/*
	** Agendar publica&ccedil;&atilde;o
	*/
	$(".agendar").change(function(){
		if($("#agendar").is(":checked")) {
			$("#agendador").slideDown();
		} else {
			$("#agendador").slideUp();
		}
	});
	/*
	** DatePicker
	*/
	$("#dataAgenda").datepicker({
		dateFormat: "dd/mm/yy",
		dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
		dayNames: ['Domingo','Segunda','Terca','Quarta','Quinta','Sexta','S&aacute;bado'],
		monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		nextText: "Pr&oacute;ximo",
		prevText: "Anterior"
	});
	
	var $input_id = $("#id");
	
	// excluir foto do texto
	$(".cadastro-foto-excluir").click(function(){ 
		$("#alert-excluir").show(200);
	});
	
	// cancelar exclus&atilde;o
	$("#alert-excluir #cancelarExclusao").click(function(){
		$("#alert-excluir").hide(200);
	});
	
	// confirmar exclus&atilde;o
	$("body").delegate("#alert-excluir #excluirSim","click",function(){ 
		$("#alert-excluir").hide(200);
		
		// tirar da tela
		tirarDaTela = function() {
			$("#previa-img").attr("src","");
			$(".cadastro-foto-excluir").hide();
		};
		
		// se tiver ID
		if($input_id.val()>0){
			// vars
			var 	dataId	= $input_id.val(),
				acao	= "eliminarImagemBanner";
			
			$.post("codigos/moduloBanners.php", {
				acao:acao, dataId:dataId
			}, function(resposta) { 
				if(resposta ==1) { // excluiu
					tirarDaTela();
				}
				else { // erro
					console.log('Erro ao excluir a imagem!');
				}
			});
		}
		else {
			tirarDaTela();
		}	
	});
	
	// exibir pr&eacute;via da imagem
	$(".imagem-texto").change(function(){ 
		var elemento = document.getElementById('file');
		
		if(elemento.files.length>0){
			readFileDataURL(elemento.files.item(0), 'previa-img');
		}
	});
	
	// ler a imagem
	function readFileDataURL(quem, aonde){
		var reader	= new FileReader();
		reader.readAsDataURL(quem);
		reader.onload = function(event){
			document.getElementById(aonde).src=event.target.result;
			$(".cadastro-foto-excluir").show();
		};
	};
});
</script>