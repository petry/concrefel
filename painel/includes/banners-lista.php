<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 13/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');

//Verifica se o usu&aacute;rio pode acessar esta p&aacute;gina
restritoAcesso(1);

$where = "1=1";
//Filtros
if ((isset($_POST["pesquisa"]) && $_POST["pesquisa"]!="Pesquisar") || isset($_GET["pesquisa"])) {
	$pesquisa = (isset($_POST["pesquisa"])) ? $_POST["pesquisa"] : $_GET["pesquisa"];
	$pesquisa1 = str_replace(" ", "%", antiInjection($pesquisa));
	$where .= " AND (titulo LIKE '%$pesquisa1%' )";
}

//Total de registros a serem exibitos por p&aacute;ginas
$registrosPorPagina = 50;

// total de banners
$consultaTotal = consultar("id", "banners", $where);
$totalPaginas = ceil(@mysqli_num_rows($consultaTotal)/$registrosPorPagina);

// pagina&ccedil;&atilde;o
if (isset($_GET["pag"]) && is_numeric($_GET["pag"])) {
	$pag = $_GET["pag"];
	if ($pag > $totalPaginas) {$pag = $totalPaginas;}
	else if ($pag < 1) {$pag = 1;}
	$comecar = (($pag - 1) * $registrosPorPagina);
}else{
	$comecar = 0;
	$pag = 1;
}
?>

<script>
$(function() {
	// mudar posicionamento
	$(".sortable").sortable({ 
		update:function(event,ui){ 
			gravaPosicaoBanners();
		}
	});
	//
	function gravaPosicaoBanners(){
		var ArrId	= [],
			ArrPos	= [];
		//
		for(i=0; i<$("ul.sortable li").length; i++){
			ArrId[i]=$("ul.sortable li:eq("+i+")").attr("data-id");
			ArrPos[i]=i;
		};
		$.post("codigos/moduloImagem.php",{
			acao:"mudaPosicao", ArrId:ArrId, ArrPos:ArrPos, tabela:"banners"
		}, function(resposta){
			//console.log("Posi&ccedil;&atilde;o alterada.");
		});
	};
			
});
</script>

<div class="titulo">
	<h1>Banners da Capa</h1>
    <div class="tituloBotoes">
    	<form action="?pagina=banners-lista" name="frm_filtros" method="post">
        	<div class="pesquisarFundo"><input type="submit" name="enviar" value='' class="pesquisarBotao" /></div>
        	<input type="text" name="pesquisa" id="pesquisa" placeholder="Pesquisar" class="pesquisarCampo" value="<?=$pesquisa;?>" />
        </form>
    	<a href="home.php?pagina=banners-editar" title="Adicionar novo banner" class="botao botaoAdicionar">Adicionar banner</a>
    </div>
</div>
<div class="spacer50"></div>

<div class="legenda">
	<p>Clique e arraste para alterar ordem de exibi&ccedil;&atilde;o</p>
	<img src="img/cursor-move2.png" width="18" height="18" />
	<p>Ativo</p>
	<img src="img/legenda-visivel.png" width="15" height="15" />
</div>

<ul class="sortable">
	<li class="sortableHead" style="height:40px; border:none;">
	  <div class="listaBarraCheck sortableCheck"><input type="checkbox" id="selecionarTodos" title="Selecionar todos os banners" /></div>
	  <div class="listaBarraTexto sortableTexto">Banner</div>
	  <div class="listaBarraTexto sortableTexto">T&iacute;tulo</div>
	  <div class="listaBarraIcones sortableIcones"><img src="img/cursor-move2.png" width="15" height="15" /></div>
	  <div class="listaBarraIcones sortableIcones"><img src="img/legenda-visivel.png" width="18" height="18" /></div>
	</li>
	<?php
	$consulta = consultar("*", "banners", $where, "pos ASC", $comecar, $registrosPorPagina);
	////
	while ($banner	= mysqli_fetch_assoc($consulta)) {
		?>
		<li data-id="<?=$banner['id'];?>">
			<div class="listaItemCheck sortableCheck"><input type="checkbox" class="check" data-id="<?=$banner['id'];?>" title="Selecionar esse banner" /></div>
			
			<div class="listaItemTexto sortableImage">
				<?php if($banner['file']!=""){ ?>
				<a href="home.php?pagina=banners-editar&id=<?=$banner['id'];?>" title="Editar">
					<img src='../includes/thumb.php?file=../imgs/<?=$banner['file'];?>&w=200&h=200' width="150" />
					<span>&nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a>
					<?php } ?>
			</div>
			
			<div class="listaItemTexto sortableTexto"><a href="home.php?pagina=banners-editar&id=<?=$banner['id'];?>" title="Editar">
				<span> <?=$banner['titulo'];?> &nbsp;<img src="img/fundo-editar.png" width="10" height="10" /></span></a>
			</div>
						
			<div class="listaBarraIcones sortableIcones"><img src="img/cursor-move2.png" width="15" height="15" alt="Clique e arraste para mudar o posicionamento" /></div>
			<div class="listaItemIcones sortableIcones ajaxContent">
				<a href="javascript:;" data-campo="ativo" data-status="<?=$banner['ativo'];?>" data-id="<?=$banner['id'];?>" title="Ativo"><img src="img/form-checkbox<?php if($banner['ativo']) { echo "-checked"; } ?>.jpg" width="13" height="13" /></a>
			</div>
		</li>
	<?php
	} // while
	?>
</ul>

<div class="divExcluir">
	<input type="button" value="Excluir" class="botaoInput botaoInputExcluir" />
	<span class="ajaxStatusDel"></span>
</div>
<div class="paginacao">
<?php 
/* PAGINA&Ccedil;ĂO */ 
if($totalPaginas>1) { ?>
	<?php if($pag>1){ ?>
	<a href="javascript:paginacao(<?= $pag-1; ?>);" class="paginacao-botao paginacao-botao-anterior" title="P&aacute;gina anterior">Ť Anterior</a>
	<?php } 
	if($pag<$totalPaginas){ ?>
	<a href="javascript:paginacao(<?= $pag+1; ?>);" class="paginacao-botao paginacao-botao-proxima" title="Pr&oacute;xima p&aacute;gina">Pr&oacute;xima ť</a>
	<?php } ?>
	
	<div class="paginacao-input">
		<input type="text" name="paginacao" id="paginacao" class="paginacao-input-input" value="<?= (isset($pag)) ? $pag : 1; ?>" />
		<div class="paginacao-input-texto">de <?= $totalPaginas;?></div>
	</div>
<?php } ?>
</div>

<script type="text/javascript" src="js/_isdesign.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready (function () {
	ativarOuExcluir({tabela: "banners"}); // fun&ccedil;&atilde;o da isDesign que permite ativar e excluir por ajax
});

/* 
** pagina&ccedil;&atilde;o 
*/
$("#paginacao").keypress(function(e) {
	if (e.which === 13) {
		if (this.value >= 1 && this.value <= <?= $totalPaginas;?>) {
			document.frm_filtros.action = ["home.php?pagina=banners-lista&pag=", $("#paginacao").val()].join("");;
			document.frm_filtros.submit();
		}
	} else if( e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
});
// A's
function paginacao(pagina){
	document.frm_filtros.action = ["home.php?pagina=banners-lista&pag=", pagina].join("");;
	document.frm_filtros.submit();
}
/* pagina&ccedil;&atilde;o */
</script>