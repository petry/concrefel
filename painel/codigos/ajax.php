<?php header ('Content-type: text/html; charset=ISO-8859-1');
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 10/04/15
Vers&atilde;o: 0.1
*/

if (file_exists('codigos/funcoesPainel.php')) {
	require_once('codigos/funcoesPainel.php'); 
}else{
	require_once('../codigos/funcoesPainel.php'); 
}
$dadosPainel  = dadosConfiguracoes();

//
if(isset($_POST['acao'])){
	$acao = $_POST["acao"];
}

//---------ATIVAR---------//

if($acao == "ativar") {
	//buscar os campos
	$id 	= $_POST["id"];
	$campo 	= $_POST["campo"];
	$status = $_POST["status"];
	($status==1) ? $status=0 : $status=1;
	$table 	= $_POST["table"];
	
	$conex	= conectaBD();
	$update = "UPDATE $table SET $campo='$status' WHERE id='$id'";
	
	$bdconect1 = mysqli_query($conex, $update);
	salvaLog("O registro $id foi alterado para $campo = $status na tabela $table."); //logs
	mysqli_close($conex);
	
	if ($bdconect1 > 0) { 
		?><a href="javascript:;" data-campo="<?php echo $campo; ?>" data-status="<?php echo $status; ?>" data-id="<?php echo $id; ?>"><img src="img/form-checkbox<?php if($status) { echo "-checked"; } ?>.jpg" width="13" height="13" align="absmiddle" /></a><?php 
	}
}

//---------EXCLUIR UMA IMAGEM PRINCIPAL---------//

if($acao == "excluirImagem") {
	//buscar os campos
	$id		= $_POST["id"];
	$campo	= $_POST["campo"];
	$table	= $_POST["table"];
	//
	if ($table == 'noticia' || $table == 'texto' || $table == 'produto') {
		// exclui do FTP
		$consulta	= consultar("imagem_$campo", $table, "id=$id");
		$dados		= mysqli_fetch_assoc($consulta);
		
		$imagem		= $dados["imagem_".$campo];
		@unlink("../../imgs/".$imagem);
		
		// apagar do BD
		$conex	= conectaBD();
		$update = "UPDATE $table SET imagem_$campo='', legenda_$campo='' where id='$id'";
		$bdconect = mysqli_query($conex, $update);
		mysqli_close($conex);
		
		// retorno
		if($bdconect){
			echo "Imagem excluida com sucesso!";
		}
	}
	
}

//---------EXCLUIR UM ARQUIVO---------//

if($acao == "excluirArquivo") {
	//buscar os campos
	$idArq	= $_POST["idArq"];
	
	// apagar do ftp	
	$consultaArq = consultar("arquivo", "arquivo", "id='$idArq' ");
	$dados = mysqli_fetch_assoc($consultaArq);
	$file = $dados['arquivo'];
	// apaga do FTP
	if (file_exists("../../imgs/$file")) {
		@unlink("../../imgs/$file");
	}else if (file_exists("../imgs/$file")) {
		@unlink("../imgs/$file");
	}
	
	// deletar  do BD
	$conex = conectaBD();
	$update = "delete from arquivo where id='$idArq' ";
	$bdconect1 = mysqli_query($conex, $update);
	// logs
	if($bdconect1){
		echo "Arquivo excluido com sucesso!";
	}
}

//---------EXCLUIR---------//

if($acao == "excluir") {
	//buscar os campos
	$excluir 	= $_POST["excluir"];
	$qtdExcluir = count($excluir);
	$table 		= $_POST["table"];
	//salvaLog("vamos excluir uma ".$table."!");
	
	switch($table){	
	
		// excluir banners
		case 'banners':
			for($l=0;$l<$qtdExcluir;$l++){
				$id = $excluir[$l];
				$consultaParaEliminar = consultar("file", "banners", "id=$id ");
				$pega = mysqli_fetch_assoc($consultaParaEliminar);
				$file = $pega['file']; 
				@unlink("../../imgs/".$file);
				$excluiu = excluirRegistro("banners", "id", $id);
			}
			if($excluiu){
				echo "excluido";
			}
			break;
			
		//excluir categoria	
		case 'categoria':
			$conex	= conectaBD();
			for($l=0;$l<$qtdExcluir;$l++){
				$id = $excluir[$l];
				$update = "UPDATE produto SET categoria=0 WHERE categoria = $id ";
				$execUp = mysqli_query($conex, $update);
				$excluiu = excluirRegistro("categoria", "id", $id);
			}
			if($excluiu){
				echo "excluido";
			}
			mysqli_close($conex);
			break;
			
		// Excluir produto 
		case 'produto':
			if (file_exists('codigos/moduloArquivo.php')) {
				require_once('codigos/moduloArquivo.php');
				require_once('codigos/moduloVideo.php');
			}else{
				require_once('../codigos/moduloArquivo.php');
				require_once('../codigos/moduloVideo.php');
			}
		
			for($l=0;$l<$qtdExcluir;$l++){
				$id = $excluir[$l];
			
				//Exclui as imagens do produto
				$consulta	= consultar("file", "imagem", "pai='produto' AND idPai=$id ");
				while ($dados = mysqli_fetch_assoc($consulta)) {
					$file		= $dados['file'];
					// apaga do FTP
					@unlink("../../imgs/".$file);
				}
				// apaga do BD
				$conex = conectaBD();
				$query = "DELETE FROM imagem where pai='produto' AND idPai=$id ";
				$delete = mysqli_query($conex, $query);
				
				// excluir rela&ccedil;&otilde;es
				excluirArquivo('produto', $id);
				excluirVideo('produto', $id);
				excluirRegistro("cor_produto","idProduto",$id);
				
				$consultaImgProduto = consultar("imagem_topo,imagem_lado", "produto", "id=$id");
				$faz = mysqli_fetch_assoc($consultaImgProduto);
				//apaga o ftp das imgs que est&atilde;o salvas dentro do produto
				@unlink("../../imgs/".$faz['imagem_topo']);
				@unlink("../../imgs/".$faz['imagem_lado']);
				// excluir o produto do BD
				$excluiu = excluirRegistro("produto", "id", $id);
				mysqli_close($conex);
			}
			if($excluiu){
				echo "excluido";
			}
			break;
		
		//Excluir noticia
		case 'noticia': 
			if (file_exists('codigos/moduloArquivo.php')) {
				require_once('codigos/moduloArquivo.php');
				require_once('codigos/moduloVideo.php');
			}else{
				require_once('../codigos/moduloArquivo.php');
				require_once('../codigos/moduloVideo.php');
			}
			
			for($l=0;$l<$qtdExcluir;$l++){
				$id	= $excluir[$l];
				
				//consultar dado
				$consulta	= consultar("file", "imagem", "pai='noticia' AND idPai=$id ");
				while($dados = mysqli_fetch_assoc($consulta)){
				
					$imagem		= $dados['file'];
					//Exclui a imagem da not&iacute;cia
					@unlink("../../imgs/".$imagem);
				}
				$conex = conectaBD();
				$queryD = "DELETE FROM imagem WHERE pai='noticia' AND idPai=$id ";
				$execQueryD = mysqli_query($conex, $queryD);
				
				// excluir rela&ccedil;&otilde;es
				excluirArquivo('noticia', $id);
				excluirVideo('noticia', $id);
				
				$consultaImgNoticia = consultar("imagem_topo,imagem_lado", "noticia", "id = $id");
				$pegaImgs = mysqli_fetch_assoc($consultaImgNoticia);
				@unlink("../../imgs/".$pegaImgs['imagem_topo']);
				@unlink("../../imgs/".$pegaImgs['imagem_lado']);
				//excluir o registro
				$excluiu = excluirRegistro("noticia", "id", $id);
				mysqli_close($conex);
			}
			if($excluiu){
				echo "excluido";
			}
			break;
		
		//Caso seja a exclus&atilde;o de um usu&aacute;rio
		case 'login':
			for($l=0;$l<$qtdExcluir;$l++){
				$id = $excluir[$l];
				// excluir rela&ccedil;&otilde;es
				excluirRegistro("permissao", "idLogin", $id);
				// excluir 
				$excluiu = excluirRegistro("login", "id", $id);
			}
			if($excluiu){
				echo "excluido";
			}
			break;
		
		//Caso seja a exclus&atilde;o de um pedido
		case 'orcamentos':
			for($l=0;$l<$qtdExcluir;$l++){
				$id = $excluir[$l];
				//itens
				excluirRegistro("orcamentos_itens","idOrcamento",$id);
				//dados 
				$excluiu = excluirRegistro("orcamentos","id",$id);
			}
			if($excluiu){
				echo "excluido";
			}
			break;
		
		case 'cor':
			for($l=0;$l<$qtdExcluir;$l++){
				$id = $excluir[$l];
				//dados 
				excluirRegistro("cor_produto","idCor",$id);
				
				$excluiu = excluirRegistro("cor","id",$id);
			}
			if($excluiu){
				echo "excluido";
			}
			break;
		
		
			
	}
}
//---------CONFIGURACOES---------// 

if($acao == "config") {
	$valor	= $_POST["valor"];
	$coluna	= $_POST["coluna"];
	
	$atualizarConfigs = alterarConfiguracoes($valor, $coluna);
	if($atualizarConfigs){
		echo "ok";
	}
}
//
if($acao == "trocarStatus"){
	$table	= $_POST['table'];
	$campo	= $_POST['campo'];
	$status	= $_POST['status'];
	$id		= $_POST['id'];
	
	$conex = conectaBD();
	$query = "UPDATE $table SET $campo='$status' WHERE id=$id";
	mysqli_query($conex, $query);
	mysqli_close($conex);
}
//---------//---------//
?>