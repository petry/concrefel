<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 10/04/15
Vers&atilde;o: 0.1
*/

// fun&ccedil;&atilde;o para gravar uma imagem
require('../vendor/autoload.php');

$s3 = Aws\S3\S3Client::factory();
$bucket = getenv('S3_BUCKET')?: die('No "S3_BUCKET" config var in found in env!');

function gravaImagem($id,$tabela,$legenda,$tipoImagem,$tmpArquivo,$campo="",$prefixo="",$pai="",$idPai="",$maxlargura=820,$maxaltura=820,$qualidade=80){
	//Se houver uma imagem nova para cadastrar

	if($tipoImagem!=''){

		if ($legenda == "") {
			$legenda = $prefixo;
		}
		$extensao = extensaoPadrao($tipoImagem);

		// gera o nome da imagem
		$nomeImagem 	= $prefixo."/".geraUrlLimpa($legenda).$extensao;
		$nomeImagem 	= verNomeImagem($nomeImagem,$tabela,$campo); // ver se esse nome j&aacute; existe.

		// gravar em qual pasta?
		$destinoImagem = "../imgs/$nomeImagem";

		// gravar no BD
		$conex = conectaBD();
		if($id !=""){
			if($campo == "topo" || $campo == "lado"){
				 $query = "UPDATE $tabela SET imagem_$campo='$nomeImagem', legenda_$campo='$legenda' WHERE id=$id";
			}
			else if($tabela == 'banners'){
				$query = "UPDATE $tabela SET file='$nomeImagem' WHERE id='$id'";
			} else {
				$query = "UPDATE $tabela SET file='$nomeImagem', legenda='$legenda' WHERE id='$id'";
			}
			$sqlAtualizacao = @mysqli_query($conex, $query);;
		}
		else {
			$query = "INSERT INTO $tabela (file, legenda, idPai, pai) VALUES ('$nomeImagem', '$legenda', '$idPai', '$pai') ";
			$destinoImagem = "../../imgs/$nomeImagem";

			$sqlAtualizacao = @mysqli_query($conex, $query);;
			$idImagem = mysqli_insert_id($conex);
		}

		$upload = $s3->upload($bucket, $nomeImagem, fopen($tmpArquivo, 'rb'), 'public-read');

		// move_uploaded_file($tmpArquivo, "$destinoImagem");
		//salvaLog($tmpArquivo."<-tmpArquivo|destinoImagem->".$destinoImagem);

		// redimensionar
		redimensionaImagem($destinoImagem, $destinoImagem, $maxlargura, $maxaltura, $qualidade);
		//
		mysqli_close($conex);
		return $idImagem;
	}
}
//==================//

// fun&ccedil;&atilde;o para atualizar o nome de uma imagem
function atualizaNomeImagem($id,$tabela,$legenda,$campo="",$prefixo=""){

	if($campo == "topo" || $campo == "lado"){
		$consulta		= consultar("imagem_".$campo.",legenda_".$campo, $tabela, 'id='.$id);
		$dados			= mysqli_fetch_assoc($consulta);
		$legendaAntiga  = $dados["legenda_".$campo];
		$imagemAntiga	= $dados["imagem_".$campo];
	}
	else if($tabela == "banners"){
		$consulta		= consultar("file,titulo", "banners", 'id='.$id);
		$dados			= mysqli_fetch_assoc($consulta);
		$legendaAntiga  = $dados["titulo"];
		$imagemAntiga	= $dados["file"];
	}
	else{
		$consulta		= consultar("file,legenda", $tabela, 'id='.$id);
		$dados			= mysqli_fetch_assoc($consulta);
		$legendaAntiga = $dados['legenda'];
		$imagemAntiga	= $dados['file'];
	}

	// ver se existia uma legenda antiga e se mudou esta legenda
	if ($legendaAntiga != $legenda) {
		// descobrir o nome da imagem antiga
		$nomeAntiga		= substr($imagemAntiga, 0, -4); // sem a extens&atilde;o (removi os ultimos 4 caracteres)

		// gera o nome da imagem nova
		$nomeImagem 	= $prefixo."/".geraUrlLimpa($legenda).extensaoPadrao($tipoImagem);
		$nomeImagem	= verNomeImagem($nomeImagem,$tabela,$campo);// ver se esse nome j&aacute; existe.

		// Substitui 'nome-antigo' por 'nome-novo'
		$novo_nome = str_replace($nomeAntiga, $nomeImagem, $imagemAntiga);

		// ver se esse nome j&aacute; existe.
		$novo_nome 	= verNomeImagem($novo_nome,$tabela,$campo);

		// Renomea o arquivo na pasta
		if (file_exists("../imgs/".$imagemAntiga)) {
			$renomear = rename("../imgs/".$imagemAntiga, "../imgs/".$novo_nome);
		}
		else if (file_exists("../../imgs/".$imagemAntiga)) {
			$renomear = rename("../../imgs/".$imagemAntiga, "../../imgs/".$novo_nome);
		}
		if ($renomear) {
			$conex	= conectaBD();
			// Renomea o arquivo no BD
			if($campo != ""){
				$query = "UPDATE $tabela SET imagem_$campo='$novo_nome',legenda_$campo='$legenda' WHERE id='$id'";
				//salvaLog($query);
			}
			else if($tabela == 'banners'){
				$query = "UPDATE $tabela SET titulo='$legenda',file='$novo_nome' WHERE id='$id'";
			} else {
				$query = "UPDATE $tabela SET file='$novo_nome', legenda='$legenda' WHERE id='$id'";
			}
			$sqlAtualizacao = @mysqli_query($conex, $query);
			mysqli_close($conex);
		}
	}
}
//==================//

//Fun&ccedil;&atilde;o para redimensionar uma imagem
function redimensionaImagem($origem,$destino,$maxlargura=820,$maxaltura=820,$qualidade=80){
	if(!strstr($origem,"http") && !file_exists($origem)){
		echo("Arquivo de origem da imagem inexistente");
		return false;
	}
	$ext = strtolower(end(explode('.', $origem)));
	if($ext == "jpg" || $ext == "jpeg"){
		$img_origem = @imagecreatefromjpeg($origem);
	}elseif ($ext == "gif"){
		$img_origem = @imagecreatefromgif($origem);
	}elseif ($ext == "png"){
		$img_origem = @imagecreatefrompng($origem);
	}
	if(!$img_origem){
		//echo("Erro ao carregar a imagem, formato n&atilde;o suportado.");
		return false;
	}
	$alt_origem = imagesy($img_origem);
	$lar_origem = imagesx($img_origem);
	$escala = min($maxaltura/$alt_origem, $maxlargura/$lar_origem);

	if($escala < 1){ // redimensiona
		$alt_destino = floor($escala*$alt_origem);
		$lar_destino = floor($escala*$lar_origem);
	}else { // nao redimensiona
		$alt_destino = $alt_origem;
		$lar_destino = $lar_origem;
	}

	// transparente
	$isTrueColor = imageistruecolor($img_origem);
	if ($isTrueColor){
		$img_destino = imagecreatetruecolor($lar_destino,$alt_destino);
		imagealphablending($img_destino, false);
		imagesavealpha($img_destino,true);
	}else{
		$img_destino = imagecreate($lar_destino,$alt_destino);
		imagealphablending($img_destino,false);
		$transparent = imagecolorallocatealpha($img_destino,0,0,0,127);
		imagefill($img_destino,0,0,$transparent);
		imagesavealpha($img_destino,true);
		imagealphablending($img_destino,true);
	}
	// fim transparente

	// Redimensiona
	imagecopyresampled($img_destino, $img_origem, 0, 0, 0, 0, $lar_destino, $alt_destino, $lar_origem, $alt_origem);
	imagedestroy($img_origem);

	$ext = strtolower(end(explode('.', $destino)));
	if($ext == "jpg" || $ext == "jpeg"){
		imagejpeg($img_destino,$destino,$qualidade);
		return true;
	}elseif ($ext == "gif"){
		imagepng($img_destino,$destino);
		return true;
	}elseif ($ext == "png"){
		imagepng($img_destino,$destino);
		return true;
	}else {
		echo("Formato de destino nao suportado.");
		return false;
	}
}
//====================================//

$acao = $_POST["acao"];
if($acao){
	require_once('../codigos/funcoesPainel.php'); //Fun&ccedil;őes padrőes do sistema
}
//====================================//

// Fun&ccedil;&atilde;o para incluir uma nova foto
if($acao == "incluirFoto") {

	$pai		= aspasPHP($_POST['pai']);
	$idPai		= (int)$_POST['idPai'];
	$prefixo	= aspasPHP($_POST['prefixo']);
	$upload		= "fotosToUpload";
	$error		= "";
	$msg		= "";

	// for do multiple
	for($l=0; $l<count($_FILES[$upload]); $l++){

		// imagem est&aacute; vazia
		if(empty($_FILES[$upload]['tmp_name'][$l]) || $_FILES[$upload]['tmp_name'][$l] == 'none'){
			//$error .= ' Nenhum arquivo foi uploaded na posicao: '.$l.' / ';
		}
		else {
			//buscar os campos
			$tipoImagem		= $_FILES[$upload]['type'][$l];
			$tmpName		= $_FILES[$upload]['tmp_name'][$l];
			$legenda		= 'Foto';

			// go
			$idImagem = gravaImagem('','imagem',$legenda,$tipoImagem,$tmpName,'',$prefixo,$pai,$idPai,900,900,80);

			// mensagem
			$msg .= $idImagem."|";
		}
	}
	// retorno
	echo "{";
	echo "error: '" . $error . "',\n";
	echo "msg: '" . $msg . "'\n";
	echo "}";
	//salvaLog(
	//"{ error: '" . $error . "',\n msg: '" . $msg . "'\n }"
	//);
}
//====================================//

// Fun&ccedil;&atilde;o para trocar a posi&ccedil;&atilde;o
else if($acao == "mudaPosicao") {
	//
	$ArrId = $_POST['ArrId'];
	$ArrPos = $_POST['ArrPos'];
	$tabela = $_POST['tabela'];

	$conex = conectaBD();
	// for
	for($l=0; $l<count($ArrId); $l++){
		//Edi&ccedil;&atilde;o dos dados
		$query = "UPDATE $tabela SET pos='$ArrPos[$l]' WHERE id='$ArrId[$l]'";
		mysqli_query($conex, $query);
	}
	mysqli_close($conex);
}
//====================================//

// Fun&ccedil;&atilde;o para trocar a legenda
else if($acao == "trocaLegenda") {
	//
	$id			= aspasPHP(rawurldecode($_POST["id"]));
	$nome		= aspasPHP(rawurldecode($_POST["nome"]));
	$tabela		= aspasPHP(rawurldecode($_POST["tabela"]));
	$prefixo	= aspasPHP(rawurldecode($_POST["prefixo"]));
	$nome		= utf8_encode($nome);
	//
	atualizaNomeImagem($id,$tabela,$nome,"",$prefixo);
}
//Fun&ccedil;&atilde;o para eliminar imagens do produto
else if($acao == "eliminarImagem") {
	//
	$id	 = (int)$_POST['dataId'];
	$tabela = $_POST['tabela'];
	//
	$consulta	= consultar('file', $tabela, 'id='.$id);
	$dados 		= mysqli_fetch_assoc($consulta);
	$file		= $dados['file'];

	// apaga do FTP
	if (file_exists("../../imgs/$file")) {
		@unlink("../../imgs/$file");
	}else if (file_exists("../imgs/$file")) {
		@unlink("../imgs/$file");
	}

	// apaga do BD
	$conex = conectaBD();
	$query = "DELETE FROM $tabela where id=$id ";
	$delete = mysqli_query($conex, $query);
	mysqli_close($conex);

	if($delete){
		echo '1';
	}
}
//====================================//
?>
