<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 10/04/15
Vers&atilde;o: 0.1
*/
require_once('codigos/funcoesPainel.php'); //Fun&ccedil;őes padrőes do sistema
require_once('codigos/moduloImagem.php'); //M&oacute;dulo para gerenciamento de imagens
require_once('codigos/moduloArquivo.php'); //M&oacute;dulo para gerenciamento de arquivos
require_once('codigos/moduloVideo.php'); //M&oacute;dulo para o gerenciamento de v&iacute;deos

/* 
** TEXTO
*/
//Fun&ccedil;ao para editar um texto
function salvarTexto(){
	//
	$id				= (int)$_POST['id'];
	$titulo 			= aspasPHP($_POST['titulo']);
	vazio($titulo, "Titulo da pagina", "Preencher o titulo da pagina");
	//
	$subtitulo		= aspasPHP($_POST['subtitulo']);
	if($subtitulo 	== "Subtitulo da pagina"){ $subtitulo = ""; }
	$titulo1		= aspasPHP($_POST['titulo1']);
	if($titulo1		== "Titulo do texto"){ $titulo1 = ""; }
	$texto			= aspasSimples($_POST['texto']);
	
	$imagemTopo		= $_FILES['imagem_topo'];
	$tipoImagemTopo	= $_FILES['imagem_topo']['type'];
	$legendaLado	= aspasPHP($_POST['legendaLado']); 
	if ($legendaLado=="") { $legendaLado=$titulo; }
	
	$imagemLado		= $_FILES['imagem_lado'];
	$tipoImagemLado	= $_FILES['imagem_lado']['type'];
	$legendaTopo	= aspasPHP($_POST['legendaTopo']);
	if ($legendaTopo=="") { $legendaTopo=$titulo; }
	
	if(!$tipoImagemTopo){
		$imagemTopo 	= NULL;
		$tipoImagemTopo = NULL;
	}
	if(!$tipoImagemLado){
		$imagemLado 	= NULL;
		$tipoImagemLado = NULL;
	}
	//	
	
	$conex = conectaBD();
	$query = "UPDATE texto SET titulo='$titulo', subtitulo='$subtitulo', titulo_1='$titulo1', texto='$texto' WHERE id='$id' ";
	$sqlAtualizacao = @mysqli_query($conex, $query);
	
	if($sqlAtualizacao){
		
		//atualizar nome da imagem 
		atualizaNomeImagem($id,'texto',$legendaTopo,'topo','texto');
		atualizaNomeImagem($id,'texto',$legendaLado,'lado','texto');
		
		//Se houver uma imagem nova para cadastrar
		gravaImagem($id,'texto',$legendaTopo,$tipoImagemTopo,$imagemTopo['tmp_name'],'topo','texto','','',1400,'',90);	
		gravaImagem($id,'texto',$legendaLado,$tipoImagemLado,$imagemLado['tmp_name'],'lado','texto');
		
		//Atualiza os outros dados
		editarArquivo();
		cadastrarArquivo("texto", $id);
		excluirVideo("texto", $id);
		cadastrarVideo("texto", $id);
	} 
	//
	$msg_tipo = salvaLog2($sqlAtualizacao, 'salvo', 'texto', $titulo, 'textos');
	mysqli_close($conex);
}
?>