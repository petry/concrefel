<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 30/03/15
VERSAO: 0.1
*/
//====================================//

//Sessoes para autentica&ccedil;&atilde;o
define("SESSAO_CHAVE", "_isDesign_Concrefel_2015_");

$dadosPainel = dadosConfiguracoes();
//====================================//
//Conex&atilde;o com o banco de dados
/* function conectaBD(){
	//variaveis para conex&atilde;o com o banco de dados e servidor.
	$host   =  "localhost";
	$usuario=  "concrefel";
	$senha  =  "8ytPv#94";
	$banco  =  "concrefel";
		
	$conexaoBanco = mysql_connect($host,$usuario,$senha) or die(mysql_error());
	$conexaoBanco = mysql_select_db($banco) or die ("N&atilde;o foi possivel conectar com o banco de dados!");
	//------------------------------------------------
	if($conexaoBanco){ //Se conseguiu conectar com o banco de dados
		return 1;
	}else{
		return 0;
	}
} */
//Conex&atilde;o com o banco de dados
function conectaBD(){
	$url = parse_url(getenv("CLEARDB_DATABASE_URL"));
	$host   =  $url["host"];
	$usuario=  $url["user"];
	$senha  =  $url["pass"];
	$banco  =  substr($url["path"], 1);
	
	$conex = mysqli_connect($host, $usuario, $senha, $banco);
	//------------------------------------------------
	// check connection
	if (mysqli_connect_errno()) {
		printf("Nao conseguiu conectar com o banco de dados. Erro: %s\n", mysqli_connect_error());
		exit();
	}else{
		return $conex;
	}
}

//====================================//

//Salvar LOG // sem verificar
function salvaLog($mensagem) {
	@session_start();
	$ip			= $_SERVER['REMOTE_ADDR']; // Salva o IP do visitante
	$login		= $_SESSION[SESSAO_CHAVE];
	$navegador	= "Usando: ".qualNavegador();
	
	// para poder inserir a mensagem no banco sem ter problemas com aspas e outros caracteres
	//$mensagem = mysqli_escape_string($mensagem);
	
	$conex	= conectaBD();
	$query	= "INSERT INTO log (ip, usuario, mensagem, extras) VALUES ('$ip','$login','$mensagem','$navegador')";
	$sql	= mysqli_query($conex, $query);
	mysqli_close($conex);
	
	return	$sql;
}
//====================================//

//Salvar LOG (vers&atilde;o 2 // verifica se deu certo ou n&atilde;o)
function salvaLog2($consulta, $acao, $tabela, $id, $redirect="") {
	$ip			= $_SERVER['REMOTE_ADDR']; // IP do visitante
	$login		= $_SESSION[SESSAO_CHAVE];
	$navegador	= "Usando: ".qualNavegador();
	//$acao 	= ucfirst($acao);
	$tabela		= ucfirst($tabela);
	
	// verbalizar
	if($acao == 'atualizada' || $acao == 'atualizado') {
		$verbo == 'atualizar';
	}
	else if($acao == 'cadastrada' || $acao == 'cadastrado'){
		$verbo == 'cadastrar';
	}
	else { 
		$verbo = $acao;
	}
	
	// ver se deu certo
	if ($consulta) {
		$mensagem = "$tabela $acao com sucesso! ($id)";
		$msg_tipo = 1;
	} else {
		$mensagem = "Erro ao $verbo $tabela. Tente novamente. ($id)";
		$msg_tipo = 2;
	}
	
	// gravar no BD
	$conex = conectaBD();
	$sql = "INSERT INTO log (ip, usuario, mensagem, extras) VALUES ('$ip','$login','$mensagem','$navegador')";
	mysqli_query($conex, $query);
	mysqli_close($conex);
	
	// exibir a mensagem para o internauta
	$_SESSION['mensagem'] = $mensagem;
	
	// redirect
	if($redirect != "") {
		?>
		<SCRIPT>
			location.href="home.php?pagina=<?=$redirect;?>-lista&msg=<?=$msg_tipo;?>";
		</SCRIPT>
		<?php
	}
}
//====================================//

//Restringe o Acesso
function restritoAcesso($verPermissao=0){
	$dadosPainel  = dadosConfiguracoes();
	$idUsuario = $_SESSION[SESSAO_CHAVE];
	$pagina_ativa = consultaMenuAtivo(antiInjection($_GET["pagina"]));
	
	// 1ş passo: ver se tem sess&atilde;o
	if (!isset($_SESSION[SESSAO_CHAVE])) {
		session_destroy();
		header("Location: sair.php");
		exit;
	}
	
	// 2ş passo: pemiss&atilde;o
	if($verPermissao) {
		if($idUsuario!=1){ //  user 1 acessa tudo
			$conex	= conectaBD();
			$query = "SELECT * FROM permissao WHERE idLogin='$idUsuario' AND idMenu='$pagina_ativa'";
			$sqlPermissao = mysqli_query($conex, $query);
			if(mysqli_num_rows($sqlPermissao)==0){ //Se n&atilde;o tiver nenhum registro
				mysqli_close($conex);
				echo "Permissao de acesso negada. Consulte o administrador do sistema.";
				exit;
			}
			mysqli_close($conex);
		}
	}
}
//====================================//

//Fun&ccedil;&atilde;o para pegar os dados do usu&aacute;rio
function dadosUsuario($idUsuario){
	$conex	= conectaBD();
	$query	= "SELECT * FROM login WHERE id='$idUsuario'";
	$consulta		= mysqli_query($conex, $query);
	
	$dados = mysqli_fetch_assoc($consulta);
	
	mysqli_free_result($consulta);
	mysqli_close($conex);
	return $dados;
}
//====================================//

//Fun&ccedil;&atilde;o para exibir as configura&ccedil;őes do Painel
function dadosConfiguracoes(){
	$conex = conectaBD();
	$query = "SELECT * FROM configuracoes WHERE id=1";
	
	if ($consulta = mysqli_query($conex, $query)) {
		$dados = mysqli_fetch_assoc($consulta);
		mysqli_free_result($consulta);
		mysqli_close($conex);
		
		return $dados;
	}
	mysqli_close($conex);
}
//====================================//

//Fun&ccedil;&atilde;o para alterar as configura&ccedil;őes do Painel
function alterarConfiguracoes($valor, $coluna){
	$conex	= conectaBD();
	$campo	= $_POST[$campo];
	$query	= "UPDATE configuracoes SET $coluna='$valor' WHERE id=1";
	$consulta		= mysqli_query($conex, $query);
	mysqli_free_result($consulta);
	mysqli_close($conex);
	return $consulta;
}
//====================================//

//DATAS

//Inverter Data - Americano pro Brasileiro
function dataPadraoBrasileiro($data){
	 $dia = substr($data, 8, 2);
	 $mes = substr($data, 5, 2);
	 $ano = substr($data, 0, 4);
	 $data = $dia."/".$mes."/".$ano;
	 if($data == "00/00/0000" || $data == "01/01/2999"){$data = "";}
	 return $data;
}
//Inverter Data - Americano pro Brasileiro (2 digitos no ano
function dataPadraoBrasileiro2($data){
	 $dia = substr($data, 8, 2);
	 $mes = substr($data, 5, 2);
	 $ano = substr($data, 2, 2);
	 $data = $dia."/".$mes."/".$ano;
	 if($data == "00/00/0000" || $data == "01/01/2999"){$data = "";}
	 return $data;
}
//Inverter Data - Americano pro Brasileiro com hora
function dataEhora($data){
	$dia = substr($data, 8, 2);
	$mes = substr($data, 5, 2);
	$ano = substr($data, 0, 4);
	$hora = substr($data, 11, 5);
	$data = $dia."/".$mes."/".$ano." - ".$hora;
	return $data;
}
//DIA
function diaPadraoBrasileiro($data){
	return date('d', strtotime($data));
}
//MĘS
function mesPadraoBrasileiro($data){
	$mes = date('m', strtotime($data));
	switch ($mes){
		case 1: $mes = "JAN"; break;
		case 2: $mes = "FEV"; break;
		case 3: $mes = "MAR"; break;
		case 4: $mes = "ABR"; break;
		case 5: $mes = "MAI"; break;
		case 6: $mes = "JUN"; break;
		case 7: $mes = "JUL"; break;
		case 8: $mes = "AGO"; break;
		case 9: $mes = "SET"; break;
		case 10: $mes = "OUT"; break;
		case 11: $mes = "NOV"; break;
		case 12: $mes = "DEZ"; break;
	}
	return $mes;
}
//HORA
function horaPadraoBrasileiro($data){
	return date('H', strtotime($data));
}
//MINUTO
function minutoPadraoBrasileiro($data){
	return date('i', strtotime($data));
}
//Inverter Data - Americano pra URL
function dataPadraoURL($data){
	 $dia = substr($data, 8, 2);
	 $mes = substr($data, 5, 2);
	 $ano = substr($data, 0, 4);
	 $data = $dia."-".$mes."-".$ano;
	 return $data;
}
//Inverter Data - Brasileiro para Americano
function dataPadraoAmericano($data){
	$dia = substr($data, 0, 2);
	$mes = substr($data, 3, 2);
	$ano = substr($data, 6, 4);
	return $ano."-".$mes."-".$dia;
}
//Inverter Data - Por Extenso
function dataPorExtenso($data){
	$dia = substr($data, 0, 2);
	$mes = substr($data, 3, 2);
	$ano = substr($data, 6, 4);
	
	// configura&ccedil;&atilde;o mes
	switch ($mes){
		case 1: $mes = "janeiro"; break;
		case 2: $mes = "fevereiro"; break;
		case 3: $mes = "mar&ccedil;o"; break;
		case 4: $mes = "abril"; break;
		case 5: $mes = "maio"; break;
		case 6: $mes = "junho"; break;
		case 7: $mes = "julho"; break;
		case 8: $mes = "agosto"; break;
		case 9: $mes = "setembro"; break;
		case 10: $mes = "outubro"; break;
		case 11: $mes = "novembro"; break;
		case 12: $mes = "dezembro"; break;
	}
	return $dia." de ".$mes." de ". $ano;
}
// dia da semana
function diaDaSemana($data){
	$dia = (int)substr($data, 0, 2);
	$mes = substr($data, 3, 2);
	$ano = substr($data, 6, 4);
	$diasemana = date("w", mktime(0,0,0,$mes,$dia,$ano) );
	
	// semana
	switch($diasemana){  
		case"0": $diasemana = "Domingo";	   break;  
		case"1": $diasemana = "Segunda-Feira"; break;  
		case"2": $diasemana = "Ter&ccedil;a-Feira";   break;  
		case"3": $diasemana = "Quarta-Feira";  break;  
		case"4": $diasemana = "Quinta-Feira";  break;  
		case"5": $diasemana = "Sexta-Feira";   break;  
		case"6": $diasemana = "S&aacute;bado";		break;  
	}
	// configura&ccedil;&atilde;o mes
	switch ($mes){
		case 1: $mes = "janeiro"; break;
		case 2: $mes = "fevereiro"; break;
		case 3: $mes = "mar&ccedil;o"; break;
		case 4: $mes = "abril"; break;
		case 5: $mes = "maio"; break;
		case 6: $mes = "junho"; break;
		case 7: $mes = "julho"; break;
		case 8: $mes = "agosto"; break;
		case 9: $mes = "setembro"; break;
		case 10: $mes = "outubro"; break;
		case 11: $mes = "novembro"; break;
		case 12: $mes = "dezembro"; break;
	}
	
	return "<div>".$diasemana."</div><div>".$dia." de ".$mes." de ". $ano."</div>";
}
//====================================//

//Fun&ccedil;&atilde;o para gerar nova senha rand&ocirc;mica
function geraSenha($tamanho, $maiusculas, $numeros, $simbolos){
	$lmin = 'abcdefghijklmnopqrstuvwxyz';
	$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$num = '1234567890';
	$simb = '!@#$%*-';
	$retorno = '';
	$caracteres = '';
	
	$caracteres .= $lmin;
	if ($maiusculas) $caracteres .= $lmai;
	if ($numeros) $caracteres .= $num;
	if ($simbolos) $caracteres .= $simb;
	
	$len = strlen($caracteres);
	for ($n = 1; $n <= $tamanho; $n++) {
	$rand = mt_rand(1, $len);
	$retorno .= $caracteres[$rand-1];
	}
	return $retorno;
}
//====================================//

//Função anti SQL INJECTION
function antiInjection($valor){
	$valor = @preg_replace(sql_regcase("/(from|select|insert|update|delete|where|join|left|inner|delimeter|like|drop|show tables|truncate|alter|create|into|table|#|<|>|=|!|;|\*|--|\\\\)/"),"", $valor);
	//$valor = trim($valor);//limpa espaços vazio
	$valor = strip_tags($valor);//tira tags html e php
	$valor = addslashes($valor);//Adiciona barras invertidas a uma string
	return $valor;
}
//====================================//

//Limpa o texto
function textoLimpo($texto){
	$trocarIsso = array('<','>',';',',','"','&','*','=','?');
	$porIsso = array(' ',' ',' ',' ',' ',' ',' ',' ',' ');
	$textoLimpo = str_replace($trocarIsso, $porIsso, $texto);
	return addslashes($textoLimpo);
}
//====================================//

// função que gera uma texto limpo pra virar URL:
function geraUrlLimpa($texto){
	$trocarIsso = array('à','á','â','ã','ä','å','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ü','ú','ÿ','À','Á','Â','Ã','Ä','Å','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','O','Ù','Ü','Ú','Ÿ',"&lt;?","?&gt;","&rsquo;","&rsquo;","&ldquo;","&rdquo;","&lsquo;","&rsquo;");
	$porIsso = array('a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','y','A','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','O','U','U','U','Y',"","","","","","","","");
	$textoSemAcentos = str_replace($trocarIsso, $porIsso, $texto);
	//tirar outros caracteres
    $replaces = array(
		'/( )/'          => '-',
		'/[^a-zA-Z0-9\-_]/' => '', //tirando outros caracteres invalidos
		'/_/'            => '-',
		'/-+/'           => '-' // substitui múltiplos espaços (hifens) por apenas um
		
	);
	$textoLimpo = preg_replace(array_keys($replaces), array_values($replaces), $textoSemAcentos);
	return strtolower($textoLimpo);
}
//====================================//

//Função para substituir as tags php e as aspas duplas e simples
function aspasPHP($texto){
	$trocarIsso = array("<?", "?>", "'", "‘", "’", '"', "“", "”");
	$porIsso = array("&lt;?", "?&gt;", "&rsquo;", "&rsquo;", "&ldquo;", "&rdquo;", "&lsquo;", "&rsquo;");
	$textoLimpo = str_replace($trocarIsso, $porIsso, $texto);
	$textoLimpo = addslashes($textoLimpo);//Adiciona barras invertidas a uma string
	return $textoLimpo;
}
//Função para substituir as tags php e as aspas simples apenas
function aspasSimples($texto){
	$trocarIsso = array("<?", "?>", "'", "‘", "’", "%u2019");
	$porIsso = array("&lt;?", "?&gt;", "&rsquo;", "&rsquo;", "&rsquo;", "&rsquo;");
	$textoLimpo = str_replace($trocarIsso, $porIsso, $texto);
	$textoLimpo = addslashes($textoLimpo);//Adiciona barras invertidas a uma string
	return $textoLimpo;
}
//====================================//

//Função para cortar texto sem cortar a palavra
function quebra_texto($mensagem,$quantos) {
	$texto=strip_tags($mensagem);
	$str = substr($texto,$quantos,1);
	
	if(strlen($texto) > $quantos){
		if($str!="" && $str!=" "){
			while($str!="" && $str!=" "){
				$quantos++;
				$str = substr($texto,$quantos,1);
			}
		}
		$str = substr($texto,0,$quantos);
		return $str."...";
	} 
	else {
		return $texto;
	}
}
//====================================//

//Função anti SQL INJECTION
function geraKeywords($valor){
	$trocarIsso = array(" a ", " e ", " o ", " da ", " de ", " do ", " na ", " em ", " no ", " para ", " pra ", " à ", " até ", " com " , " sem ", " se ", " me ","&rsquo;", " | ", " / ", " - ");
	$porIsso = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ",""," "," "," ");
	$valor = str_replace($trocarIsso, $porIsso, $valor);
	$valor = strip_tags($valor);//tira tags html e php
	$valor = str_replace(' ', ', ', $valor); // alterar espaços por vírgulas
	return $valor;
}
//====================================//

//Fun&ccedil;&atilde;o para retornar uma extensao para os arquivos
function extensaoPadrao($tipo){
	//tipo de arquivos
	$tipoDOC = 'application/msword';
	$tipoDOC1 = 'application/doc';
	$tipoDOC2 = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
	$tipoTXT = 'text/plain';
	$tipoPPT = 'application/vnd.ms-powerpoint';
	$tipoPPT1 = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
	$tipoPDF = 'application/pdf';
	$tipoXLS = 'application/vnd.ms-excel';
	$tipoXLS1 = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
	$tipoZIP = 'application/zip';
	$tipoZIP1 = 'application/binary';
	$tipoZIP2 = 'application/x-zip-compressed';
	$tipoZIP3 = 'application/octet-stream';
	// e vamos inserir as imagens tamb&eacute;m
	$tipoJPG = 'image/pjpeg';
	$tipoJPG1= 'image/jpeg';
	$tipoPNG = 'image/x-png';
	$tipoPNG1= 'image/png';
	$tipoGIF = 'image/gif';
	$tipoSWF = 'application/x-shockwave-flash';
	//----------------
	if (($tipo == $tipoDOC) || ($tipo == $tipoDOC1) || ($tipo == $tipoDOC2)) {
		$extensao = '.doc';
	} else if ($tipo == $tipoTXT) {
		$extensao = '.txt';
	} else if (($tipo == $tipoPPT) || ($tipo == $tipoPPT1)) {
		$extensao = '.ppt';
	} else if ($tipo == $tipoPDF) {
		$extensao = '.pdf';
	} else if (($tipo == $tipoXLS) || ($tipo == $tipoXLS1)) {
		$extensao = '.xls';
	} else if (($tipo == $tipoZIP) || ($tipo == $tipoZIP1) || ($tipo == $tipoZIP2) || ($tipo == $tipoZIP3)) { 
		$extensao = '.zip';
	} // imgs 
	else if (($tipo == $tipoJPG) || ($tipo == $tipoJPG1)) { 
		$extensao = '.jpg';
	}else if ($tipo == $tipoPNG || $tipo == $tipoPNG1) {
		$extensao = '.png';
	}else if ($tipo == $tipoGIF) {
		$extensao = '.gif';
	}else if($tipo == $tipoSWF){
		$extensao = '.swf';
	}
	return $extensao;
}
//====================================//

//Fun&ccedil;&atilde;o para exibir o icone do arquivo
function iconeArquivo($arquivo){
	$extensaoArq = strtolower(end(explode(".", $arquivo)));
	if ($extensaoArq=='doc' || $extensaoArq=='txt') {
		$iconeArq = "doc";
	}
	else if ($extensaoArq=='ppt' || $extensaoArq=='pdf') {
		$iconeArq = "ppt";
	}
	else if ($extensaoArq=='xls') {
		$iconeArq = "xls";
	}
	else if ($extensaoArq=='jpg' || $extensaoArq=='png' || $extensaoArq=='gif' || $extensaoArq=='swf') {
		$iconeArq = "img";
	}
	else {
		$iconeArq = "zip";
	}
	return $iconeArq;
}
//====================================//

//Fun&ccedil;&atilde;o para inserir visualiza&ccedil;&atilde;o de p&aacute;gina
function visualizacao($tabela, $id){
	$conex	= conectaBD();
	$query = "UPDATE $tabela SET vis=vis+1 WHERE id='$id'";
	mysqli_query($conex, $query);
	mysqli_close($conex);
}
//====================================//

//Fun&ccedil;&atilde;o para excluir um registro
function excluirRegistro($tabela, $coluna, $registro){
	$conex	= conectaBD();
	
	if(!($registro)){ //Se n&atilde;o passar um registro, pega do POST
		$registro = $_POST['excluir'];
		if(is_array($registro)){
			for($l=0;$l<count($registro);$l++){
				$query = "DELETE FROM $tabela WHERE $coluna=$registro[$l]";
				$sqlExcluir = @mysqli_query($conex, $query);
				salvaLog("DELETE FROM $tabela WHERE $coluna=$registro[$l]");
			}
		}
	}else{
		$query = "DELETE FROM $tabela WHERE $coluna=$registro";
		$sqlExcluir = @mysqli_query($conex, $query);
		salvaLog("DELETE FROM $tabela WHERE $coluna=$registro");
	}
	mysqli_close($conex);
	return $sqlExcluir;
}
//====================================//

//Fun&ccedil;&atilde;o para exibir o menu
function carregarMenu(){
	$idUsuario = $_SESSION[SESSAO_CHAVE];
	
	$conex	= conectaBD();
	if($idUsuario==1){
		$query = "SELECT m.id as idMenu, m.nome as nome, m.link as link FROM menu m WHERE m.pagina_inicial=1 ORDER by m.nome asc";
		$sqlConsulta = @mysqli_query($conex, $query);
	}else{
		$query = "SELECT m.id as idMenu, m.nome as nome, m.link as link FROM menu m INNER JOIN permissao p ON (p.idMenu=m.id) WHERE p.idLogin='$idUsuario' AND m.pagina_inicial=1 ORDER by m.nome asc";
		$sqlConsulta = @mysqli_query($conex, $query);
	}
	mysqli_close($conex);
	return $sqlConsulta;
}
//Fun&ccedil;&atilde;o para carregar o submenu
function carregaSubMenu($idMenu){
	$conex	= conectaBD();
	$query = "SELECT id as idSubmenu, idMenu, nome, link FROM submenu WHERE idMenu='$idMenu' AND pagina_inicial=1 ORDER by nome asc";
	$sqlConsulta = @mysqli_query($conex, $query);
	mysqli_close($conex);
	return $sqlConsulta;
}
// consultar qual menu est&aacute; ativo
function consultaMenuAtivo($pagina) {
	$pagina = '?pagina='.$pagina;
	
	$conex	= conectaBD();
	$query = "SELECT idMenu FROM submenu WHERE link = '$pagina' limit 1";
	$consulta = @mysqli_query($conex, $query);
	$dados = mysqli_fetch_assoc($consulta);
	
	return $dados["idMenu"];
	
	mysqli_free_result($consulta);
	mysqli_close($conex);
}
//====================================//

//Fun&ccedil;&atilde;o para verificar se campo est&aacute; ou n&atilde;o vazio
function vazio($campo, $placeHolder, $mensagem){
	if($campo=="" || $campo==NULL || $campo==$placeHolder){
		echo '<script>
			 alert("'.$mensagem.'");
		     history.go(-1);
			 </script>';	 
		exit;  
	}
}
//====================================//
// ver se esse nome j&aacute; existe.
function verNomeImagem($nomeImagem,$tabela,$campo=""){

	// pesquisar no BD
	$conex	= conectaBD();
	if($campo =="topo" || $campo == "lado"){
		$query = "SELECT id FROM $tabela where imagem_topo ='$nomeImagem' OR imagem_lado = '$nomeImagem' ";
	}
	else{
		$query = "SELECT id FROM $tabela where file ='$nomeImagem' ";
	}
	$consulta = mysqli_query($conex, $query);
	
	if (@mysqli_num_rows($consulta)>0) {
		// extens&atilde;o
		$ext = substr($nomeImagem,(strlen($nomeImagem)-4),strlen($nomeImagem));
		
		//nome sem extens&atilde;o 
		$nomeSEx = substr($nomeImagem,0,-4);
		
		// ver se tem underline
		if (strstr($nomeSEx, '_') !== false) {
			$vetor_string = explode("_", $nomeSEx);
			$nomeSEx = $vetor_string[0];
			$numero = (int)$vetor_string[1];
		} else {
			$numero = 0;
		}
		// acrescentar 1 ao n&uacute;mero
		$numero++;
		
		// definir o novo nome
		$nomeImagem = $nomeSEx."_".$numero.$ext;
		
		// ver se esse nome j&aacute; existe
		return verNomeImagem($nomeImagem,$tabela,$campo);
		
	}
	else {
		mysqli_free_result($consulta);
		mysqli_close($conex);
		return $nomeImagem;
	}
}
//====================================//

//Fun&ccedil;ao padr&atilde;o para consultas 
/*
function consultar($campos="*", $tabela, $where="", $ordem="", $inicio=0, $fim=0, $echo=0){
	conectaBD();
	// Define o Where
	if($where!=0 || $where!=""){ 
		$where = "WHERE ".$where;
	}else{
		$where = "";
	}	
	// Define a ordem
	if($ordem!=0 || $ordem!=""){ 
		$ordem = "ORDER BY ".$ordem;
	}else{
		$ordem = "";
	}
	// Define o limite de registros
	if($inicio!=0 || $fim!=0){ 
		$limite = "LIMIT $inicio, $fim";
	}else{
		$limite = ""; //Nao h&aacute; limita&ccedil;ao
	}
	//
	$query = "SELECT $campos FROM $tabela $where $ordem $limite";
	$consulta = mysql_query($query);
	if($echo) {
		echo $query;
	}
	mysql_close();
	return $consulta;	
}
*/
//Fun&ccedil;ao padr&atilde;o para consultas
function consultar($campos="*", $tabela, $where="", $ordem="", $inicio=0, $fim=0, $echo=0){
	$conex	= conectaBD();
	
	// Define o Where
	if($where!=0 || $where!=""){ 
		$where = "WHERE ".$where;
	}else{
		$where = "";
	}
		
	// Define a ordem
	if($ordem!=0 || $ordem!=""){ 
		$ordem = "ORDER BY ".$ordem;
	}else{
		$ordem = "";
	}
	
	// Define o limite de registros
	if($inicio!=0 || $fim!=0){ 
		$limite = "LIMIT $inicio, $fim";
	}else{
		$limite = ""; //Nao h&aacute; limita&ccedil;ao
	}
	
	$query = "SELECT $campos FROM $tabela $where $ordem $limite";
	$consulta = mysqli_query($conex, $query);
	//
	if($echo) {
		echo $query;
	}
	mysqli_close($conex);
	return $consulta;
	
	/* agora &eacute; s&oacute; colocar:
	while ($dados = mysqli_fetch_assoc($consulta)) {
		$dados["campo"];
	}
	*/
}
//====================================//

//Fun&ccedil;ao para verificar rela&ccedil;őes
function relacionar($pai, $idPai, $filho, $idFilho){
	$conex	= conectaBD();
	//
	$sql = "SELECT idPai FROM ".$pai."_tem_".$filho." WHERE idPai='".$idPai."' AND id".ucfirst($filho)."='".$idFilho."'";
	$consulta = @mysqli_query($conex, $query);
	$result = @mysqli_num_rows($consulta);
	//	
	mysqli_free_result($consulta);
	mysqli_close($conex);
	return $result;
}
//====================================//
//Fun&ccedil;&atilde;o para criar paginac&atilde;o no Painel
function paginar($tabela, $link, $where, $pesquisa, $registrosPorPagina) {
	echo "P&aacute;gina: ";
	$sql 	= consultar("*", $tabela, $where);
	$qtd 	= @mysqli_num_rows($sql);
	$resto	= $qtd/$registrosPorPagina;
	$tamanho = ceil($resto);
	for ($x=1;$x<=$tamanho;$x++) {
		if ( (isset($_GET["pag"])) AND ($_GET["pag"] == $x) ) {
			echo "<strong>[".$x."]</strong>&nbsp; ";
		}elseif ( (!isset($_GET["pag"])) AND ($x == 1) ) {
			echo "<strong>[".$x."]</strong>&nbsp; ";
		}elseif($where!=''){
			echo "<a href='?pagina=".$link."-lista&pag=$x&pesquisa=".$pesquisa."'>".$x."</a>&nbsp; ";
		}else{
			echo "<a href='?pagina=".$link."-lista&pag=$x'>".$x."</a>&nbsp; ";
		}
	}
}
//====================================//

//Fun&ccedil;&atilde;o para gerar a URL do V&iacute;deo
function urlDoVideo($urlDoVideo,$tipo='video') {
	// pegar somente o id do v&iacute;deo em links normais
	if (strstr($urlDoVideo, 'watch?v=') !== false) {
		$vetor_string = explode("watch?v=", $urlDoVideo);
		$urlDoVideo = $vetor_string[1];
	}
	// pegar somente o id do v&iacute;deo em links curtos
	if (strstr($urlDoVideo, 'youtu.be/') !== false) {
		$vetor_string = explode("youtu.be/", $urlDoVideo);
		$urlDoVideo = $vetor_string[1];
	}
	// tirar o feature ou qualquer coisa assim.
	if (strstr($urlDoVideo, '&') !== false) {
		$vetor_string = explode("&", $urlDoVideo);
		$urlDoVideo = $vetor_string[0];
	}
	if (strstr($urlDoVideo, '#') !== false) {
		$vetor_string = explode("#", $urlDoVideo);
		$urlDoVideo = $vetor_string[0];
	}
	if($tipo=='player') {
		return '<iframe width="100%" height="400" src="http://www.youtube.com/embed/'.$urlDoVideo.'?rel=0" frameborder="0" allowfullscreen></iframe>';
	} else if($tipo=='url') {
		return $urlDoVideo;
	} else if($tipo=='imagem') {
		return 'http://i1.ytimg.com/vi/'.$urlDoVideo.'/mqdefault.jpg';
	} else {
		return '//www.youtube.com/embed/'.$urlDoVideo;
	}
}
//====================================//

//Fun&ccedil;&atilde;o para descobrir navegador do internauta
function qualNavegador() {
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'IE';
	} elseif (preg_match( '|Opera/([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'Opera';
	} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'Firefox';
	} elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'Chrome';
	} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
		$browser_version=$matched[1];
		$browser = 'Safari';
	} else {
	// browser not recognized!
		$browser_version = '';
		$browser= 'Outro';
	}
	return "$browser $browser_version";
}
//====================================//

//Fun&ccedil;&atilde;o para converter moeda
function moedaToFloat($valor){
	$source = array('.', ',');
	$replace = array('', '.');
	$valor = str_replace($source, $replace, $valor); //remove os pontos e substitui a virgula pelo ponto
	return $valor; //retorna o valor formatado para gravar no banco
}
function floatToMoeda($valor){
	$valor = number_format($valor, 2, ',', '.'); 
	return $valor; //retorna o valor formatado para gravar no banco
}

// mostrar erros
function mostraErros($boolean=0){
	if($boolean == 1){
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
	}
}
function debugVariaveis($onde,$mostraTudo=0, $die = 0){
	$toPrint = $onde;
	//print_r($toPrint);
	if($mostraTudo==0){
		$ignore = array('GLOBALS', '_FILES', '_COOKIE', '_POST', '_GET', '_SERVER', '_ENV', 'ignore','onde', "_REQUEST");
		foreach(array_keys($toPrint) as $chave){
			if(in_array($chave,$ignore)){
				//echo $chave.":chave<br>";
				unset($toPrint[$chave]);
			}
		}
	}
	if($mostraTudo!=0){
		print_r($toPrint);
	}else{
		debugPrintArray($toPrint);
	}
	if($die!=0){
		die();
	}
}
function debugPrintArray($array, $ident=0){
	foreach($array as $chave => $elemento){
		$identacao = "";
		for($i=0;$i<$ident; $i++){
			$identacao .= "-----|";
		}
		if(is_array($elemento)){			
			echo "$identacao chave: $chave { <br>";
			debugPrintArray($elemento, ++$ident);
			echo "$identacao }<br>";
			$ident--;
		}else if(is_string($elemento) || is_int($elemento) || is_float($elemento)){
			echo "$identacao $chave:   $elemento<br>";
		}else if(is_object($elemento)){
			echo "$identacao $chave:   Objeto<br>";
		}else if(!isset($array)){
			echo "$identacao $chave:  /Vazio/<br>";
		}
		else{
			echo "$identacao $chave: Tipo desconhecido<br>";
		}
	}
}
?>
