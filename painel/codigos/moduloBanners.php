<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 31/04/15
Vers&atilde;o: 0.1
*/
if (file_exists('codigos/funcoesPainel.php')) {
	require_once('codigos/funcoesPainel.php');
	require_once('codigos/moduloImagem.php');
} else if (file_exists('../codigos/funcoesPainel.php')) {
	require_once('../codigos/funcoesPainel.php');
	require_once('../codigos/moduloImagem.php');
}
/* 
** BANNERS
*/
//Fun&ccedil;ao para editar um texto
function salvarBanner(){
	//
	$idBanner	= (int)$_POST['id'];
	$titulo 		= aspasPHP($_POST['titulo']);
	if($titulo == "Titulo do banner"){
		$titulo = "";
	}
	$subtitulo 	= aspasPHP($_POST['subtitulo']);
	if($subtitulo == "Subtitulo do banner"){
		$subtitulo = "";
	}
	$link		= aspasPHP($_POST['link']);
	if($link == "http://www.exemplo.com.br/") {$link = "";}
	$ativo		= (int)$_POST['ativo'];
	//
	$agendar	= (int)$_POST['agendar'];
	if ($agendar) {
		$agenda_data	= dataPadraoAmericano($_POST['dataAgenda']);
		$horaPublica	= (int)$_POST['horaAgenda'];
		$minutoPublica	= (int)$_POST['minutoAgenda'];
		$exibirApartir	= $agenda_data." ".str_pad($horaPublica, 2, "0", STR_PAD_LEFT).":".str_pad($minutoPublica, 2, "0", STR_PAD_LEFT).":00";
		
	} else {
		$exibirApartir = "0000-00-00";
	}
	//
	$file		= $_FILES["file"];
	$tipoFile	= $_FILES["file"]['type'];
	//
	if(!$tipoFile){ 
		$file	= NULL;
		$tipoFile = NULL;
	}
	//
	$conex = conectaBD();
	if($idBanner==0){ 
		$query = "INSERT INTO 
		banners (titulo, subtitulo, link, ativo, exibirAPartir)
		VALUES ('$titulo', '$subtitulo', '$link', '$ativo', '$exibirApartir')";
		$cadastro = @mysqli_query($conex, $query);
		$idBanner = mysqli_insert_id($conex);
	}
	 else {
		$query = "UPDATE banners SET 
		subtitulo='$subtitulo', link='$link', ativo='$ativo', exibirAPartir='$exibirApartir'
		WHERE id='$idBanner'";
		$cadastro = @mysqli_query($conex, $query);
	}
	// imagem
	if($cadastro){
		gravaImagem($idBanner,'banners',$titulo,$tipoFile,$file['tmp_name'],"","banners","","", 720, 720, 90);
		atualizaNomeImagem($idBanner,'banners',$titulo,"","banners");
	}
	$msg_tipo = salvaLog2($cadastro, 'salvo', 'banner', $titulo, 'banners');
	mysqli_close($conex);
}
//====================================//

$acao = $_POST["acao"];
//====================================//

//Fun&ccedil;&atilde;o para eliminar imagens do Banner
if($acao == "eliminarImagemBanner") {
	//
	$id	 = (int)$_POST['dataId'];
	//
	$consulta	= consultar('file', 'banners', 'id='.$id);
	$dados 		= mysqli_fetch_assoc($consulta);
	
	// apaga do FTP
	@unlink("../../imgs/".$dados['file']);
	
	// apaga do BD
	$conex = conectaBD();
	$query = "UPDATE banners SET file='' WHERE id='$id'";
	$delete = mysqli_query($conex, $query);
	mysqli_close($conex);
	
	if($delete){
		echo '1';
	}
}
//====================================//
?>