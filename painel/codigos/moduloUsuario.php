<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 13/04/15
Vers&atilde;o: 0.1
*/
require_once('codigos/funcoesPainel.php'); //Fun&ccedil;őes padrőes do sistema


//Fun&ccedil;&atilde;o para cadastrar um novo usu&aacute;rios
function cadastrarUsuario(){
	$nome	= aspasPHP($_POST['nome']);
	$email	= aspasPHP($_POST['email']);
	$senha	= md5($_POST['senha']);
	$ativo	= aspasPHP($_POST['ativo']);
	
	//Verifica se j&aacute; h&aacute; um usu&aacute;rio cadastrado com esse email
	$duplicado = mysqli_num_rows(consultar("id", "login", "nome='$nome' OR email='$email'", '', 0, 0));
	if (!$duplicado){
		$conex	= conectaBD();
		$query = "INSERT INTO login (nome, email, senha, ativo) VALUES ('$nome', '$email', '$senha', '$ativo')";
		$sqlCadastro = mysqli_query($conex, $query);;
		$ultimoId	 = mysqli_insert_id($conex);
		mysqli_close($conex	);
	} else {
		$sqlCadastro = 0;
	}
	
	if($sqlCadastro){ 
		//Cadastro efetuado com sucesso
		inserePermissao($ultimoId);
		$_SESSION['mensagem'] = "Usu&aacute;rio cadastrado com sucesso.";	
		$msg_tipo = 1;
	}else{
		$_SESSION['mensagem'] = "Nome ou e-mail j&aacute; cadastrados. Tente outro nome ou e-mail.";
		$msg_tipo = 2;
	}
	salvaLog($_SESSION['mensagem']);
	?>
	<SCRIPT>
		location.href="home.php?pagina=usuarios-lista&msg=<?php echo $msg_tipo; ?>";
	</SCRIPT>
	<?php
}

//Fun&ccedil;&atilde;o para alterar dados do usu&aacute;rio
function editarUsuario(){
	$id		= (int)$_POST['id'];
	$nome	= aspasPHP($_POST['nome']);
	$email	= aspasPHP($_POST['email']);
	$ativo	= aspasPHP($_POST['ativo']);
	
	
	//Verifica se j&aacute; h&aacute; um usu&aacute;rio cadastrado com esse usu&aacute;rio e senha
	$duplicado = mysqli_num_rows(consultar("id", "login", "(nome='$nome' OR email='$email') AND id<>$id"));
	if(!$duplicado){
		$conex	= conectaBD();
		$query = "UPDATE login SET nome='$nome', email='$email', ativo='$ativo' WHERE id='$id'";
		$sqlAtualizacao = @mysqli_query($conex, $query);
		
		if($_POST['senha']!=''){
			$senha	= md5($_POST['senha']);
			$query = "UPDATE login SET senha='$senha' WHERE id='$id'";
			$sqlAtualizacao = mysqli_query($conex, $query);;
		}
		mysqli_close($conex);
	}else{
		$sqlAtualizacao = 0;
	}
	
	if($sqlAtualizacao){ 
		//Atualiza&ccedil;ao efetuada com sucesso
		excluirRegistro("permissao", "idLogin", $id);
		inserePermissao($id);
		
		//Retorna a mensagem que o cadastro foi efetuado com sucesso
		$_SESSION['mensagem'] = "Usu&aacute;rio atualizado com sucesso.";	
		$msg_tipo = 1;
	}else{
		$_SESSION['mensagem'] = "Nome ou e-mail j&aacute; cadastrados. Tente outro nome ou e-mail.";
		$msg_tipo = 2;
	}
	salvaLog($_SESSION['mensagem']);
	?>
	<SCRIPT>
		location.href="home.php?pagina=usuarios-lista&msg=<?php echo $msg_tipo; ?>";
	</SCRIPT>
	<?php
}

//Cadastra permiss&atilde;o de acesso
function inserePermissao($idUsuario){
	$permissao = $_POST['permissao'];
	// 
	for($l=0;$l<count($permissao); $l++){
		$conex	= conectaBD();
		$query = "INSERT INTO permissao (idLogin, idMenu) VALUES ('$idUsuario', '".$permissao[$l]."')";
		$sql = mysqli_query($conex, $query);
		mysqli_close($conex);
	}
}

//Verifica se usu&aacute;rio tem determinada permiss&atilde;o (na edi&ccedil;&atilde;o do usu&aacute;rio)
function permissaoUsuario($idUsuario, $idMenu){
	$conex	= conectaBD();
	$query = "SELECT * FROM permissao WHERE idLogin='$idUsuario' AND idMenu='$idMenu'";
	$sqlPermissao = mysqli_query($conex, $query);
	mysqli_close($conex);
	return mysqli_num_rows($sqlPermissao);
}

//Fun&ccedil;&atilde;o para alterar a senha do usuario
function alterarSenha(){
	$idUsuario		= $_SESSION[SESSAO_CHAVE];
	$senhaAtual 		= md5($_POST['senhaAtual']);
	$senhaNova		= md5($_POST['senhaNova']);
	$senhaConfirma	= md5($_POST['senhaConfirma']);
	
	if($senhaNova == $senhaConfirma){ //Verifica se a confirma&ccedil;&atilde;o da senha deu certo
		//Verifica se a senha Antiga estava certa
		$dadosUsuario = dadosUsuario($idUsuario);
		$senhaVelha = $dadosUsuario['senha'];
		
		if($senhaVelha==$senhaAtual){ //Se forem iguais, confirma a altera&ccedil;&atilde;o da senha
			$conex	= conectaBD();
			$query = "UPDATE login SET senha='$senhaNova' WHERE id='$idUsuario'";
			$sqlAltera	= @mysqli_query($conex, $query);
			mysqli_close($conex);
			return "1";
		}
		else {
			return "0"; //A senha digita para a senha antiga n&atilde;o bate
		}
	}
	else {
		return "2"; //A confirma&ccedil;&atilde;o n&atilde;o bateu
	}
}
?>