<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 10/04/15
Vers&atilde;o: 0.1
*/

if (file_exists('codigos/funcoesPainel.php')) {
	require_once('codigos/funcoesPainel.php');
	$extra = "";
} else {
	require_once('../codigos/funcoesPainel.php');
	$extra = "../";
}
		
//Fun&ccedil;&atilde;o para cadastrar arquivos
function cadastrarArquivo($pai, $idPai){
	
	$diretorio		= $extra."../imgs/";
	$arquivo		= $_FILES["arquivo"];
	$legendaArquivo	= $_POST['legendaArquivo'];
	
	for ($l=0;$l<=count($arquivo);$l++) {
		
		$nomeArq 		= $arquivo["name"][$l]; 
		$tamanhoArq		= $arquivo["size"][$l]; 
		$tmpArq			= $arquivo["tmp_name"][$l];
		$tipoArq		= extensaoPadrao($arquivo["type"][$l]);
		$legendaArq		= $legendaArquivo[$l];
		if ($legendaArq=="") { $legendaArq="arquivo"; }
		
		if ($tipoArq != '') {
			
			$nomeArquivo 	= "arquivo/".geraUrlLimpa($legendaArq).$tipoArq;
			// ver se esse nome j&aacute; existe.
			$nomeArquivo 	= verNomeImagem($nomeArquivo,"arquivo");
			
			//Cadastra o arquivo no banco de dados
			$conex	= conectaBD();
			$query = "INSERT INTO arquivo (pai, idPai, titulo, arquivo) VALUES ('$pai', '$idPai', '$legendaArq', '$nomeArquivo')";
			$sqlCadastro = mysqli_query($conex, $query);
			$id = mysqli_insert_id($conex);
			
			if($sqlCadastro){ //Se deu certo o cadastro no banco de dados
				if(move_uploaded_file($tmpArq, ($diretorio.$nomeArquivo))){
					$mensagem = "Arquivo cadastrado com sucesso";
				}else{
					$mensagem = "Erro ao fazer o upload do arquivo, tente novamente.";
				}
			}else{
				$mensagem = "Erro ao cadastrar o arquivo no BD, tente novamente.";
			}
		}
		salvaLog($mensagem);
	}
}

//Fun&ccedil;&atilde;o para editar um arquivo
function editarArquivo(){
	
	$idArquivoOld		= $_POST['idArquivoOld'];
	$legendaArquivoOld	= $_POST['legendaArquivoOld'];
	$nomeArquivoOld		= $_POST['nomeArquivoOld'];
	//
	$legendaArquivoNew	= $_POST['legendaArquivoNew'];
	
	for ($l=0;$l<count($idArquivoOld);$l++) {
		
		if ($legendaArquivoNew[$l]=="") { $legendaArquivoNew[$l]="arquivo"; }
			
		// ver se mudou o nome do arquivo 
		if ($legendaArquivoOld[$l] != $legendaArquivoNew[$l]) {
			
			// extens&atilde;o Antiga
			$ext = substr($nomeArquivoOld[$l],(strlen($nomeArquivoOld[$l])-4),strlen($nomeArquivoOld[$l]));
			
			// nome do arquivo Novo
			$nomeArquivoNew = "arquivo/".geraUrlLimpa($legendaArquivoNew[$l]).$ext;
			
			// ver se esse nome j&aacute; existe.
			$nomeArquivoNew = verNomeImagem($nomeArquivoNew,"arquivo");
			
			// Renomea o arquivo na pasta 
			$renomear = rename($extra."../imgs/".$nomeArquivoOld[$l], $extra."../imgs/".$nomeArquivoNew);
			//
			$conex = conectaBD();
			$query = "UPDATE arquivo SET arquivo='".$nomeArquivoNew."', titulo='".$legendaArquivoNew[$l]."' WHERE id='$idArquivoOld[$l]'";
			
			$sqlAtualizacao = mysqli_query($conex, $query);
			mysqli_close($conex);
		}
	}
}

//Fun&ccedil;ao para excluir um arquivo
function excluirArquivo($pai, $idPai){	

	$consultaArq = consultar("id, arquivo", "arquivo", "pai='".$pai."' AND idPai='".$idPai."'");
	while ($dados = mysqli_fetch_assoc($consultaArq)) {
		$file = $dados['arquivo'];
		// apaga do FTP
		if (file_exists("../../imgs/$file")) {
			@unlink("../../imgs/$file");
		}else if (file_exists("../imgs/$file")) {
			@unlink("../imgs/$file");
		}
		
		// apagar do BD
		$sqlExcluirArquivo = excluirRegistro("arquivo", "id", $dados['id']);  //Exclui os arquivos pelo id deles
	}
}
?>