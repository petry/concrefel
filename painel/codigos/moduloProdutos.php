<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 06/04/15
Vers&atilde;o: 0.1
*/
require_once('codigos/funcoesPainel.php'); //Fun&ccedil;őes padrőes do sistema
require_once('codigos/moduloImagem.php'); //M&oacute;dulo para gerenciamento de imagens
require_once('codigos/moduloArquivo.php'); //M&oacute;dulo para gerenciamento de arquivos
require_once('codigos/moduloVideo.php'); //M&oacute;dulo para o gerenciamento de v&iacute;deos

/* 
** PRODUTOS
*/
// fun&ccedil;&atilde;o para salvar um produto
function salvarProduto(){
	//
	$id				= (int)$_POST['id'];
	$idCategoria	= (int)$_POST['categoria'];
	$titulo			= aspasPHP($_POST['titulo']);
	vazio($titulo, "Titulo do produto", "Preencha o titulo do produto");
	
	$subtitulo		= aspasPHP($_POST['subtitulo']);
	if($subtitulo == "Subtitulo do produto"){
		$subtitulo = "";
	}
	$titulo1		= aspasPHP($_POST['titulo1']);
	if($titulo1 == "Titulo do texto"){
		$titulo1 = "";
	}
	$texto			= aspasSimples($_POST['texto']);
	
	$imagemTopo		= $_FILES['imagem_topo'];
	$tipoImagemTopo	= $_FILES['imagem_topo']['type'];
	$legendaLado	= aspasPHP($_POST['legendaLado']); 
	if ($legendaLado=="") { $legendaLado=$titulo; }
	
	$imagemLado		= $_FILES['imagem_lado'];
	$tipoImagemLado	= $_FILES['imagem_lado']['type'];
	$legendaTopo	= aspasPHP($_POST['legendaTopo']);
	if ($legendaTopo=="") { $legendaTopo=$titulo; }
	//
	$ativo			= (int)$_POST['ativo'];
	$capa			= (int)$_POST['capa'];
	$Fotos			= aspasPHP($_POST['novas-fotos']);
	
	if(!$tipoImagemTopo){
		$imagemTopo 	= NULL;
		$tipoImagemTopo = NULL;
	}
	if(!$tipoImagemLado){
		$imagemLado 	= NULL;
		$tipoImagemLado = NULL;
	}
	
	$cores = $_POST['cores'];
	//
	$conex = conectaBD();
	if($id == 0 ){
		$query = "INSERT INTO produto (titulo, subtitulo, titulo_1, texto, categoria, ativo, capa) VALUES ('$titulo', '$subtitulo', '$titulo1', '$texto', '$idCategoria', '$ativo', '$capa')";
		$sql = @mysqli_query($conex, $query);
		$id = mysqli_insert_id($conex);
		
		// novas fotos
		if($Fotos){
			$Fotos = substr($Fotos,0,-1);
			$query2 = "UPDATE imagem SET idPai='$id' WHERE id IN ($Fotos)";
			$sql = @mysqli_query($conex, $query2);
		}
	}
	else{
		$query = "UPDATE produto SET titulo='$titulo', subtitulo='$subtitulo', titulo_1='$titulo1', texto='$texto',  categoria='$idCategoria', ativo='$ativo', capa='$capa' WHERE id='$id' ";
		$sql = @mysqli_query($conex, $query);	
	}
	
	// apagar cores antigas
	$delete_rel = "DELETE FROM cor_produto WHERE idProduto = $id";
	$delete_sql = @mysqli_query($conex, $delete_rel);
	
	// inserir novas cores
	foreach($cores as $cor){
		$values .= "($cor,$id),";
	}
	$values = substr($values,0,-1);
	$query_cores = "INSERT INTO cor_produto(idCor,idProduto) VALUES $values";
	$sql_cores = @mysqli_query($conex, $query_cores);
	
	if($sql){
		//atualizar nome da imagem 
		atualizaNomeImagem($id,'produto',$legendaTopo,'topo','produto');
		atualizaNomeImagem($id,'produto',$legendaLado,'lado','produto');
		
		gravaImagem($id,'produto',$legendaTopo,$tipoImagemTopo,$imagemTopo['tmp_name'],'topo','produto','','',1400,'',90);
		gravaImagem($id,'produto',$legendaLado,$tipoImagemLado,$imagemLado['tmp_name'],'lado','produto');
		
		//Atualiza os outros dados
		editarArquivo();
		cadastrarArquivo('produto', $id);
		excluirVideo("produto", $id);
		cadastrarVideo("produto", $id);
	}
	//
	$msg_tipo = salvaLog2($sql, 'salvo', 'produto', $titulo, 'produtos');
	mysqli_close($conex);
}
//====================================//

/* 
** CATEGORIAS
*/
//Fun&ccedil;&atilde;o para cadastrar uma categoria
function salvarCategoria(){
	$id			= (int)$_POST['id'];
	$nome		= aspasPHP($_POST['nome']);
	$url		= geraUrlLimpa($nome);
	vazio($nome, "Nome da Categoria", "Voc&ecirc; deve preencher o nome da categoria");
	$ativo		= (int)$_POST['ativo'];
	
	$conex = conectaBD();
	if($id == 0 ){
		$query = "INSERT INTO categoria(nome,url,ativo) VALUES ('$nome','$url',$ativo)";
	}else{
		$query = "UPDATE categoria SET nome='$nome', url='$url', ativo='$ativo' WHERE id='$id'";
	}
	$sql = @mysqli_query($conex, $query);
		
	//
	$msg_tipo = salvaLog2($sql, 'salva', 'categoria', $nome, 'categorias');
	mysqli_close($conex);
}

function salvarCor(){
	$id		= (int)$_POST['id'];
	$titulo	= $_POST['titulo'];
	$cor	= $_POST['cor'];
	
	$conex = conectaBD();
	if($id == 0){// &eacute; cadastro
		$query = "INSERT INTO cor(cor,titulo) VALUES ('$cor','$titulo')";
	}else{// &eacute; atualiza&ccedil;ao
		$query = "UPDATE cor SET cor = '$cor', titulo = '$titulo' WHERE id = $id";
	}
	$exec = mysqli_query($conex, $query);
	
	$msg_tipo = salvaLog2($exec, 'salvo', 'cor', $titulo, 'cores');
	mysqli_close($conex );
}
//====================================//
?>