<?php
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 21/07/15
Vers&atilde;o: 0.1
*/
require_once('codigos/funcoesPainel.php'); //Fun&ccedil;őes padrőes do sistema
require_once('codigos/moduloImagem.php'); //M&oacute;dulo para gerenciamento de imagens
require_once('codigos/moduloArquivo.php'); //M&oacute;dulo para gerenciamento de arquivos
require_once('codigos/moduloVideo.php'); //M&oacute;dulo para o gerenciamento de v&iacute;deos

/* 
** NOT&Iacute;CIAS
*/
//Fun&ccedil;ao para cadastrar uma nova not&iacute;cia
function salvarNoticia(){
	//
	$id				= (int)$_POST['id'];
	$titulo			= aspasPHP($_POST['titulo']);
	vazio($titulo, "Titulo da noticia", "Preencha o titulo da noticia");
	//
	$subtitulo		= aspasPHP($_POST['subtitulo']);
	if($subtitulo == "Subtitulo da noticia"){
		$subtitulo = "";
	}
	$titulo1		= aspasPHP($_POST['titulo1']);
	if($titulo1 == "Titulo do texto"){
		$titulo1 = "";
	}
	$texto			= aspasSimples($_POST['texto']);
	
	$imagemTopo		= $_FILES['imagem_topo'];
	$tipoImagemTopo	= $_FILES['imagem_topo']['type'];
	$legendaLado	= aspasPHP($_POST['legendaLado']); 
	if ($legendaLado=="") { $legendaLado=$titulo; }
	
	$imagemLado		= $_FILES['imagem_lado'];
	$tipoImagemLado	= $_FILES['imagem_lado']['type'];
	$legendaTopo	= aspasPHP($_POST['legendaTopo']);
	if ($legendaTopo=="") { $legendaTopo=$titulo; }
	
	$data			= dataPadraoAmericano($_POST["data"]);
	$exibir			= (int)$_POST['exibicao'];
	$ativo			= (int)$_POST['ativo'];
	$capa			= (int)$_POST['capa'];
	$Fotos			= aspasPHP($_POST['novas-fotos']);
	
	if(!$tipoImagemTopo){
		$imagemTopo 	= NULL;
		$tipoImagemTopo = NULL;
	}
	if(!$tipoImagemLado){
		$imagemLado 	= NULL;
		$tipoImagemLado = NULL;
	}
	//
	$conex = conectaBD();
	if($id == 0 ){
		$query = "INSERT INTO noticia (titulo, subtitulo, titulo_1, texto, data, exibir, capa, ativo) VALUES ('$titulo', '$subtitulo', '$titulo1', '$texto', '$data', '$exibir', '$capa', '$ativo')";
		$sql = @mysqli_query($conex, $query);
		$id = mysqli_insert_id($conex);
		
		// novas fotos
		if($Fotos){
			$Fotos = substr($Fotos,0,-1);
			$query2 = "UPDATE imagem SET idPai='$id' WHERE id IN ($Fotos)";
			$sql = @mysqli_query($conex, $query2);
		}
	}
	else{
		$query = "UPDATE noticia SET titulo='$titulo', subtitulo='$subtitulo', titulo_1='$titulo1', texto='$texto', data='$data', exibir='$exibir', capa='$capa', ativo='$ativo' WHERE id='$id' ";
		$sql = @mysqli_query($conex, $query);
	}
	
	if($sql){
		//atualizar nome da imagem 
		atualizaNomeImagem($id,'noticia',$legendaTopo,'topo','noticia');
		atualizaNomeImagem($id,'noticia',$legendaLado,'lado','noticia');
		
		gravaImagem($id,'noticia',$legendaTopo,$tipoImagemTopo,$imagemTopo['tmp_name'],'topo','noticia','','',1400,'',90);
		gravaImagem($id,'noticia',$legendaLado,$tipoImagemLado,$imagemLado['tmp_name'],'lado','noticia');
		
		//Atualiza os outros dados
		editarArquivo();
		cadastrarArquivo('noticia', $id);
		excluirVideo("noticia", $id);
		cadastrarVideo("noticia", $id);
	}
	//
	$msg_tipo = salvaLog2($sql, 'salvo', 'noticia', $titulo, 'noticias');
	mysqli_close($conex);
}
//====================================//
?>