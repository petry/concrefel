<?php
header ('Content-type: text/html; charset=UTF-8');
/*
Copyright [2015] [isDesign Est&uacute;dio de Cria&ccedil;&atilde;o Digital]
Desenvolvido por: ISDESIGN - FELIZ - RS
Cria&ccedil;&atilde;o: 08/04/15
Vers&atilde;o: 0.1
*/
session_start();
require_once('codigos/funcoesPainel.php');
$dadosPainel  = dadosConfiguracoes();
//
restritoAcesso();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<?php
require_once('includes/head.php'); 
$idUsuario 	  = $_SESSION[SESSAO_CHAVE];
$dadosUsuario = dadosUsuario($idUsuario);
?>
<!-- C&oacute;digo para o Editor de texto -->
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	width:    "100%",
	height:   "350",
	mode:     "exact",
	theme:    "advanced",
	language: "pt",
	convert_urls : false,
	elements: "texto1",
	
	plugins : "safari,pagebreak,style,layer,table,advimage,advlink,emotions,inlinepopups,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras",
	
	// Theme options
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough, | ,sub,sup, |  ,bullist,numlist, | ,justifyleft,justifycenter,justifyright,justifyfull, | ,link,unlink, | ,hr,image,media,charmap, | ,search,replace, | ,undo,redo, | ,cut,copy,paste,pastetext",
	theme_advanced_buttons2 : "tablecontrols,visualaid, | ,code,fullscreen, | ,selectall,removeformat",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	
	content_css : "estilos_editor.css"
});
</script>
</head>
<body>
	<div class="cabecalho">
        <a href="home.php" title="P&aacute;gina inicial" class="cabecalhoHome"><img src="img/menu-home.png" width="18" height="16" alt="P&aacute;gina inicial" /></a>
        <div class="cabecalhoBarras"></div>
        <div class="cabecalhoTitulo">Painel de Administra&ccedil;&atilde;o do <?php echo $dadosPainel['nome_entidade']; ?></div>
        <a href="sair.php" title="Sair" class="cabecalhoSair">Sair</a>
        <div class="cabecalhoBarras cabecalhoBarrasRight"></div>
        <a href="home.php?pagina=usuarios-senha" title="Alterar senha" class="cabecalhoSenha">Alterar senha</a>
        <div class="cabecalhoBarras cabecalhoBarrasRight"></div>
        <div class="cabecalhoUsuario">Usu&aacute;rio: <strong><?php echo $dadosUsuario['nome']; ?></strong></div>
    </div>
    
	<div class="wrapper">
		<ul class="menu">
        <?php
		$menu = carregarMenu($idUsuario);
		while ($dadosMenu = mysqli_fetch_assoc($menu)) {
			$encode = utf8_encode($dadosMenu['nome'])
			?>
        
            <li id="menu-<?php echo $dadosMenu['idMenu']; ?>" class="menu">
                <a href="home.php<?php echo $dadosMenu['link']; ?>" class="menu"><?php echo $encode; ?></a><span class="menu"></span>
                <?php 
				$submenu = carregaSubMenu($dadosMenu['idMenu']);
				if(@mysqli_num_rows($submenu)>0){
				?>
                <div class="submenu">
                    <div></div>
                    <ul>
                    <?php 
					while ($dadosSubmenu = mysqli_fetch_assoc($submenu)) {
						?>
                        <li><a href="home.php<?php echo $dadosSubmenu['link']; ?>"><?php echo $dadosSubmenu['nome']; ?></a></li>
                    <?php } ?>
                    </ul>
                </div>
                <?php } ?>
            </li>
        <?php } ?>
        </ul>
        
		<?php
		//------------------------------------------
        // mensagem de acerto ou de erro
		if (isset($_GET["msg"])) {
			$msg = (int)$_GET["msg"];
			if($msg == 1) { ?>
            	<div class="status status-certo"><img src="img/icon-status-certo.png" /> 
				<?php echo $_SESSION['mensagem']; ?>
                </div>
			<?php 
			} else if($msg == 2) { ?>
				<div class="status status-erro"><img src="img/icon-status-erro.png" />
                <?php echo $_SESSION['mensagem']; ?>
                </div>
			<?php 
			}
		}
		//------------------------------------------
		
        // incluir a p&aacute;gina
		if (isset($_GET["pagina"])) {
			
			$pagina = "includes/".$_GET["pagina"].".php";
			include_once($pagina);
			
            // pagina ativa // para restacar no menu
			$pagina_ativa = consultaMenuAtivo(antiInjection($_GET["pagina"]));
		}
		//}
		else{
			//Se nao houver INCLUDE, chama a home
			include_once("includes/home.php");
		}
		//----------------------------------
        ?>
        <div class="clearBoth"></div>
	</div>
    
	<div class="rodape">
        <div class="rodapeCopyright"><p>Painel de Administra&ccedil;&atilde;o | <?php echo $dadosPainel['nome_entidade']; ?> | Todos os direitos reservados.</p></div>
        <div class="rodapeCreditos">
            <p>Desenvolvido por:</p>
            <a href="http://www.isdesign.com.br/" title="isDesign: Est&uacute;dio de Cria&ccedil;&atilde;o Digital" target="_blank"></a>
        </div>
    </div>
	
<!-- -->
<div class="status-flutuante-wrap" id="status-flutuante-wrap"><div class="status-flutuante" id="status-flutuante"></div></div>

<div id="alert-excluir" class="modal">
	<div class="modal-wrap" style="width:300px; height:90px; margin:-95px 0px 0px -200px;">
		<p class="text18">Tem certeza que deseja excluir?</p>
		<div style="width:220px; margin:30px auto 0px auto;">
			<a class="modal-botao float" id="excluirSim" data-posicao="0" data-referencia="0" data-id="0" data-oque="">Excluir</a>
			<a class="modal-botao floatRight" id="cancelarExclusao">Cancelar</a>
		</div>
	</div>
</div>

<div id="overlay">
	<div id="overlay-wrap" class="text14">salvando...</div>
</div>
<!-- -->
</body>
</html>

<script type="text/javascript">
$(document).ready (function(){
	
	/* efeito par a p&aacute;gina ativa */
	$("#menu-<?php echo $pagina_ativa; ?>").addClass("menuAtivo"); 
	
	/* Efeitos para lista e pesquisar */
	if (!$.support.placeholder) {
		$("input").focus (function(){ if ($(this).val() == $(this).attr("placeholder")) { $(this).val(""); } });
		$("input").blur  (function(){ if ($(this).val() == "") { $(this).val($(this).attr("placeholder")); } });
		$("input").blur();
	}
	
	/* esconder o campo de mensagem */
	$(".status").click(function(){
		$(this).slideUp();
	});
	<?php if (isset($_GET["msg"])) { // mensagem de erro ?>
		setTimeout(function() {
			$(".status").slideUp();
		}, 10000);
	<?php } ?>
});

/*
** Fun&ccedil;&otilde;es para adicionar &iacute;tens
** Autor: isDesign | www.isdesign.com.br
** Versao: 1.0
*/
//
numVideo = parseInt($("#morefilesVideo .editarConteudo:last").attr("data-id"))+1;
if(!numVideo){numVideo=0;};
function dispMoreFilesVideo() {
	var filelocalVideo = document.getElementById('morefilesVideo');
	var fileIDVideo = "video"+numVideo;
	var fileDIVVideo = document.createElement('div');
	fileDIVVideo.setAttribute("id", fileIDVideo);
	fileDIVVideo.setAttribute("class", "editarConteudo");
	fileDIVVideo.innerHTML = "<div class='editarConteudoItem'><p>Insira o link do seu v&iacute;deo no YouTube:</p><input type='text' name='urlVideo[]' id='"+fileIDVideo+"' class='form-input' /></div> <div class='editarConteudoItem'><p>T&iacute;tulo do v&iacute;deo:</p><input type='text' name='tituloVideo[]' class='form-input' maxlength='150' /></div><div class='editarConteudoItem'><p>Descri&ccedil;ao do v&iacute;deo:</p><input type='text' name='descricaoVideo[]' class='form-input' /></div><div class='clearBoth'></div><a href=\"javascript:rmvMoreFilesVideo('"+fileIDVideo+"')\" title='Remover este v&iacute;deo' class='botao botaoCinza botaoRemover'>Remover</a>";
	filelocalVideo.appendChild(fileDIVVideo);
	numVideo++;
}
function rmvMoreFilesVideo(divNumVideo){
	var ddd = document.getElementById('morefilesVideo');
	var olddivVideo = document.getElementById(divNumVideo);
	ddd.removeChild(olddivVideo);
	numVideo--;
}
//
numArq = parseInt($("#morefilesArq .editarConteudo:last").attr("data-id"))+1;
if(!numArq){numArq=0};
function dispMoreFilesArq() {
	var filelocalArq = document.getElementById('morefilesArq');
	var fileIDArq = "arq"+numArq;
	var fileDIVArq = document.createElement('div');
	fileDIVArq.setAttribute("id", fileIDArq);
	fileDIVArq.setAttribute("class", "editarConteudo");
	fileDIVArq.innerHTML = "<div class='editarConteudoItem'><p>Upload de arquivos (zip, pdf, xls, doc):</p><input type='file' name='arquivo[]' id='"+fileIDArq+"' /></div> <div class='editarConteudoItem'><p>Legenda:</p><input type='text' name='legendaArquivo[]' class='input' value='' maxlength='150' /></div><div class='clearBoth'></div><a href=\"javascript:rmvMoreFilesArq('"+fileIDArq+"')\" title='Remover este arquivo' class='botao botaoCinza botaoRemover'>Remover</a>";
	filelocalArq.appendChild(fileDIVArq);
	numArq++;
}
function rmvMoreFilesArq(divNumArq){
	var ddd = document.getElementById('morefilesArq');
	var olddivArq = document.getElementById(divNumArq);
	ddd.removeChild(olddivArq);
	numArq--;
}
//
numInfo = parseInt($("#morefilesInfo .editarConteudo:last").attr("data-id"))+1;
if(!numInfo){numInfo=0};
function dispMoreFilesInfo() {
	var filelocalInfo = document.getElementById('morefilesInfo');
	var fileIDInfo = "info"+numInfo;
	var fileDIVInfo = document.createElement('div');
	fileDIVInfo.setAttribute("id", fileIDInfo);
	fileDIVInfo.setAttribute("class", "editarConteudo");
	fileDIVInfo.innerHTML = "<div class='editarConteudoItem'><p>Informa&ccedil;&atilde;o:</p><input type='text' name='it_nome[]' class='input' maxlength='100' value='' /></div><div class='editarConteudoItem'><p>Especifica&ccedil;&atilde;o:</p><input type='text' name='it_descricao[]' class='input' maxlength='300' value='' /></div><div class='clearBoth'></div><a href=\"javascript:rmvMoreFilesInfo('"+fileIDInfo+"')\" title='Remover este dado' class='botao botaoCinza botaoRemover'>Remover</a>";
	filelocalInfo.appendChild(fileDIVInfo);
	numInfo++;
}
function rmvMoreFilesInfo(divNumInfo){
	var ddd = document.getElementById('morefilesInfo');
	var olddivInfo = document.getElementById(divNumInfo);
	ddd.removeChild(olddivInfo);
	numInfo--;
}
//
</script>