<?php 
session_start();

require_once('codigos/funcoesPainel.php');

unset($_SESSION[SESSAO_CHAVE]);

session_destroy();

header('Location: index.php');
?>