<!doctype html>
<html>
	<head>
		<?php require("includes/head.php"); ?>
		
		<title>Produtos - Blocos e pisos de concreto - Concrefel</title>
		<meta name="title"			content="Produtos - Blocos de concreto e pisos de concreto - Concrefel">
		<meta name="description"	content="Diferentes modelos de blocos e pisos de concreto. Grande flexibilidade arquitet&ocirc;nica, pelas pequenas dimensoes dos blocos, al&eacute;m da variedade de cores.">
		<meta name="keywords"		content="tijolos, blocos, estrutural, meio bloco, canaleta, grama, piso, holandes, raquete, retangular, unistein, s&eacute;rie, 09, 14, 19, rio grande do sul, brasil">
		
		<meta property="og:title"		content="Produtos - Blocos de concreto e pisos de concreto - Concrefel">
		<meta property="og:description"	content="Diferentes modelos de blocos e pisos de concreto. Grande flexibilidade arquitet&ocirc;nica, pelas pequenas dimensoes dos blocos, al&eacute;m da variedade de cores.">
		
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>
		<main class="conteudo" itemscope itemtype="http://schema.org/Product">
			<div class="conteudo-margin conteudo-padding">
				<p class="color-999 font-size-12"><a href="index.php" title="Concrefel">Concrefel</a> &nbsp;/&nbsp; Produtos</p>
				<header class="margin-top-60">
					<h1 class="font-size-40 texto-titulo texto-headline">Nossos produtos</h1>
					<p>&nbsp;</p>
					<p>Blocos esruturais e pisos de concreto com a qualidade que sua obra merece.</p>
				</header>
				
				<?php
				$consultaCat = consultar("id, nome", "categoria", "ativo=1");
				while($categoria = mysqli_fetch_assoc($consultaCat)){
					$idCat = $categoria['id'];
					
					$consultaProd = consultar("id, titulo, imagem_lado", "produto", "categoria = $idCat AND ativo=1","titulo ASC");
					if(mysqli_num_rows($consultaProd) > 0){
						?>
						<section class="margin-top-60">
							
							<h2 class="font-size-24 texto-titulo"><?=$categoria['nome'];?></h2>
							<div class="home-produtos">
								<?php
								while($produto = mysqli_fetch_assoc($consultaProd)){
									//
									$img = "imgs/".$produto['imagem_lado'];
									if($produto['imagem_lado'] == "" ){
										$img = "img/no_img.png";
									} 
									?>
									<a href="produtos-detalhes.php?produto=<?=$produto['id'];?>" title="<?=$categoria['nome']." - ".$produto['titulo'];?>" class="home-produtos-li box-shadow" itemprop="url">
										<div class="home-produtos-li-imagem"><img src="includes/thumb.php?file=../<?=$img;?>&w=120&h=120" alt="<?= $nome." - ".$produto['titulo'];?>" class="img-retina" itemprop="image"></div>
										<div class="home-produtos-li-texto">
											<p class="home-produtos-li-texto-titulo" itemprop="name"><?=$produto['titulo'];?></p>
											<p class="home-produtos-li-texto-descricao" itemprop="model"><?=$categoria['nome'];?></p>
										</div>
									</a>
								<?php
								} // while produtos
								?>
							<div class="clear-both"></div>
						</div>
					</section>
					<?php
					}
				}
				?>
				<div class="clear-both"></div>
			</div>
			
			<div class="conteudo-linha"></div>
			<?php require("includes/rodape.php"); ?>
		</main>
		
		<div class="clear-both"></div>
	</body>
</html>