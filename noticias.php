<!doctype html>
<html>
	<head>
		<?php require("includes/head.php"); ?>
		
		<title>Not&iacute;cias e Novidades - Concrefel</title>
		<meta name="title"			content="Not&iacute;cias e Novidades - Concrefel">
		<meta name="description"	content="Veja as novidades em projetos residenciais, comerciais e urbanos.">
		<meta name="keywords"		content="noticias, novidades, concreto, construcao, blocos, pisos, concrefel, vale do cai, rs, brasil">
		
		<meta property="og:title"		content="Not&iacute;cias e Novidades - Concrefel">
		<meta property="og:description"	content="Veja as novidades em projetos residenciais, comerciais e urbanos.">
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>
		<main class="conteudo">
		
			<div class="conteudo-margin conteudo-padding" itemscope itemtype="https://schema.org/NewsArticle">
				<p class="color-999 font-size-12"><a href="index.php" title="Concrefel">Concrefel</a> &nbsp;/&nbsp; &Uacute;ltimas not&iacute;cias</p>
				<header class="margin-top-60">
					<h1 class="font-size-40 texto-titulo texto-headline">&Uacute;ltimas not&iacute;cias</h1>
				</header>
				<section class="texto-paragrafo">
					<p>&nbsp;</p>
					<p>Fique por dentro de nossas &uacute;ltimas not&iacute;cias referentes &agrave;s obras e produtos da Concrefel.</p>
				</section>
				<div class="clear-both margin-top-60"></div>
				<?php require("includes/noticias.php"); ?>
			</div>
			
			<div class="conteudo-linha"></div>
			
			<?php 
			if($idsNoticias) {
				$idsNoticias = substr($idsNoticias, 0, -1);
				$whereNot = "AND id NOT IN($idsNoticias)";
			}
			$consulta = consultar("id,data,titulo,imagem_topo,imagem_lado", "noticia", "ativo=1 AND (exibir=1 OR exibir=3) $whereNot", "data DESC");
			if(mysqli_num_rows($consulta)>0) { ?>
				<section class="conteudo-margin conteudo-padding" style="padding-top:2em;">
					<?php
					while($noticia = mysqli_fetch_assoc($consulta)){
						// imagem
						if ($noticia['imagem_topo'] != ""){
							$imgN = $noticia['imagem_topo'];
						} else if ($noticia['imagem_lado'] != ""){
							$imgN = $noticia['imagem_lado'];
						} else {
							$cImagens = consultar("file", "imagem", "pai='noticia' AND idPai=".$noticia['id'], "pos ASC, id ASC",0,1);
							if(mysqli_num_rows($cImagens)>0) { 
								$imgs = mysqli_fetch_assoc($cImagens);
								$imgN = $imgs['file'];
							} else {
								$imgN = '';
							}
						}
						?>
						<a href="noticias-detalhes.php?noticia=<?=$noticia['id'];?>" title="<?=$noticia['titulo'];?>" class="noticias box-shadow" itemprop="url">
							<?php if ($imgN) { ?>
							<div class="noticias-imagem"><div class="noticias-imagem-wrap"><img src="includes/thumb.php?file=../imgs/<?=$imgN;?>&w=400" alt="<?=$noticia['titulo'];?>" class="noticias-imagem-img"></div></div>
							<?php } ?>
							<div class="noticias-texto">
								<p class="color-999 font-size-12"><?=dataPadraoBrasileiro($noticia['data']);?></p>
								<p class="font-size-14" style="margin-top:0.5em;"><?=$noticia['titulo'];?></p>
							</div>
							<div class="clear-both"></div>
						</a>
					<?php } ?>
				</section>
			<?php } ?>
			<div class="conteudo-linha"></div>
			<?php require("includes/rodape.php"); ?>
		</main>
		
		<div class="clear-both"></div>
	</body>
</html>