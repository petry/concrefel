<!doctype html>
<html>
	<head>
		<?php require("includes/head.php"); ?>
		
		<title>Fotos de Obras realizadas pela Concrefel</title>
		<meta name="title"			content="Fotos de Obras realizadas pela Concrefel">
		<meta name="description"	content="Veja fotos de obras p&uacute;blicas e particulares com produtos de concreto produzidos e instalados pela Concrefel.">
		<meta name="keywords"		content="fotos, obras, fotos de obras, concreto, blocos de concreto, pisos, concrefel, vale do ca&iacute;, rs, brasil">
		
		<meta property="og:title"		content="Fotos de Obras realizadas pela Concrefel">
		<meta property="og:description"	content="Veja fotos de obras p&uacute;blicas e particulares com produtos de concreto produzidos e instalados pela Concrefel.">
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>
		<main class="conteudo">
			<section class="conteudo-margin conteudo-padding">
				<p class="color-999 font-size-12"><a href="index.php" title="Concrefel">Concrefel</a> &nbsp;/&nbsp; Fotos de Obras</p>
				<header class="margin-top-60">
					<h1 class="font-size-40 texto-titulo texto-headline">Fotos de Obras</h1>
				</header>
				<div class="texto-paragrafo">
					<p>&nbsp;</p>
					<p>Veja fotos de obras p&uacute;blicas e particulares com produtos de concreto produzidos e instalados pela Concrefel.</p>
				</dic>
			
				<div class="clear-both margin-top-80"></div>
				<?php
				$consulta = consultar("id,data,titulo,imagem_topo,imagem_lado", "noticia", "ativo=1 AND (exibir=2 OR exibir=3) ", "data DESC");
				while($noticia = mysqli_fetch_assoc($consulta)){
					// imagem
					if ($noticia['imagem_topo'] != ""){
						$imgN = $noticia['imagem_topo'];
					} else if ($noticia['imagem_lado'] != ""){
						$imgN = $noticia['imagem_lado'];
					} else {
						$cImagens = consultar("file", "imagem", "pai='noticia' AND idPai=".$noticia['id'], "pos ASC, id ASC",0,1);
						if(mysqli_num_rows($cImagens)>0) { 
							$imgs = mysqli_fetch_assoc($cImagens);
							$imgN = $imgs['file'];
						} else {
							$imgN = '';
						}
					}
					?>
					<a href="noticias-detalhes.php?noticia=<?=$noticia['id'];?>" title="<?=$noticia['titulo'];?>" class="noticias box-shadow" itemprop="url">
						<?php if ($imgN) { ?>
						<div class="noticias-imagem"><div class="noticias-imagem-wrap"><img src="includes/thumb.php?file=../imgs/<?=$imgN;?>&w=400" alt="<?=$noticia['titulo'];?>" class="noticias-imagem-img"></div></div>
						<?php } ?>
						<div class="noticias-texto">
							<p class="color-999 font-size-12"><?=dataPadraoBrasileiro($noticia['data']);?></p>
							<p class="font-size-14" style="margin-top:0.5em;"><?=$noticia['titulo'];?></p>
						</div>
						<div class="clear-both"></div>
					</a>
				<?php } ?>
			</section>
			
			<div class="conteudo-linha"></div>
			<?php require("includes/rodape.php"); ?>
		</main>
		
		<div class="clear-both"></div>
	</body>
</html>