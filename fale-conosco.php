<!doctype html>
<html>
	<head>
		<?php require("includes/head.php");
		$consulta = consultar("*","texto","id = 3");
		$dados = mysqli_fetch_assoc($consulta);
		?>
		
		<title><?=$dados['titulo'];?> - Concrefel</title>
		<meta name="title"			content="<?=$dados['titulo'];?> - Concrefel">
		<meta name="description"	content="<?php
		if (trim($dados['subtitulo'])){
			echo $dados['subtitulo'];
		} else {
			echo quebra_texto($dados['texto'], 140);
		}
		?>">
		<meta name="keywords" content="<?=geraKeywords($dados['titulo']).", ".geraKeywords($dados['subtitulo']);?>, concreto, concreto, blocos, pisos, blocos de concreto, pavimenta&ccedil;&atilde;o intertravada ">
		
		<meta property="og:title"		content="<?=$dados['titulo'];?> - Concrefel">
		<meta property="og:description"	content="<?=$dados['titulo'];?>">
		
		<script type="text/javascript" src="js/core/jquery.maskedinput.min.js"></script>
	<meta charset="utf-8">
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>
		
		<main class="conteudo">
			<?php if($dados['imagem_topo'] != ""){ ?>
				<figure class="conteudo-banner">
					<img src="imgs/<?=$dados['imagem_topo'];?>" class="img-retina" alt="<?=$dados['legenda_topo'];?>">
				</figure>
			<?php } ?>
			
			<section class="conteudo-margin conteudo-padding">
			
				<p class="color-999 font-size-12"><a href="index.php" title="Concrefel">Concrefel</a> &nbsp;/&nbsp; <?=$dados['titulo'];?></p>
				<header class="margin-top-60">
					<h1 class="font-size-40 texto-titulo texto-headline"><?=$dados['titulo'];?></h1>
					<?php if(trim($dados['subtitulo']) != ""){ ?>
					<p>&nbsp;</p>
					<h2 class="color-666 font-size-24 texto-titulo"><?=$dados['subtitulo'];?></h2>
					<?php } ?>
				</header>
				<?php
				if(isset($_GET['resposta']) && $_GET['resposta'] == 101){
				?>
					<div class="form-mensagem form-mensagem-certo" id="mensagem-certo" style="display:block;">Obrigado, mensagem enviada com sucesso!</div>
				<?php
				}else if($_GET['resposta'] == 102 || $_GET['resposta'] == 103){
				?>
					<div class="form-mensagem form-mensagem-erros" id="mensagem-erros" style="display:block;">Ops. Ocorreu um erro. Tente novamente.</div>
				<?php
				}
				?>
				<div class="clear-both margin-top-80"></div>
				
				<?php if($dados['imagem_lado'] != ""){ ?>
					<figure class="conteudo-imagem conteudo-imagem-d">
						<a href="imgs/<?=$dados['imagem_lado']?>" class="conteudo-imagem-a" title="<?=$dados['legenda_lado'];?>" data-fancybox-group="gallery"><img src="includes/thumb.php?file=../imgs/<?=$dados['imagem_lado'];?>&w=255&h=300" alt="<?=$dados['legenda_lado'];?>" class="img-retina"></a>
						<figcaption class="conteudo-imagem-legenda"><?=$dados['legenda_lado'];?></figcaption>
					</figure>
				<?php } ?>
				
				<?php if(trim($dados['texto']) != ""){ ?>
					<div class="texto-paragrafo"><?=$dados['texto'];?></div>
				<?php } ?>
				<div class="clear-both margin-top-60"></div>
				
				
				<h2 class="font-size-28 texto-titulo">Envie sua d&uacute;vida ou sugest&atilde;o</h2>
				<form id="formulario" class="margin-top-20" method="post" action="ajax/fale-conosco.php">
					<div>
						<label for="nome" class="form-label">*Nome:</label>
						<input type="text" id="nome" name="nome" class="form-input" maxlength="250">
					</div>
					<div class="margin-top-20">
						<label for="mail" class="form-label">*E-mail:</label>
						<input type="email" id="mail" name="mail" class="form-input" maxlength="250">
					</div>
					<div class="margin-top-20">
						<label for="fone" class="form-label">*Telefone:</label>
						<input type="tel" id="fone" name="fone" class="form-input" maxlength="15">
					</div>
					<div class="margin-top-20">
						<label for="fone" class="form-label">*Endere&ccedil;o:</label>
						<input type="text" id="endereco" name="endereco" class="form-input" maxlength="250">
					</div>
					<div class="margin-top-20">
						<label for="fone" class="form-label">*Cidade / Estado:</label>
						<input type="text" id="cidade" name="cidade" class="form-input" maxlength="250">
					</div>
					<div class="margin-top-20">
						<label for="fone" class="form-label">*CPF ou CNPJ:</label>
						<input type="text" id="cpf" name="cpf" class="form-input" maxlength="20">
					</div>
					<div class="margin-top-20">
						<label for="mensagem" class="form-label">*Mensagem:</label>
						<textarea id="mensagem" name="mensagem" class="form-input form-textarea"></textarea>
					</div>
					<div class="form-mensagem form-mensagem-erros" id="mensagem-erros">Formul&aacute;rio n&atilde;o enviado, h&aacute; campos incorretos, verifique se preencheu todos campos obrigatórios(*) .</div>
					<div class="form-mensagem form-mensagem-certo" id="mensagem-certo">Obrigado, formul&aacute;rio enviado com sucesso. Responderemos sua mensagem o quanto antes.</div>
					<input type="submit" value="Enviar" id="enviar" class="form-botao margin-top-20">
				</form>
			</section>
			<div class="conteudo-linha"></div>
			
			<section class="conteudo-margin conteudo-padding" itemscope itemtype="https://schema.org/PostalAddress">
				<h2 class="font-size-28 texto-titulo">Onde estamos</h2>
				<div class="texto-paragrafo">
					<p>&nbsp;</p>
					<p><span itemprop="addressLocality">RS 122 - KM28</span> Bom Princ&iacute;pio, ao lado da pol&iacute;cia rodovi&aacute;ria - Fone: <span itemprop="telephone">(51) 3634.1516</span></p>
				</div>
				<div class="margin-top-20">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d222303.09090630818!2d-51.352478999999995!3d-29.473022!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951c0023d42715c3%3A0x6cb71f17304384e5!2sCONCREFEL+Artefatos+de+Concreto!5e0!3m2!1spt-BR!2sbr!4v1427118651323" frameborder="0" style="width:100%; height:30em; border:0;"></iframe>
				</div>
				<a href="https://www.google.com.br/maps/place/CONCREFEL+Artefatos+de+Concreto/@-29.473022,-51.352479,11z/data=!4m2!3m1!1s0x951c0023d42715c3:0x6cb71f17304384e5" target="_blank" class="botao home-noticias-botao box-shadow">Ver mapa completo</a>
				<div class="clear-both"></div>
			</section>
			<div class="conteudo-linha"></div>
			
			<?php require("includes/rodape.php"); ?>
		</main>
		
		<div class="clear-both"></div>
	</body>
</html>
<script>
$(document).ready(function(){
	
	var erros			= false,
		validar			= false,
		$enviar			= $("#enviar"),
		$formulario		= $("#formulario"),
		$mensagem_certo	= $("#mensagem-certo"),
		$mensagem_erros	= $("#mensagem-erros");
	
	$("#fone").mask("?(99) 9999.99999");
	
	validacao = (function(field, size) {
		validar = false;
		
		if (size == "mail"){
			part1 = $("#"+field).val().indexOf("@");
			part2 = $("#"+field).val().indexOf(".");
			part3 = $("#"+field).val().length;
			if (!((part1 >= 2) && (part2 >= 1) && (part3 >= 7))) { erros=true; validar=true; }
			else { validar=false; }
		}
		else {
			if ($("#"+field).val().length < size) { validar=true; erros=true; }
			else { validar=false; }
		}
		
		if (validar) { $("#"+field).addClass("form-input-erros"); }
		else { $("#"+field).removeClass("form-input-erros"); }
	});
	
	$("#nome").blur		(function(){ validacao("nome","2");		});
	$("#mail").blur		(function(){ validacao("mail","mail");	});
	$("#fone").blur		(function(){ validacao("fone","12");		});
	$("#endereco").blur	(function(){ validacao("endereco","5");	});
	$("#cidade").blur	(function(){ validacao("cidade","5");	});
	$("#cpf").blur		(function(){ validacao("cpf","11");		});
	$("#mensagem").blur	(function(){ validacao("mensagem","8");	});
	
	$enviar.click(function(e){
		e.preventDefault();
		$mensagem_certo.hide();
		$mensagem_erros.hide();
		
		erros = false;
		
		validacao("nome","2");
		validacao("mail","mail");
		validacao("fone","12");
		validacao("endereco","5");
		validacao("cidade","5");
		validacao("cpf","11");
		validacao("mensagem","8");
		
		if (erros) {
			$mensagem_erros.show();
			return false;
		}
		else {
			//$enviar.hide();
			$("#formulario").submit();
		}
		return false;
	});	
	
});
</script>