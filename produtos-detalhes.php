<!doctype html>
<html>
	<head>
		<?php require("includes/head.php"); 
		
		$idProduto = (int)$_GET['produto'];
		$consulta = consultar("*","produto","id=$idProduto");
		
		if(mysqli_num_rows ($consulta) == 0){
			echo 'Produto n&atilde;o encontrado. <META HTTP-EQUIV="Refresh" CONTENT="1; URL=produtos.php">';
			exit;
		}
		$produto = mysqli_fetch_assoc($consulta);
		//
		$idCat = $produto['categoria'];
		$consultaC = consultar("nome", "categoria", "id=$idCat ");
		$categoria = mysqli_fetch_assoc($consultaC);
		?>
		
		<title><?=$produto['titulo'];?> - <?=$categoria['nome'];?> - <?=$produto['subtitulo'];?> - Concrefel</title>
		<meta name="title"			content="<?=$produto['titulo'];?> - <?=$categoria['nome'];?> - <?=$produto['subtitulo'];?> - Concrefel">
		<meta name="description"	content="<?=quebra_texto($produto['texto'], 140);?>">
		<meta name="keywords"		content="<?=geraKeywords($categoria['nome']).", ".geraKeywords($produto['titulo']).", ".geraKeywords($produto['subtitulo']);?>, concreto">
		
		<meta property="og:title"		content="<?=$produto['titulo'];?> - <?=$categoria['nome'];?> - <?=$produto['subtitulo'];?> - Concrefel">
		<meta property="og:description"	content="<?=quebra_texto($produto['texto'], 140);?>">
		
		<?php require("includes/fancybox.php"); ?>
	<meta charset="utf-8">
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>
		
		<main class="conteudo" itemscope itemtype="https://schema.org/Product">
			<?php if($produto['imagem_topo'] != ""){ ?>
				<figure class="conteudo-banner">
					<img src="imgs/<?=$produto['imagem_topo'];?>" class="img-retina" alt="<?=$produto['legenda_topo'];?>" itemprop="image">
				</figure>
			<?php } ?>
			
			<div class="conteudo-margin conteudo-padding">
				<p class="color-999 font-size-12"><a href="index.php" title="Concrefel">Concrefel</a> &nbsp;/&nbsp; <a href="produtos.php" title="Produtos">Produtos</a> &nbsp;/&nbsp; <a href="produtos.php" title="<?=$categoria['nome'];?>"><?=$categoria['nome'];?></a>&nbsp;/&nbsp;<?=$produto['titulo'];?></p>
				<header class="margin-top-60">
					<h1 class="font-size-40 texto-titulo texto-headline" itemprop="about"><?=$produto['titulo'];?></h1>
					<?php if(trim($produto['subtitulo']) != ""){ ?>
					<p>&nbsp;</p>
					<h2 class="color-666 font-size-24 texto-titulo" itemprop="description"><?=$produto['subtitulo'];?></h2>
					<?php } ?>
					<p>&nbsp;</p>
					
					<div class="conteudo-social">
						<div class="conteudo-social-li">
							<div id="fb-root"></div>
							<script>(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));</script>
							<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
						</div>
						<div class="conteudo-social-li">
							<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
						</div>
						<div class="conteudo-social-li">
							<script src="https://apis.google.com/js/platform.js" async defer>{lang:'pt-BR'}</script>
							<div class="g-plus" data-action="share" data-annotation="none"></div>
						</div>
						<div class="clear-both"></div>
					</div>
				</header>
			</div>
			<div class="conteudo-linha"></div>
			
			<section class="conteudo-margin conteudo-padding">
				
				<?php if($produto['imagem_lado'] != ""){ ?>
					<figure class="conteudo-imagem conteudo-imagem-d">
						<a href="imgs/<?=$produto['imagem_lado']?>" class="conteudo-imagem-a" title="<?=$produto['legenda_lado'];?>" data-fancybox-group="gallery"><img src="includes/thumb.php?file=../imgs/<?=$produto['imagem_lado'];?>&w=255&h=300" alt="<?=$produto['legenda_lado'];?>" class="img-retina"></a>
						<figcaption class="conteudo-imagem-legenda"><?=$produto['legenda_lado'];?></figcaption>
					</figure>
				<?php } ?>
				
				<?php if(trim($produto['texto']) != ""){ ?>
					<div class="texto-paragrafo" itemprop="text">
						<?=$produto['texto'];?><br>
						
						<?php 
						$consulta_cores = consultar("c.cor AS cor,c.titulo AS titulo","cor c, cor_produto cp","c.id = cp.idCor AND cp.idProduto = $idProduto");
						if(mysqli_num_rows($consulta_cores) > 0){
							echo '<p>Cores dispon&iacute;veis:</p>';
							while($cor = mysqli_fetch_assoc($consulta_cores)){ ?>
							
							<div class="produtos-cores">
								<div class="produtos-cores-cor" style="background-color:<?=$cor['cor'];?>"></div>&nbsp;&nbsp;<?=$cor['titulo'];?>
							</div>
						
						<?php } 
						}?>
						
					</div>
				<?php } ?>
				<div class="clear-both"></div>
				<?php
				$cImagens = consultar("file,legenda", "imagem", "pai='produto' AND idPai=$idProduto ", "pos ASC, id ASC");
				if(mysqli_num_rows($cImagens)>0) { 
					?>
					<div class="conteudo-galeria">
						<?php 
						while($imgs = mysqli_fetch_assoc($cImagens)){
							$file = $imgs['file'];
							$legenda = $imgs['legenda'];
							if($legenda=='Foto'){$legenda=$produto['titulo'];}
							?>
							<a href="imgs/<?=$file;?>" class="conteudo-galeria-li" title="<?=$legenda;?>" data-fancybox-group="gallery"><img src="includes/thumb.php?file=../imgs/<?=$file;?>&w=255&h=500" alt="<?=$legenda;?>" class="img-retina" ></a>
						<?php } ?>
						<div class="clear-both"></div>
					</div>
				<?php } ?>
			</section>
			<div class="conteudo-linha"></div>
			
			<?php
			$cVideos = consultar("titulo,descricao,url", "video", "pai='produto' AND idPai=$idProduto AND url!='' ", "titulo ASC");
			while($video = mysqli_fetch_assoc($cVideos)){
				?>
				<section class="conteudo-margin conteudo-padding">
					
					<?php if(trim($video['titulo']) != ""){ ?>
						<h3 class="font-size-24 texto-titulo"><?=$video['titulo'];?></h3>
					<?php } ?>
					
					<?php if(trim($video['descricao']) != ""){ ?>
						<div class="texto-paragrafo"><?=$video['descricao'];?></div>
					<?php } ?>
					
					<div class="conteudo-video margin-top-60">
						<?=urlDoVideo($video['url'], 'player');?>
					</div>
				</section>
				<div class="conteudo-linha"></div>
			<?php } ?>
			
			<?php
			$cArquivos = consultar("arquivo,titulo", "arquivo", "pai='produto' AND idPai=$idProduto ", "titulo ASC");
			if(mysqli_num_rows($cArquivos) > 0){
				?>
				<section class="conteudo-margin conteudo-padding">
					<h3 class="font-size-24 texto-titulo">Arquivos</h3>
					<div class="conteudo-arquivos">
						<?php
						while($arquivo = mysqli_fetch_assoc($cArquivos)){
							$icone = iconeArquivo($arquivo['arquivo']);
							?>
							<a href="imgs/<?=$arquivo['arquivo'];?>" target="_blank" title="<?=$arquivo['titulo'];?>" class="conteudo-arquivos-li box-shadow">
								<div class="conteudo-arquivos-icone"><img src="img/arquivos-<?=$icone?>.png" alt="<?=extensaoPadrao($arquivo['arquivo']);?>" class="img-retina"></div>
								<div class="conteudo-arquivos-texto"><?=$arquivo['titulo'];?></div>
								<div class="clear-both"></div>
							</a>
						<?php } ?>
						
					<div class="clear-both"></div>
					</div>
				</section>
			<?php } ?>
			
			<section class="conteudo-margin conteudo-padding">
				<a href="orcamentos.php?produto=<?=$idProduto;?>" title="Solicitar or&ccedil;amento" style="color:#333;">
					<div class="pedidos-orcamento"><img src="img/produtos-orcamento.png" width="100%"></div>
					<div class="float-left">
						<h3 class="font-size-24 texto-titulo">Gostou deste produto?</h3>
						<div class="texto-paragrafo float-left" style="padding:0.28125em 0;">Solicite um or&ccedil;amento!</div>
					</div>
				</a>
			</section>
			<div class="margin-top-32"></div>
			
			<?php require("includes/rodape.php"); ?>
		</main>
		
		<div class="clear-both"></div>
	</body>
</html>