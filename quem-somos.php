<!doctype html>
<html>
	<head>
		<?php require("includes/head.php");
		$consulta = consultar("*","texto","id = 1");
		$dados = mysqli_fetch_assoc($consulta);
		?>
		
		<title><?=$dados['titulo'];?> - Concrefel</title>
		<meta name="title"			content="<?=$dados['titulo'];?> - Concrefel">
		<meta name="description"	content="<?php
		if (trim($dados['subtitulo'])){
			echo $dados['subtitulo'];
		} else {
			echo quebra_texto($dados['texto'], 140);
		}
		?>">
		<meta name="keywords" content="<?=geraKeywords($dados['titulo']).", ".geraKeywords($dados['subtitulo']);?>, concreto, concreto, blocos, pisos, blocos de concreto, pavimenta&ccedil;&atilde;o intertravada ">
		
		<meta property="og:title"		content="<?=$dados['titulo'];?> - Concrefel">
		<meta property="og:description"	content="<?php
		if (trim($dados['subtitulo'])){
			echo $dados['subtitulo'];
		} else {
			echo quebra_texto($dados['texto'], 140);
		}
		?>">
		
		<?php require("includes/fancybox.php"); ?>
	<meta charset="utf-8">
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>		
		<main class="conteudo">
			<?php if($dados['imagem_topo'] != ""){ ?>
				<figure class="conteudo-banner">
					<img src="imgs/<?=$dados['imagem_topo'];?>" class="img-retina" alt="<?=$dados['legenda_topo'];?>">
				</figure>
			<?php } ?>
			<section class="conteudo-margin conteudo-padding">
				<p class="color-999 font-size-12"><a href="index.php" title="Concrefel">Concrefel</a> &nbsp;/&nbsp; <?=$dados['titulo'];?></p>
				<header class="margin-top-60">
					<h1 class="font-size-40 texto-titulo texto-headline"><?=$dados['titulo'];?></h1>
					<?php if(trim($dados['subtitulo']) != ""){ ?>
					<p>&nbsp;</p>
					<h2 class="color-666 font-size-24 texto-titulo"><?=$dados['subtitulo'];?></h2>
					<?php } ?>
				</header>
				<div class="clear-both margin-top-80"></div>
				
				<?php if($dados['imagem_lado'] != ""){ ?>
					<figure class="conteudo-imagem conteudo-imagem-d">
						<a href="imgs/<?=$dados['imagem_lado']?>" class="conteudo-imagem-a" title="<?=$dados['legenda_lado'];?>" data-fancybox-group="gallery"><img src="includes/thumb.php?file=../imgs/<?=$dados['imagem_lado'];?>&w=255&h=300" alt="<?=$dados['legenda_lado'];?>" class="img-retina"></a>
						<figcaption class="conteudo-imagem-legenda"><?=$dados['legenda_lado'];?></figcaption>
					</figure>
				<?php } ?>
				
				<?php if(trim($dados['texto']) != ""){ ?>
					<div class="texto-paragrafo"><?=$dados['texto'];?></div>
				<?php } ?>
				
				<?php
				$cImagens = consultar("file,legenda", "imagem", "pai='texto' AND idPai=1 ", "pos ASC, id ASC");
				if(mysqli_num_rows($cImagens)>0) { 
					?>
					<div class="clear-both"></div>
					<div class="conteudo-galeria">
						<?php 
						while($imgs = mysqli_fetch_assoc($cImagens)){
							$file = $imgs['file'];
							$legenda = $imgs['legenda'];
							if($legenda=='Foto'){$legenda='';}
							?>
							<a href="imgs/<?=$file;?>" class="conteudo-galeria-li" title="<?=$legenda;?>" data-fancybox-group="gallery"><img src="includes/thumb.php?file=../imgs/<?=$file;?>&w=255&h=500" alt="<?=$legenda;?>" class="img-retina" ></a>
						<?php } ?>
						<div class="clear-both"></div>
					</div>
				<?php } ?>
			</section>
			<div class="conteudo-linha"></div>
			
			<?php
			$cVideos = consultar("titulo,descricao,url", "video", "pai='texto' AND idPai=1 AND url!='' ", "titulo ASC");
			while($video = mysqli_fetch_assoc($cVideos)){
				?>
				<section class="conteudo-margin conteudo-padding">
					
					<?php if(trim($video['titulo']) != ""){ ?>
						<h3 class="font-size-24 texto-titulo"><?=$video['titulo'];?></h3>
					<?php } ?>
					
					<?php if(trim($video['descricao']) != ""){ ?>
						<div class="texto-paragrafo"><?=$video['descricao'];?></div>
					<?php } ?>
					
					<div class="conteudo-video margin-top-60">
						<?=urlDoVideo($video['url'], 'player');?>
					</div>
				</section>
				<div class="conteudo-linha"></div>
			<?php } ?>
			
			<?php
			$cArquivos = consultar("arquivo,titulo", "arquivo", "pai='texto' AND idPai=1", "titulo ASC");
			if(mysqli_num_rows($cArquivos) > 0){
				?>
				<section class="conteudo-margin conteudo-padding">
					<h3 class="font-size-24 texto-titulo">Arquivos</h3>
					<div class="conteudo-arquivos">
						<?php
						while($arquivo = mysqli_fetch_assoc($cArquivos)){
							$icone = iconeArquivo($arquivo['arquivo']);
							?>
							<a href="imgs/<?=$arquivo['arquivo'];?>" target="_blank" title="<?=$arquivo['titulo'];?>" class="conteudo-arquivos-li box-shadow">
								<div class="conteudo-arquivos-icone"><img src="img/arquivos-<?=$icone?>.png" alt="<?=extensaoPadrao($arquivo['arquivo']);?>" class="img-retina"></div>
								<div class="conteudo-arquivos-texto"><?=$arquivo['titulo'];?></div>
								<div class="clear-both"></div>
							</a>
						<?php } ?>
						
					<div class="clear-both"></div>
					</div>
				</section>
				<div class="conteudo-linha"></div>
			<?php } ?>
			
			<?php require("includes/rodape.php"); ?>
		</main>
		<div class="clear-both"></div>
	</body>
</html>