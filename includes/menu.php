<div class="lateral" id="lateral">
	<div class="lateral-wrap">
		<header class="lateral-topo">
			<a href="javascript:;" title="Abrir menu" class="lateral-topo-menu" id="lateral-topo-menu"><img src="img/icone-menu.png" alt="Menu" class="img-retina"></a>
			<a href="index.php" title="Concrefel - Artefatos de concreto" class="lateral-topo-logo"><img src="img/logo-concrefel.png" alt="Concrefel - Artefatos em concreto" class="img-retina"></a>
		</header>
		<div class="lateral-mobile" id="lateral-mobile">
			<nav class="lateral-menu">
				<div class="lateral-colunas">
					<a href="index.php" title="Home"					class="lateral-menu-a"><span class="lateral-menu-a-span">Home</span></a>
					<a href="quem-somos.php" title="Quem somos"		class="lateral-menu-a"><span class="lateral-menu-a-span">Quem somos</span></a>
					<a href="produtos.php" title="Produtos"			class="lateral-menu-a"><span class="lateral-menu-a-span">Produtos</span></a>
					<a href="orcamentos.php" title="Orçamentos"		class="lateral-menu-a"><span class="lateral-menu-a-span">Or&ccedil;amentos</span></a>
				</div>
				<div class="lateral-2">
					<a href="noticias.php" title="Not&iacute;cias"				class="lateral-menu-a"><span class="lateral-menu-a-span">Not&iacute;cias</span></a>
					<a href="fotos-de-obras.php" title="Fotos de obras"	class="lateral-menu-a"><span class="lateral-menu-a-span">Fotos de obras</span></a>
					<a href="representantes.php" title="Representantes"	class="lateral-menu-a"><span class="lateral-menu-a-span">Representantes</span></a>
					<a href="fale-conosco.php" title="Fale conosco"		class="lateral-menu-a"><span class="lateral-menu-a-span">Fale conosco</span></a>
				</div>
				<div class="clear-both"></div>
			</nav>
			<section class="lateral-redes-sociais">
				<a href="javascript:;" title="Concrefel no Facebook" class="lateral-redes-sociais-a" target="_blank">
					<img src="img/redes-sociais-facebook.png" alt="&Iacute;cone do Facebook" class="img-retina">
				</a>
				
				<a href="javascript:;" title="Concrefel no Twitter"  class="lateral-redes-sociais-a" target="_blank">
					<img src="img/redes-sociais-twitter.png"  alt="&Iacute;cone do Twitter"  class="img-retina">
				</a>
			</section>
			<section class="lateral-rodape">
				<div class="lateral-colunas">
					<p>(51) 3634.1516</p>
					<p>contato@concrefel.com.br</p>
				</div>
				<div class="lateral-colunas">
					<p>RS122, Km 28 - Bom Princ&iacute;pio, RS</p>
					<p>Ao lado da pol&iacute;cia rodovi&aacute;ria.</p>
				</div>
				<div class="clear-both"></div>
			</section>
			<section class="lateral-rodape">
				<p>Concrefel &copy; 2015</p>
				<p>Todos os direitos reservados.</p>
			</section>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	
	var $lateral_topo_menu	= $("#lateral-topo-menu"),
		$lateral_mobile		= $("#lateral-mobile");
	
	$lateral_topo_menu.click(function(){
		if ($lateral_mobile.hasClass("lateral-mobile-show"))
			$lateral_mobile.removeClass("lateral-mobile-show");
		else
			$lateral_mobile.addClass("lateral-mobile-show");
	});
	
});
</script>