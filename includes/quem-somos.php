<?php
$consultarTexto = consultar("titulo,subtitulo,imagem_topo,imagem_lado","texto","id=1");
$ver = mysqli_fetch_assoc($consultarTexto);
if($ver['imagem_topo'] != ""){
	$imagem = $ver['imagem_topo'];
}else if($ver['imagem_lado'] != ""){
	$imagem = $ver['imagem_lado'];
}else{
	$imagem = "";
}
?>
<div class="conteudo-margin conteudo-padding">
	<h2 class="texto-titulo texto-headline font-size-32">Quem &eacute; a Concrefel?</h2>
	<p>&nbsp;</p>
	
	<?php if($ver['subtitulo']) { ?>
	<div class="texto-paragrafo">
		<p><?=$ver['subtitulo'];?></p>
	</div>
	<p>&nbsp;</p>
	<?php } ?>
	<?php if($imagem) { ?>
	<img src="imgs/<?=$imagem;?>" alt="<?=$ver['titulo'];?>" class="imagem box-shadow img-retina" >
	<p>&nbsp;</p>
	<?php } ?>
	<a href="quem-somos.php" title="Saiba mais sobre a Concrefel" class="botao home-noticias-botao box-shadow">Saiba mais</a>
	<div class="clear-both"></div>
</div>