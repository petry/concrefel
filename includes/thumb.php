<?php ini_set('memory_limit', '64M');
$a['document_root'] = $_SERVER['DOCUMENT_ROOT'];
$b = 128;
$a['absolute_uri'] = str_replace('///', '//', str_replace('thumb.php?' . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']));
$a['path_thumbnail'] = $a['absolute_uri'] . '/cache/images/';
$c = $a['document_root'] . $a['absolute_uri'] . '/images/icons/image_error.png';
$d = 0;
$e = 0;
if (@$_GET["quality"] <> 0) {
    $f = $_GET["quality"];
} else {
    $f = 80;
}
if (@$_GET["size"] <> 0) {
    $b = intval($_GET["size"]);
}
if (intval(@$_GET["w"]) > 0) {
    $d = intval($_GET["w"]);
    if (intval(@$_GET["h"]) > 0) {
        $e = intval($_GET["h"]);
    } else {
        $e = $d;
    }
}
if (file_exists($_GET['file'])) {
    $g = $_GET['file'];
} else {
    $g = str_replace('//', '/', $a['document_root'] . $a['absolute_uri'] . '/' . $_GET["file"]);
}
if (substr_count($g, "http://") > 0) {
    $g = str_replace($a['document_root'] . $a['absolute_uri'] . '/', '', $g);
}
$g = str_replace("\'", "'", $g);
$g = rtrim($g);
$g = str_replace("//", "/", $g);
$h = substr($g, strrpos($g, ".") + 1);
$i = str_replace('//', '/', $a['document_root'] . $a['path_thumbnail'] . md5($g . @$b . @$d . @$e . @$f) . '.' . $h);
if (@$_GET['nocache'] == 1) {
    if (file_exists($i)) {
        unlink($i);
    }
}
if ((file_exists($i)) && (@filemtime($i) > @filemtime($g))) {
    header('Content-type: image/' . $h);
    header("Expires: Mon, 26 Jul 2030 05:00:00 GMT");
    header('Content-Disposition: inline; filename=' . str_replace('/', '', md5($g . $b . $d . $e . $f) . '.' . $h));
    echo(join('', file($i)));
    exit;
}
$j = intval(str_replace(".", "", phpversion()));
if ($j >= 430) {
    $k = @gd_info();
}
if (!$l = @getimagesize($g)) {
    header('Content-type: image/png');
    if (@$_GET['noerror']) {
        exit;
    } else {
        echo(join('', file($a['document_root'] . $c)));
        exit;
    }
}
$m = $l[2];
switch ($m) {
    case 2:
        if (!$n = @imagecreatefromjpeg($g)) {
            $n = imagecreatefrompng($c);
            $o = "png";
            if (file_exists($i)) {
                unlink($i);
            }
        }
        break;
    case 3:
        if (!$n = @imagecreatefrompng($g)) {
            $n = imagecreatefrompng($c);
            $o = "png";
            if (file_exists($i)) {
                unlink($i);
            }
        }
        break;
    case 1:
        if (!$n = @imagecreatefromgif($g)) {
            $n = imagecreatefrompng($c);
            $o = "png";
            if (file_exists($i)) {
                unlink($i);
            }
        }
        break;
    default:
        $n = imagecreatefrompng($c);
        break;
}
$p = imagesx($n);
$q = imagesy($n);
if (@$d > 0) {
    $r = $d;
    $s = $p / $d;
    $t = intval($q / $s);
    if ($t > $e) {
        $t = $e;
        $s = $q / $e;
        $r = intval($p / $s);
    }
} else {
    $r = $b;
    $s = $p / $b;
    $t = intval($q / $s);
    if ($t > $b) {
        $t = $b;
        $s = $q / $b;
        $r = intval($p / $s);
    }
}
if ($p < 4000) {
    $u = imageistruecolor($n);
    if ($u) {
        $v = imagecreatetruecolor($r, $t);
        imagealphablending($v, false);
        imagesavealpha($v, true);
    } else {
        $v = imagecreate($r, $t);
        imagealphablending($v, false);
        $w = imagecolorallocatealpha($v, 0, 0, 0, 127);
        imagefill($v, 0, 0, $w);
        imagesavealpha($v, true);
        imagealphablending($v, true);
    }
    if (substr_count(strtolower($k['GD Version']), "2.") > 0) {
        imagecopyresampled($v, $n, 0, 0, 0, 0, $r, $t, $p, $q);
    } else {
        $v = imagecreate($r, $t);
        imagecopyresized($v, $n, 0, 0, 0, 0, $r, $t, $p, $q);
    }
} else {
    if (substr_count(strtolower($k['GD Version']), "2.") > 0) {
        imagecopyresized($v, $n, 0, 0, 0, 0, $r, $t, $p, $q);
    } else {
        $v = imagecreate($r, $t);
        imagecopyresized($v, $n, 0, 0, 0, 0, $r, $t, $p, $q);
    }
}
if (@$_GET['tag'] <> "") {
    $x = 1;
    $y = $_GET['tag'];
    $z = imagecolorallocate($v, 255, 255, 255);
    $aa = imagecolorallocate($v, 0, 0, 0);
    imagestring($v, $x, 3, $t - 9, $y, $aa);
    imagestring($v, $x, 2, $t - 10, $y, $z);
}
switch ($m) {
    case 2:
        header('Content-type: image/jpeg');
        header('Content-Disposition: inline; filename=' . str_replace('/', '', md5($g . $b . $d . $e . $f) . '.jpeg'));
        @imagejpeg($v, $i, $f);
        imagejpeg($v, null, $f);
        break;
    case 3:
        header('Content-type: image/png');
        header('Content-Disposition: inline; filename=' . str_replace('/', '', md5($g . $b . $d . $e . $f) . '.png'));
        @imagepng($v, $i);
        imagepng($v);
        break;
    case 1:
        if (function_exists('imagegif')) {
            header('Content-type: image/gif');
            header('Content-Disposition: inline; filename=' . str_replace('/', '', md5($g . $b . $d . $e . $f) . '.gif'));
            @imagegif($v, $i);
            imagegif($v);
        } else {
            header('Content-type: image/jpeg');
            header('Content-Disposition: inline; filename=' . str_replace('/', '', md5($g . $b . $d . $e . $f) . '.jpg'));
            @imagejpeg($v, $i);
            imagejpeg($v);
        }
        break;
}
imagedestroy($n);
imagedestroy($v); ?>