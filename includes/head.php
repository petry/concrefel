<meta charset="utf-8">
<meta name="robots"		content="all">
<meta name="language"	content="pt-br">
<meta name="generator"	content="http://www.concrefel.com.br/">
<meta name="webmaster"	content="isDesign">
<meta name="author"		content="http://www.isdesign.com.br/">
<meta name="viewport"	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<link type="image/x-icon"	rel="apple-touch-icon"	href="img/favoritos-apple.png">
<link type="image/x-icon"	rel="shortcut icon"		href="img/favoritos-icone.png">
<link type="text/css"		rel="stylesheet"			href="estilos.css">

<script type="text/javascript" src="js/core/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/core/retina.min.js"></script>
<script type="text/javascript" src="js/core/html5shiv.min.js"></script>
<script type="text/javascript" src="js/core/hammer-2.0.3.min.js"></script>
<?php require_once("painel/codigos/funcoesPainel.php"); ?>