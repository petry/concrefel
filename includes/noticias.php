<?php
$consultaN = consultar("id,titulo,subtitulo,texto,imagem_topo,imagem_lado", "noticia", "ativo=1 AND capa=1 AND (imagem_topo !='' OR imagem_lado !='')", "data DESC", 0,2);
$cont = 0;
$idsNoticias = '';
while($ultimas = mysqli_fetch_assoc($consultaN)){
	$cont++;
	$idsNoticias .= $ultimas['id'].",";
	
	$imgN = $ultimas['imagem_topo'];
	if ($ultimas['imagem_topo'] == ""){
		$imgN = $ultimas['imagem_lado'];
	}
	
	//para trocar o lado salvo a div numa vari&aacute;vel	
	$ladoImg = '<div class="home-noticias-imagem"><img src="includes/thumb.php?file=../imgs/'.$imgN.'&w=400" alt="'.$ultimas['titulo'].'" class="img-retina" itemprop="image"></div>'
?>
<a href="noticias-detalhes.php?noticia=<?=$ultimas['id'];?>" title="<?=$ultimas['titulo'];?>" class="home-noticias box-shadow" itemprop="url">
	<?php if($cont % 2 != 0){ echo $ladoImg; }?>
	<div class="home-noticias-texto">
		<p class="font-size-20 texto-titulo" itemprop="about"><?=$ultimas['titulo'];?></p>
		<p>&nbsp;</p>
		<p class="texto-paragrafo" itemprop=""><?php
		if (trim($ultimas['subtitulo'])){
			echo $ultimas['subtitulo'];
		} else {
			echo quebra_texto($ultimas['texto'], 140);
		}
		?></p>
		<p>&nbsp;</p>
		<p class="texto-link">Saiba mais</p>
	</div>
	<?php if($cont % 2 == 0){ echo $ladoImg; }?>
	<div class="clear-both"></div>
</a>
<?php } ?>

<script type="text/javascript">
$(document).ready(function(){
	var $window					= $(window),
		$lateral				= $("#lateral"),
		$home_noticias			= $(".home-noticias"),
		$home_noticias_imagem	= $(".home-noticias-imagem");
	
	$fn_noticias_resize = (function(){
		$home_noticias_imagem.css("height", $home_noticias_imagem.width() * 0.75);
		
		if ($lateral.css("float") == "left") $home_noticias.css("height", $home_noticias_imagem.width() * 0.75);
		else $home_noticias.css("height", "auto");
	});
	
	$window.resize(function(){ $fn_noticias_resize(); });
	$fn_noticias_resize();
});
</script>