<div class="conteudo-margin conteudo-padding">
	<h2 class="texto-titulo texto-headline font-size-32">Nossos produtos</h2>
	<p>&nbsp;</p>
	<p class="texto-paragrafo">Blocos esruturais e pisos de concreto com a qualidade que sua obra merece.</p>
	<p>&nbsp;</p>
	<div class="home-produtos">
		<?php
		$consulta1 = consultar("p.id as id, titulo, subtitulo, c.nome as categoria, imagem_lado", "produto p, categoria c", "p.categoria=c.id AND p.capa=1 AND p.ativo=1 AND c.ativo=1 AND imagem_lado !=''", "RAND()",0,6);
		while($destaque = mysqli_fetch_assoc($consulta1)){
			?>
			<a href="produtos-detalhes.php?produto=<?=$destaque['id'];?>" title="<?=$destaque['categoria']." - ".$destaque['titulo'];?>" class="home-produtos-li box-shadow">
				<div class="home-produtos-li-imagem"><img src="includes/thumb.php?file=../imgs/<?=$destaque['imagem_lado']?>&w=120&h=120" alt="<?=$destaque['categoria']." - ".$destaque['titulo'];?>" class="img-retina"></div>
				<div class="home-produtos-li-texto">
					<p class="home-produtos-li-texto-titulo"><?=$destaque['titulo'];?></p>
					<p class="home-produtos-li-texto-descricao"><?=$destaque['categoria'];?></p>
				</div>
			</a>
		<?php
		}
		?>
		<div class="clear-both"></div>
		<a href="produtos.php" title="Mais produtos" class="botao home-produtos-botao box-shadow">Mais produtos</a>
		<div class="clear-both"></div>
	</div>
</div>