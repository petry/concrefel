<div class="anime">
	<?php
	$banners = consultar("titulo,subtitulo,link,file", "banners", "exibirAPartir < NOW() AND ativo=1", "pos ASC", 0,6);
	while($banner = mysqli_fetch_assoc($banners)){
		if($banner['link'] == ""){
			$link = 'javascript:;';
			$cursor = 'style="cursor:default; "';
		}else{
			$link = $banner['link'];
			$cursor = '';
			
			if(strstr($link,"concrefel.com.br") || !strstr($link,"www.") ){
				$target = "";
			} else {
				$target = "target='_blank'";
			}
		}
	?>
	<a href="<?=$link;?>" class="anime-a" <?=$target.' '.$cursor;?>>
		<div class="anime-a-overlay">
			<div class="anime-a-texto">
				<?php if($banner['titulo']){ ?>
				<p class="anime-a-titulo"><?=$banner['titulo'];?></p>
				<?php } 
				if($banner['subtitulo']){ ?>
					<p class="anime-a-descricao"><?=$banner['subtitulo'];?></p>
				<?php } ?>
			</div>
		</div>
		<img src="includes/thumb.php?file=../imgs/<?=$banner['file'];?>&w=476" class="anime-a-imagem" height="auto">
	</a>
	<?php
	}
	?>
	<div class="clear-both"></div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	
	var $window		= $(window),
		$anime_a	= $(".anime-a");
	
	$fn_anime_resize = (function(){
		$anime_a.css("padding-bottom", $anime_a.width() / 4 * 3);
	});
	
	$window.resize	(function(){ $fn_anime_resize(); })
	$fn_anime_resize();
	
	$window.load(function(){ 
		$(".anime-a-imagem").addClass("anime-a-imagem-ativo");
	});
});
</script>