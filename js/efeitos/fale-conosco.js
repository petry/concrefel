$(document).ready(function(){
	
	var erros			= false,
		validar			= false,
		$enviar			= $("#enviar"),
		$formulario		= $("#formulario"),
		$mensagem_certo	= $("#mensagem-certo"),
		$mensagem_erros	= $("#mensagem-erros");
	
	$("#fone").mask("?(99) 9999.99999");
	
	validacao = (function(field, size) {
		validar = false;
		
		if (size == "mail"){
			part1 = $("#"+field).val().indexOf("@");
			part2 = $("#"+field).val().indexOf(".");
			part3 = $("#"+field).val().length;
			if (!((part1 >= 2) && (part2 >= 1) && (part3 >= 7))) { erros=true; validar=true; }
			else { validar=false; }
		}
		else {
			if ($("#"+field).val().length < size) { validar=true; erros=true; }
			else { validar=false; }
		}
		
		if (validar) { $("#"+field).addClass("form-input-erros"); }
		else { $("#"+field).removeClass("form-input-erros"); }
	});
	
	$("#nome").blur		(function(){ validacao("nome","2");			});
	$("#mail").blur		(function(){ validacao("mail","mail");		});
	$("#fone").blur		(function(){ validacao("fone","12");		});
	$("#mensagem").blur	(function(){ validacao("mensagem","10");	});
	
	$enviar.click(function(e){
		e.preventDefault();
		$mensagem_certo.hide();
		$mensagem_erros.hide();
		
		erros = false;
		
		validacao("nome","2");
		validacao("mail","mail");
		validacao("fone","12");
		validacao("mensagem","10");
		
		if (erros) {
			$mensagem_erros.show();
			return false;
		}
		else {
			$enviar.hide();
			
			var nome			= escape($("#nome").val()),
				mail		= escape($("#mail").val()),
				fone		= escape($("#fone").val()),
				endereco	= escape($("#endereco").val()),
				cpf			= escape($("#cpf").val()),
				mensagem	= escape($("#mensagem").val());
			
			$.post("ajax/fale-conosco.php", {
				nome:nome, mail:mail, fone:fone, endereco:endereco, cpf:cpf, mensagem:mensagem
			}, function(resposta){
				if(resposta=="ok"){
					$enviar.show();
					$mensagem_certo.show();
					$formulario[0].reset();
				}
			});
		}
		return false;
	});	
	
});