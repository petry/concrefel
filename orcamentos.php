<!doctype html>
<html>
	<head>
		<?php require("includes/head.php"); 
		$idProduto = (int)$_GET['produto'];
		?>
		
		<title>Solicite um Or&ccedil;amento - Concrefel</title>
		<meta name="title"			content="Solicite um Or&ccedil;amento - Concrefel">
		<meta name="description"	content="Gostou de algum produto da Concrefel? Solicite um or&ccedil;amento sem compromisso.">
		<meta name="keywords"		content="or&ccedil;amentos, orcamento, pedidos, blocos, pisos, concreto, rs, brasil">
		
		<meta property="og:title"		content="Solicite um Or&ccedil;amento - Concrefel">
		<meta property="og:description"	content="Gostou de algum produto da Concrefel? Solicite um or&ccedil;amento sem compromisso.">
		
		<script type="text/javascript" src="js/core/jquery.maskedinput.min.js"></script>
	<meta charset="utf-8">
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>
		
		<main class="conteudo">
			<div class="conteudo-margin conteudo-padding">
				<p class="color-999 font-size-12"><a href="index.php" title="Concrefel">Concrefel</a> &nbsp;/&nbsp; Or&ccedil;amentos</p>
				<header class="margin-top-60">
					<h1 class="font-size-40 texto-titulo texto-headline">Or&ccedil;amentos</h1>
				</header>
				<section class="texto-paragrafo">
					<p>&nbsp;</p>
					<p>Gostou de algum produto da Concrefel? Solicite um or&ccedil;amento sem compromisso. Basta preencher os campos abaixo e aguardar o nosso retorno.</p>
				</section>
				<?php
				if(isset($_GET['resposta']) && $_GET['resposta'] == 101){
				?>
					<div class="form-mensagem form-mensagem-certo" id="mensagem-certo" style="display:block;">Obrigado, or&ccedil;amento enviado com sucesso!</div>
				<?php
				}else if($_GET['resposta'] == 102 || $_GET['resposta'] == 103 || $_GET['resposta'] == 104){
				?>
					<div class="form-mensagem form-mensagem-erros" id="mensagem-erros" style="display:block;">Ops. Ocorreu um erro. Tente novamente.</div>
				<?php
				}
				?>
				<section class="margin-top-60">
					<form id="formulario-orcamentos" method="post" action="ajax/orcamentos.php">
						<div>
							<label for="nome" class="form-label">*Nome:</label>
							<input type="text" id="nome" name="nome" class="form-input" maxlength="250">
						</div>
						<div class="margin-top-20">
							<label for="mail" class="form-label">*E-mail:</label>
							<input type="email" id="mail" name="email" class="form-input" maxlength="250">
						</div>
						<div class="margin-top-20">
							<label for="fone" class="form-label">*Telefone:</label>
							<input type="tel" id="fone" name="fone" class="form-input" maxlength="15">
						</div>
						<div class="margin-top-20">
							<label for="fone" class="form-label">*Endere&ccedil;o:</label>
							<input type="text" id="endereco" name="endereco" class="form-input" maxlength="250">
						</div>
						<div class="margin-top-20">
							<label for="fone" class="form-label">*Cidade / Estado:</label>
							<input type="text" id="cidade" name="cidade" class="form-input" maxlength="250">
						</div>
						<div class="margin-top-20">
							<label for="fone" class="form-label">*CPF ou CNPJ:</label>
							<input type="text" id="cpf" name="cpf" class="form-input" maxlength="20">
						</div>
						
						<div class="conteudo-linha margin-20"></div>
						
						<!-- -->
						<div id="produtos">
							<div id="content-var">
								<div class="pedidos-produtos-1 margin-top-40">
									<label for="produto" class="form-label">Produto:</label>
									<div class="form-input">
										<select id="produto" name="produto[]" class="form-select produtoscores" data-id="var">
											<option value="0" selected>- - Selecione - - </option>
											<?php
											$consulta = consultar("p.id AS id, c.nome AS categoria, p.titulo AS titulo, p.categoria as idCat", "categoria c, produto p", "c.id=p.categoria AND c.ativo=1 AND p.ativo=1");
											while($ver = mysqli_fetch_assoc($consulta)){
												?>
												<option value="<?=$ver['id'];?>" <?php 
												if($ver['id'] == $idProduto) { echo 'selected'; } 
												?> data-cat="<?=$ver['idCat'];?>"><?=$ver['categoria']." - ".$ver['titulo'];?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="pedidos-produtos-gap-1"></div>
								<div class="pedidos-produtos-2 margin-top-20">
									<label for="quantidade" class="form-label">Quantidade:</label>
									<input type="number" min="1" id="quantidade" name="quantidade[]" class="form-input">
								</div>
								<div class="pedidos-produtos-gap-2"></div>
								<div id="cores-var" class="pedidos-produtos-2 margin-top-20 cores" style="width:22.41666666666667%; margin-right:3.333333333333333%;">
									<label for="cor" class="form-label">Cor:</label>
									<div class="form-input">
										<select id="cor-var" name="cor[]" class="form-select">
											<option value="0" selected>- - Selecione - - </option>
										</select>
									</div>
								</div>
								
								<div class="pedidos-produtos-2 float-left margin-top-20" style="width:5.25%;">
									<div class="margin-top-20"></div>
									<a href="javascript:;" class="botao pedidos-botao box-shadow orcamentos-remover" title="Remover" data-id="var">X</a>
								</div>
								
								<div class="clear-both"></div>
								
								<div class="conteudo-linha margin-20"></div>
							</div>
						</div>
						<!-- -->
						
						<div class="pedidos-produtos-1 float-left">
							<a href="javascript:;" id="adicionar" class="botao pedidos-botao box-shadow" title="Adicionar">Adicionar mais itens</a>
						</div>
						
						<div class="clear-both"></div>
						
						<div class="margin-top-60">
							<label for="observacoes" class="form-label">*Observa&ccedil;&otilde;es:</label>
							<textarea id="observacoes" name="observacoes" class="form-input form-textarea"></textarea>
						</div>
						
						<div class="form-mensagem form-mensagem-erros" id="mensagem-erros">Or&ccedil;amento n&atilde;o enviado, h&aacute; campos incorretos verifique se preencheu todos campos obrigatórios(*).</div>
						
						<input type="submit" value="Enviar" id="enviar" class="form-botao margin-top-20">
					</form>
				</section>
			</div>
			<div class="conteudo-linha"></div>
			
			<?php require("includes/rodape.php"); ?>
		</main>
		
		<div class="clear-both"></div>
	</body>
	
<script type="text/javascript">
$(function(){
	
	/* adicionar itens */
	var $form_produtos	= $(".form-produtos"),
		$produtos		= $("#produtos"),
		$adicionar 		= $("#adicionar"),
		_numero			= 1;
	
	_var = $produtos.html();
	//console.log(_var.replace(/var/g, _numero));
	$produtos.html(_var.replace(/var/g, _numero));
	
	$adicionar.click(function(){
		_numero++;
		$produtos.append(_var.replace(/var/g, _numero));
		$("#content-"+_numero).find(".produtoscores").change();
	});
	
	/* remover itens */
	$("body").delegate(".orcamentos-remover","click",function(){ 
		var quem = $(this).attr('data-id');
		//console.log('remover '+quem);
		$("#content-"+quem).remove();
	});
	
	/* cores */
	$("body").delegate(".produtoscores","change",function(){ 
		var idProd = $("option:selected", this).val();
		var quem = $(this).attr('data-id');
		//console.log('idProd '+idProd);
		//
		$.post("ajax/buscar_cores.php", {
			produto:idProd
		}, function(resposta) {
			//console.log("resposta: "+resposta);
			$("#cor-"+quem).html(resposta);
			
			if(resposta=='<option value="0" selected>- - Selecione - - </option>') {
				$("#cor-"+quem+" option[value='0']").attr("selected", true);
				$("#cores-"+quem).hide();
			} else {
				$("#cores-"+quem).show();
				$("#cor-"+quem).show();
			}
		});
	});
	$(".produtoscores").change();
	
	////// 
	
	/* validação */
	var erros			= false,
		validar			= false,
		$enviar			= $("#enviar"),
		$formulario		= $("#formulario-orcamentos"),
		$mensagem_erros	= $("#mensagem-erros");
	
	$("#fone").mask("?(99) 9999.99999");
	
	validacao = (function(field, size) {
		validar = false;
		
		if (size == "mail"){
			part1 = $("#"+field).val().indexOf("@");
			part2 = $("#"+field).val().indexOf(".");
			part3 = $("#"+field).val().length;
			if (!((part1 >= 2) && (part2 >= 1) && (part3 >= 7))) { erros=true; validar=true; }
			else { validar=false; }
		}
		else {
			if ($("#"+field).val().length < size) { validar=true; erros=true; }
			else { validar=false; }
		}
		
		if (validar) { $("#"+field).addClass("form-input-erros"); }
		else { $("#"+field).removeClass("form-input-erros"); }
	});
	
	$("#nome").blur			(function(){ validacao("nome","2");		   	});
	$("#mail").blur			(function(){ validacao("mail","mail");	   	});
	$("#fone").blur			(function(){ validacao("fone","12");		   	});
	$("#cpf").blur			(function(){ validacao("cpf","11");		   	});
	$("#endereco").blur		(function(){ validacao("endereco","5");		});
	$("#cidade").blur		(function(){ validacao("cidade","5");		});
	$("#observacoes").blur	(function(){ validacao("observacoes","2");	});
	
	$enviar.click(function(e){
		e.preventDefault();
		
		erros = false;
		
		validacao("nome","2");
		validacao("mail","mail");
		validacao("fone","12");
		validacao("cpf","11");
		validacao("endereco","5");
		validacao("cidade","5");
		validacao("observacoes","2");
		
		if (erros) {
			$mensagem_erros.show();
			return false;
		} else {
			$("#formulario-orcamentos").submit();
		}
	});
});
</script>
</html>