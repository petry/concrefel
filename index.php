<!doctype html>
<html>
	<head>
		<?php require("includes/head.php"); ?>
		
		<title>Concrefel - Blocos e pisos de concreto.</title>
		<meta name="title"			content="Concrefel - Blocos e pisos de concreto.">
		<meta name="description"	content="A Concrefel, empresa localizada em Bom Princ&iacute;pio-RS, trabalha com a fabrica&ccedil;&atilde;o e instala&ccedil;&atilde;o de blocos estruturais e pisos de contreto.">
		<meta name="keywords"	content="concrefel, tijolos, blocos, pisos, unistein, holandes, raquete, canaleta, meio bloco, piso, vale do ca&iacute;, bom princ&iacute;pio, porto alegre, brasil">
		
		<meta property="og:title"		content="Concrefel - Blocos e pisos de concreto.">
		<meta property="og:description"	content="A Concrefel, empresa localizada em Bom Princ&iacute;pio-RS, trabalha com a fabrica&ccedil;&atilde;o e instala&ccedil;&atilde;o de blocos estruturais e pisos de contreto.">
	</head>
	
	<body>
		<?php require("includes/menu.php"); ?>
		
		<div class="conteudo">
			<?php require("includes/anime.php"); ?>
			
			<?php require("includes/produtos.php"); ?>
			
			<div class="conteudo-linha"></div>
			
			<div class="conteudo-margin conteudo-padding" itemscope itemtype="https://schema.org/NewsArticle">
				<h2 class="texto-titulo texto-headline font-size-32">&Uacute;ltimas not&iacute;cias</h2>
				<div class="texto-paragrafo">
					<p>&nbsp;</p>
					<p>Fique por dentro de nossas &uacute;ltimas not&iacute;cias referentes &aacute;s obras e produtos da Concrefel.</p>
				</div>
				<?php require("includes/noticias.php"); ?>
				<a href="noticias.php" title="Mais not&iacute;cias" class="botao home-noticias-botao box-shadow">Mais not&iacute;cias</a>
				<div class="clear-both"></div>
			</div>
			
			<div class="conteudo-linha"></div>
			
			<?php require("includes/quem-somos.php"); ?>
			
			<div class="conteudo-linha"></div>
			
			<?php require("includes/rodape.php"); ?>
		</div>
		<div class="clear-both"></div>
	</body>
</html>