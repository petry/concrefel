<?php

require_once("../phpmailer/class.phpmailer.php");

$nome		= $_POST['nome'];
$mail		= $_POST['mail'];
$fone		= $_POST['fone'];
$endereco	= $_POST['endereco'];
$cidade		= $_POST['cidade'];
$cpf		= $_POST['cpf'];
$mensagem	= nl2br($_POST['mensagem']);
$data		= date('d/m/Y');
$r = "";
if($nome && $mail && $mensagem) {
	
	$destino	= "contato@concrefel.com.br";
	//$destino	= "jarbasorlandin@gmail.com";
	$assunto	= "Site Concrefel - Fale Conosco - ".$nome;
	
	$arquivo = "<html>
	<head>
		<title>Concrefel: Pedido</title>
		<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	</head>
	<body text='#333333' link='#333333' vlink='#333333' alink='#333333'>
		<table width='600' border='0' cellspacing='2' cellpadding='8'>
		  <tr>
			<td colspan='2' bgcolor='#F9F9F9'>Bom Princ&iacute;pio, $data</td>
		  </tr>
		  <tr>
			<td width='155' align='right' bgcolor='#F9F9F9'>Nome:</td>
			<td width='407' align='left' bgcolor='#F9F9F9'><strong>$nome</strong></td>
		  </tr>
		  <tr>
			<td align='right' bgcolor='#F9F9F9'>E-mail:</td>
			<td align='left' bgcolor='#F9F9F9'><strong>$mail</strong></td>
		  </tr>
		  <tr>
			<td align='right' bgcolor='#F9F9F9'>Telefone: </td>
			<td align='left' bgcolor='#F9F9F9'><strong>$fone</strong></td>
		  </tr>
		  <tr>
			<td align='right' bgcolor='#F9F9F9'>Endere&ccedil;o: </td>
			<td align='left' bgcolor='#F9F9F9'><strong>$endereco</strong></td>
		  </tr>
		  <tr>
			<td align='right' bgcolor='#F9F9F9'>Cidade: </td>
			<td align='left' bgcolor='#F9F9F9'><strong>$cidade</strong></td>
		  </tr>
		  <tr>
			<td align='right' bgcolor='#F9F9F9'>CPF ou CNPJ: </td>
			<td align='left' bgcolor='#F9F9F9'><strong>$cpf</strong></td>
		  </tr>
		  <tr>
			<td align='right' bgcolor='#F9F9F9'>Mensagem: </td>
			<td align='left' bgcolor='#F9F9F9'><strong>$mensagem</strong></td>
		  </tr>
		  </table>
		  
		 
	</body>
	</html>";
	
	 // faço a chamada da classe
	$Email = new PHPMailer();
	// na classe, há a opção de idioma, setei como br
	$Email->SetLanguage("br");
	//Define que a mensagem será SMTP
	$Email->IsSMTP();
	/*
	// Usa autenticação SMTP?
	$Email->SMTPAuth = "False";
	// Endereço do servidor SMTP
	$Email->Host = "localhost";
	// Usuário do servidor SMTP
	$Email->Username = "websites@visao.psi.br";
	// Senha do servidor SMTP
	$Email->Password = "q1w2e3123!";
	*/
	// ativa o envio de e-mails em HTML.
	$Email->IsHTML(true); 
	// email do remetente da mensagem
	$Email->From = $mail;
	// nome do remetente do email
	$Email->FromName = $nome;
	// Endereço de destino, pra onde a mensagem vai
	$Email->AddAddress($destino);
	//$Email->AddAddress($destino2);
	
	// informando o assunto da mensagem
	$Email->Subject = $assunto;
	// Define o texto da mensagem (aceita HTML)
	$Email->Body = $arquivo;
	//
	
	// enviar
	if($Email->Send()) {
		$r = 101;
	}
	else {
		$r = 102;
	}
}
else {
	$r = 103;
}
header( "Location: ../fale-conosco.php?resposta=".$r );
?>